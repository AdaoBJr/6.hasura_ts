module.exports = {
    "scalars": [
        1,
        2,
        3,
        7,
        8,
        9,
        11,
        13,
        19,
        20,
        33,
        42,
        58,
        59,
        68,
        88,
        90,
        106,
        109,
        115,
        117,
        120,
        149,
        154,
        155,
        165,
        176,
        177,
        182,
        184,
        185,
        192,
        221,
        244,
        249,
        262,
        310,
        334,
        379,
        393,
        404,
        437,
        446,
        461,
        462,
        499
    ],
    "types": {
        "Query": {
            "attributesMetadata": [
                4,
                {
                    "entityType": [
                        1,
                        "AttributeEntityTypeEnum!"
                    ],
                    "attributeUids": [
                        2,
                        "[ID!]"
                    ],
                    "showSystemAttributes": [
                        3
                    ]
                }
            ],
            "availableStores": [
                12,
                {
                    "useCurrentGroup": [
                        3
                    ]
                }
            ],
            "cart": [
                15,
                {
                    "cart_id": [
                        7,
                        "String!"
                    ]
                }
            ],
            "categories": [
                85,
                {
                    "filters": [
                        82
                    ],
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "category": [
                86,
                {
                    "id": [
                        9
                    ]
                }
            ],
            "categoryList": [
                86,
                {
                    "filters": [
                        82
                    ]
                }
            ],
            "checkoutAgreements": [
                89
            ],
            "cmsBlocks": [
                91,
                {
                    "identifiers": [
                        7,
                        "[String]"
                    ]
                }
            ],
            "cmsPage": [
                92,
                {
                    "id": [
                        9
                    ],
                    "identifier": [
                        7
                    ]
                }
            ],
            "compareList": [
                93,
                {
                    "uid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "countries": [
                97
            ],
            "country": [
                97,
                {
                    "id": [
                        7
                    ]
                }
            ],
            "currency": [
                99
            ],
            "customAttributeMetadata": [
                102,
                {
                    "attributes": [
                        101,
                        "[AttributeInput!]!"
                    ]
                }
            ],
            "customer": [
                107
            ],
            "customerCart": [
                15
            ],
            "customerDownloadableProducts": [
                178
            ],
            "customerOrders": [
                125
            ],
            "customerPaymentTokens": [
                180
            ],
            "dynamicBlocks": [
                186,
                {
                    "input": [
                        183
                    ],
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "getHostedProUrl": [
                189,
                {
                    "input": [
                        188,
                        "HostedProUrlInput!"
                    ]
                }
            ],
            "getPayflowLinkToken": [
                191,
                {
                    "input": [
                        190,
                        "PayflowLinkTokenInput!"
                    ]
                }
            ],
            "giftCardAccount": [
                194,
                {
                    "input": [
                        193,
                        "GiftCardAccountInput!"
                    ]
                }
            ],
            "giftRegistry": [
                112,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "giftRegistryEmailSearch": [
                195,
                {
                    "email": [
                        7,
                        "String!"
                    ]
                }
            ],
            "giftRegistryIdSearch": [
                195,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "giftRegistryTypeSearch": [
                195,
                {
                    "firstName": [
                        7,
                        "String!"
                    ],
                    "lastName": [
                        7,
                        "String!"
                    ],
                    "giftRegistryTypeUid": [
                        2
                    ]
                }
            ],
            "giftRegistryTypes": [
                121
            ],
            "isEmailAvailable": [
                196,
                {
                    "email": [
                        7,
                        "String!"
                    ]
                }
            ],
            "pickupLocations": [
                202,
                {
                    "area": [
                        197
                    ],
                    "filters": [
                        198
                    ],
                    "sort": [
                        200
                    ],
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ],
                    "productsInfo": [
                        201,
                        "[ProductInfoInput]"
                    ]
                }
            ],
            "productReviewRatingsMetadata": [
                204
            ],
            "products": [
                209,
                {
                    "search": [
                        7
                    ],
                    "filter": [
                        207
                    ],
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ],
                    "sort": [
                        41
                    ]
                }
            ],
            "recaptchaV3Config": [
                220
            ],
            "route": [
                87,
                {
                    "url": [
                        7,
                        "String!"
                    ]
                }
            ],
            "storeConfig": [
                12
            ],
            "urlResolver": [
                222,
                {
                    "url": [
                        7,
                        "String!"
                    ]
                }
            ],
            "wishlist": [
                223
            ],
            "__typename": [
                7
            ]
        },
        "AttributeEntityTypeEnum": {},
        "ID": {},
        "Boolean": {},
        "AttributesMetadata": {
            "items": [
                5
            ],
            "__typename": [
                7
            ]
        },
        "AttributeMetadataInterface": {
            "attribute_labels": [
                6
            ],
            "code": [
                7
            ],
            "data_type": [
                8
            ],
            "entity_type": [
                1
            ],
            "is_system": [
                3
            ],
            "label": [
                7
            ],
            "sort_order": [
                9
            ],
            "ui_input": [
                10
            ],
            "uid": [
                2
            ],
            "on_ProductAttributeMetadata": [
                445
            ],
            "__typename": [
                7
            ]
        },
        "StoreLabels": {
            "label": [
                7
            ],
            "store_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "String": {},
        "ObjectDataTypeEnum": {},
        "Int": {},
        "UiInputTypeInterface": {
            "is_html_allowed": [
                3
            ],
            "ui_input_type": [
                11
            ],
            "on_UiAttributeTypeSelect": [
                400
            ],
            "on_UiAttributeTypeMultiSelect": [
                401
            ],
            "on_UiAttributeTypeBoolean": [
                402
            ],
            "on_UiAttributeTypeAny": [
                403
            ],
            "on_UiAttributeTypeFixedProductTax": [
                496
            ],
            "__typename": [
                7
            ]
        },
        "UiInputTypeEnum": {},
        "StoreConfig": {
            "absolute_footer": [
                7
            ],
            "allow_gift_receipt": [
                7
            ],
            "allow_gift_wrapping_on_order": [
                7
            ],
            "allow_gift_wrapping_on_order_items": [
                7
            ],
            "allow_guests_to_write_product_reviews": [
                7
            ],
            "allow_items": [
                7
            ],
            "allow_order": [
                7
            ],
            "allow_printed_card": [
                7
            ],
            "autocomplete_on_storefront": [
                3
            ],
            "base_currency_code": [
                7
            ],
            "base_link_url": [
                7
            ],
            "base_media_url": [
                7
            ],
            "base_static_url": [
                7
            ],
            "base_url": [
                7
            ],
            "braintree_cc_vault_active": [
                7
            ],
            "cart_gift_wrapping": [
                7
            ],
            "cart_printed_card": [
                7
            ],
            "catalog_default_sort_by": [
                7
            ],
            "category_fixed_product_tax_display_setting": [
                13
            ],
            "category_url_suffix": [
                7
            ],
            "check_money_order_enable_for_specific_countries": [
                3
            ],
            "check_money_order_enabled": [
                3
            ],
            "check_money_order_make_check_payable_to": [
                7
            ],
            "check_money_order_max_order_total": [
                7
            ],
            "check_money_order_min_order_total": [
                7
            ],
            "check_money_order_new_order_status": [
                7
            ],
            "check_money_order_payment_from_specific_countries": [
                7
            ],
            "check_money_order_send_check_to": [
                7
            ],
            "check_money_order_sort_order": [
                9
            ],
            "check_money_order_title": [
                7
            ],
            "cms_home_page": [
                7
            ],
            "cms_no_cookies": [
                7
            ],
            "cms_no_route": [
                7
            ],
            "code": [
                7
            ],
            "configurable_thumbnail_source": [
                7
            ],
            "contact_enabled": [
                3
            ],
            "copyright": [
                7
            ],
            "default_description": [
                7
            ],
            "default_display_currency_code": [
                7
            ],
            "default_keywords": [
                7
            ],
            "default_title": [
                7
            ],
            "demonotice": [
                9
            ],
            "enable_multiple_wishlists": [
                7
            ],
            "front": [
                7
            ],
            "grid_per_page": [
                9
            ],
            "grid_per_page_values": [
                7
            ],
            "head_includes": [
                7
            ],
            "head_shortcut_icon": [
                7
            ],
            "header_logo_src": [
                7
            ],
            "id": [
                9
            ],
            "is_default_store": [
                3
            ],
            "is_default_store_group": [
                3
            ],
            "list_mode": [
                7
            ],
            "list_per_page": [
                9
            ],
            "list_per_page_values": [
                7
            ],
            "locale": [
                7
            ],
            "logo_alt": [
                7
            ],
            "logo_height": [
                9
            ],
            "logo_width": [
                9
            ],
            "magento_reward_general_is_enabled": [
                7
            ],
            "magento_reward_general_is_enabled_on_front": [
                7
            ],
            "magento_reward_general_min_points_balance": [
                7
            ],
            "magento_reward_general_publish_history": [
                7
            ],
            "magento_reward_points_invitation_customer": [
                7
            ],
            "magento_reward_points_invitation_customer_limit": [
                7
            ],
            "magento_reward_points_invitation_order": [
                7
            ],
            "magento_reward_points_invitation_order_limit": [
                7
            ],
            "magento_reward_points_newsletter": [
                7
            ],
            "magento_reward_points_order": [
                7
            ],
            "magento_reward_points_register": [
                7
            ],
            "magento_reward_points_review": [
                7
            ],
            "magento_reward_points_review_limit": [
                7
            ],
            "magento_wishlist_general_is_enabled": [
                7
            ],
            "maximum_number_of_wishlists": [
                7
            ],
            "minimum_password_length": [
                7
            ],
            "newsletter_enabled": [
                3
            ],
            "no_route": [
                7
            ],
            "payment_payflowpro_cc_vault_active": [
                7
            ],
            "printed_card_price": [
                7
            ],
            "product_fixed_product_tax_display_setting": [
                13
            ],
            "product_reviews_enabled": [
                7
            ],
            "product_url_suffix": [
                7
            ],
            "required_character_classes_number": [
                7
            ],
            "returns_enabled": [
                7
            ],
            "root_category_id": [
                9
            ],
            "root_category_uid": [
                2
            ],
            "sales_fixed_product_tax_display_setting": [
                13
            ],
            "sales_gift_wrapping": [
                7
            ],
            "sales_printed_card": [
                7
            ],
            "secure_base_link_url": [
                7
            ],
            "secure_base_media_url": [
                7
            ],
            "secure_base_static_url": [
                7
            ],
            "secure_base_url": [
                7
            ],
            "send_friend": [
                14
            ],
            "show_cms_breadcrumbs": [
                9
            ],
            "store_code": [
                2
            ],
            "store_group_code": [
                2
            ],
            "store_group_name": [
                7
            ],
            "store_name": [
                7
            ],
            "store_sort_order": [
                9
            ],
            "timezone": [
                7
            ],
            "title_prefix": [
                7
            ],
            "title_separator": [
                7
            ],
            "title_suffix": [
                7
            ],
            "use_store_in_url": [
                3
            ],
            "website_code": [
                2
            ],
            "website_id": [
                9
            ],
            "website_name": [
                7
            ],
            "weight_unit": [
                7
            ],
            "welcome": [
                7
            ],
            "zero_subtotal_enable_for_specific_countries": [
                3
            ],
            "zero_subtotal_enabled": [
                3
            ],
            "zero_subtotal_new_order_status": [
                7
            ],
            "zero_subtotal_payment_action": [
                7
            ],
            "zero_subtotal_payment_from_specific_countries": [
                7
            ],
            "zero_subtotal_sort_order": [
                9
            ],
            "zero_subtotal_title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "FixedProductTaxDisplaySettings": {},
        "SendFriendConfiguration": {
            "enabled_for_customers": [
                3
            ],
            "enabled_for_guests": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "Cart": {
            "applied_coupon": [
                16
            ],
            "applied_coupons": [
                16
            ],
            "applied_gift_cards": [
                17
            ],
            "applied_reward_points": [
                21
            ],
            "applied_store_credit": [
                22
            ],
            "available_gift_wrappings": [
                23
            ],
            "available_payment_methods": [
                25
            ],
            "billing_address": [
                26
            ],
            "email": [
                7
            ],
            "gift_message": [
                30
            ],
            "gift_receipt_included": [
                3
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                2
            ],
            "is_virtual": [
                3
            ],
            "items": [
                31
            ],
            "prices": [
                73
            ],
            "printed_card_included": [
                3
            ],
            "selected_payment_method": [
                77
            ],
            "shipping_addresses": [
                78
            ],
            "total_quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "AppliedCoupon": {
            "code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AppliedGiftCard": {
            "applied_balance": [
                18
            ],
            "code": [
                7
            ],
            "current_balance": [
                18
            ],
            "expiration_date": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Money": {
            "currency": [
                19
            ],
            "value": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "CurrencyEnum": {},
        "Float": {},
        "RewardPointsAmount": {
            "money": [
                18
            ],
            "points": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "AppliedStoreCredit": {
            "applied_balance": [
                18
            ],
            "current_balance": [
                18
            ],
            "enabled": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "GiftWrapping": {
            "design": [
                7
            ],
            "id": [
                2
            ],
            "image": [
                24
            ],
            "price": [
                18
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "GiftWrappingImage": {
            "label": [
                7
            ],
            "url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AvailablePaymentMethod": {
            "code": [
                7
            ],
            "title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "BillingCartAddress": {
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country": [
                28
            ],
            "customer_notes": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "postcode": [
                7
            ],
            "region": [
                29
            ],
            "street": [
                7
            ],
            "telephone": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartAddressInterface": {
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country": [
                28
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "postcode": [
                7
            ],
            "region": [
                29
            ],
            "street": [
                7
            ],
            "telephone": [
                7
            ],
            "on_BillingCartAddress": [
                26
            ],
            "on_ShippingCartAddress": [
                78
            ],
            "__typename": [
                7
            ]
        },
        "CartAddressCountry": {
            "code": [
                7
            ],
            "label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartAddressRegion": {
            "code": [
                7
            ],
            "label": [
                7
            ],
            "region_id": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "GiftMessage": {
            "from": [
                7
            ],
            "message": [
                7
            ],
            "to": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartItemInterface": {
            "errors": [
                32
            ],
            "id": [
                7
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "on_SimpleCartItem": [
                433
            ],
            "on_VirtualCartItem": [
                434
            ],
            "on_DownloadableCartItem": [
                435
            ],
            "on_ConfigurableCartItem": [
                447
            ],
            "on_BundleCartItem": [
                449
            ],
            "on_GiftCardCartItem": [
                505
            ],
            "__typename": [
                7
            ]
        },
        "CartItemError": {
            "code": [
                33
            ],
            "message": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartItemErrorType": {},
        "CartItemPrices": {
            "discounts": [
                35
            ],
            "fixed_product_taxes": [
                36
            ],
            "price": [
                18
            ],
            "row_total": [
                18
            ],
            "row_total_including_tax": [
                18
            ],
            "total_item_discount": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "Discount": {
            "amount": [
                18
            ],
            "label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "FixedProductTax": {
            "amount": [
                18
            ],
            "label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductInterface": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "related_products": [
                37
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "on_VirtualProduct": [
                426
            ],
            "on_SimpleProduct": [
                427
            ],
            "on_DownloadableProduct": [
                439
            ],
            "on_BundleProduct": [
                460
            ],
            "on_GroupedProduct": [
                470
            ],
            "on_ConfigurableProduct": [
                473
            ],
            "on_GiftCardProduct": [
                497
            ],
            "__typename": [
                7
            ]
        },
        "CategoryInterface": {
            "automatic_sorting": [
                7
            ],
            "available_sort_by": [
                7
            ],
            "breadcrumbs": [
                39
            ],
            "canonical_url": [
                7
            ],
            "children_count": [
                7
            ],
            "cms_block": [
                40
            ],
            "created_at": [
                7
            ],
            "custom_layout_update_file": [
                7
            ],
            "default_sort_by": [
                7
            ],
            "description": [
                7
            ],
            "display_mode": [
                7
            ],
            "filter_price_range": [
                20
            ],
            "id": [
                9
            ],
            "image": [
                7
            ],
            "include_in_menu": [
                9
            ],
            "is_anchor": [
                9
            ],
            "landing_page": [
                9
            ],
            "level": [
                9
            ],
            "meta_description": [
                7
            ],
            "meta_keywords": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "path": [
                7
            ],
            "path_in_store": [
                7
            ],
            "position": [
                9
            ],
            "product_count": [
                9
            ],
            "products": [
                43,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ],
                    "sort": [
                        41
                    ]
                }
            ],
            "staged": [
                3
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_suffix": [
                7
            ],
            "on_CategoryTree": [
                86
            ],
            "__typename": [
                7
            ]
        },
        "Breadcrumb": {
            "category_id": [
                9
            ],
            "category_level": [
                9
            ],
            "category_name": [
                7
            ],
            "category_uid": [
                2
            ],
            "category_url_key": [
                7
            ],
            "category_url_path": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CmsBlock": {
            "content": [
                7
            ],
            "identifier": [
                7
            ],
            "title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductAttributeSortInput": {
            "name": [
                42
            ],
            "position": [
                42
            ],
            "price": [
                42
            ],
            "relevance": [
                42
            ],
            "__typename": [
                7
            ]
        },
        "SortEnum": {},
        "CategoryProducts": {
            "items": [
                37
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "SearchResultPageInfo": {
            "current_page": [
                9
            ],
            "page_size": [
                9
            ],
            "total_pages": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "CustomAttribute": {
            "attribute_metadata": [
                5
            ],
            "entered_attribute_value": [
                46
            ],
            "selected_attribute_options": [
                47
            ],
            "__typename": [
                7
            ]
        },
        "EnteredAttributeValue": {
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SelectedAttributeOption": {
            "attribute_option": [
                48
            ],
            "__typename": [
                7
            ]
        },
        "AttributeOptionInterface": {
            "is_default": [
                3
            ],
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "on_AttributeOption": [
                104
            ],
            "__typename": [
                7
            ]
        },
        "ComplexTextValue": {
            "html": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductImage": {
            "disabled": [
                3
            ],
            "label": [
                7
            ],
            "position": [
                9
            ],
            "url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "MediaGalleryInterface": {
            "disabled": [
                3
            ],
            "label": [
                7
            ],
            "position": [
                9
            ],
            "url": [
                7
            ],
            "on_ProductImage": [
                50
            ],
            "on_ProductVideo": [
                420
            ],
            "__typename": [
                7
            ]
        },
        "MediaGalleryEntry": {
            "content": [
                53
            ],
            "disabled": [
                3
            ],
            "file": [
                7
            ],
            "id": [
                9
            ],
            "label": [
                7
            ],
            "media_type": [
                7
            ],
            "position": [
                9
            ],
            "types": [
                7
            ],
            "uid": [
                2
            ],
            "video_content": [
                54
            ],
            "__typename": [
                7
            ]
        },
        "ProductMediaGalleryEntriesContent": {
            "base64_encoded_data": [
                7
            ],
            "name": [
                7
            ],
            "type": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductMediaGalleryEntriesVideoContent": {
            "media_type": [
                7
            ],
            "video_description": [
                7
            ],
            "video_metadata": [
                7
            ],
            "video_provider": [
                7
            ],
            "video_title": [
                7
            ],
            "video_url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductPrices": {
            "maximalPrice": [
                56
            ],
            "minimalPrice": [
                56
            ],
            "regularPrice": [
                56
            ],
            "__typename": [
                7
            ]
        },
        "Price": {
            "adjustments": [
                57
            ],
            "amount": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "PriceAdjustment": {
            "amount": [
                18
            ],
            "code": [
                58
            ],
            "description": [
                59
            ],
            "__typename": [
                7
            ]
        },
        "PriceAdjustmentCodesEnum": {},
        "PriceAdjustmentDescriptionEnum": {},
        "PriceRange": {
            "maximum_price": [
                61
            ],
            "minimum_price": [
                61
            ],
            "__typename": [
                7
            ]
        },
        "ProductPrice": {
            "discount": [
                62
            ],
            "final_price": [
                18
            ],
            "fixed_product_taxes": [
                36
            ],
            "regular_price": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "ProductDiscount": {
            "amount_off": [
                20
            ],
            "percent_off": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "TierPrice": {
            "discount": [
                62
            ],
            "final_price": [
                18
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "ProductLinksInterface": {
            "link_type": [
                7
            ],
            "linked_product_sku": [
                7
            ],
            "linked_product_type": [
                7
            ],
            "position": [
                9
            ],
            "sku": [
                7
            ],
            "on_ProductLinks": [
                405
            ],
            "__typename": [
                7
            ]
        },
        "ProductReviews": {
            "items": [
                66
            ],
            "page_info": [
                44
            ],
            "__typename": [
                7
            ]
        },
        "ProductReview": {
            "average_rating": [
                20
            ],
            "created_at": [
                7
            ],
            "nickname": [
                7
            ],
            "product": [
                37
            ],
            "ratings_breakdown": [
                67
            ],
            "summary": [
                7
            ],
            "text": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductReviewRating": {
            "name": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductStockStatus": {},
        "ProductTierPrices": {
            "customer_group_id": [
                7
            ],
            "percentage_value": [
                20
            ],
            "qty": [
                20
            ],
            "value": [
                20
            ],
            "website_id": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "UrlRewrite": {
            "parameters": [
                71
            ],
            "url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "HttpQueryParameter": {
            "name": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Website": {
            "code": [
                7
            ],
            "default_group_id": [
                7
            ],
            "id": [
                9
            ],
            "is_default": [
                3
            ],
            "name": [
                7
            ],
            "sort_order": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "CartPrices": {
            "applied_taxes": [
                74
            ],
            "discount": [
                75
            ],
            "discounts": [
                35
            ],
            "gift_options": [
                76
            ],
            "grand_total": [
                18
            ],
            "subtotal_excluding_tax": [
                18
            ],
            "subtotal_including_tax": [
                18
            ],
            "subtotal_with_discount_excluding_tax": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "CartTaxItem": {
            "amount": [
                18
            ],
            "label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartDiscount": {
            "amount": [
                18
            ],
            "label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftOptionsPrices": {
            "gift_wrapping_for_items": [
                18
            ],
            "gift_wrapping_for_order": [
                18
            ],
            "printed_card": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "SelectedPaymentMethod": {
            "code": [
                7
            ],
            "purchase_order_number": [
                7
            ],
            "title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ShippingCartAddress": {
            "available_shipping_methods": [
                79
            ],
            "cart_items": [
                80
            ],
            "cart_items_v2": [
                31
            ],
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country": [
                28
            ],
            "customer_notes": [
                7
            ],
            "firstname": [
                7
            ],
            "items_weight": [
                20
            ],
            "lastname": [
                7
            ],
            "pickup_location_code": [
                7
            ],
            "postcode": [
                7
            ],
            "region": [
                29
            ],
            "selected_shipping_method": [
                81
            ],
            "street": [
                7
            ],
            "telephone": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AvailableShippingMethod": {
            "amount": [
                18
            ],
            "available": [
                3
            ],
            "base_amount": [
                18
            ],
            "carrier_code": [
                7
            ],
            "carrier_title": [
                7
            ],
            "error_message": [
                7
            ],
            "method_code": [
                7
            ],
            "method_title": [
                7
            ],
            "price_excl_tax": [
                18
            ],
            "price_incl_tax": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "CartItemQuantity": {
            "cart_item_id": [
                9
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "SelectedShippingMethod": {
            "amount": [
                18
            ],
            "base_amount": [
                18
            ],
            "carrier_code": [
                7
            ],
            "carrier_title": [
                7
            ],
            "method_code": [
                7
            ],
            "method_title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CategoryFilterInput": {
            "category_uid": [
                83
            ],
            "ids": [
                83
            ],
            "name": [
                84
            ],
            "parent_category_uid": [
                83
            ],
            "parent_id": [
                83
            ],
            "url_key": [
                83
            ],
            "url_path": [
                83
            ],
            "__typename": [
                7
            ]
        },
        "FilterEqualTypeInput": {
            "eq": [
                7
            ],
            "in": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "FilterMatchTypeInput": {
            "match": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CategoryResult": {
            "items": [
                86
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "CategoryTree": {
            "automatic_sorting": [
                7
            ],
            "available_sort_by": [
                7
            ],
            "breadcrumbs": [
                39
            ],
            "canonical_url": [
                7
            ],
            "children": [
                86
            ],
            "children_count": [
                7
            ],
            "cms_block": [
                40
            ],
            "created_at": [
                7
            ],
            "custom_layout_update_file": [
                7
            ],
            "default_sort_by": [
                7
            ],
            "description": [
                7
            ],
            "display_mode": [
                7
            ],
            "filter_price_range": [
                20
            ],
            "id": [
                9
            ],
            "image": [
                7
            ],
            "include_in_menu": [
                9
            ],
            "is_anchor": [
                9
            ],
            "landing_page": [
                9
            ],
            "level": [
                9
            ],
            "meta_description": [
                7
            ],
            "meta_keywords": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "path": [
                7
            ],
            "path_in_store": [
                7
            ],
            "position": [
                9
            ],
            "product_count": [
                9
            ],
            "products": [
                43,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ],
                    "sort": [
                        41
                    ]
                }
            ],
            "redirect_code": [
                9
            ],
            "relative_url": [
                7
            ],
            "staged": [
                3
            ],
            "type": [
                88
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_suffix": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "RoutableInterface": {
            "redirect_code": [
                9
            ],
            "relative_url": [
                7
            ],
            "type": [
                88
            ],
            "on_CategoryTree": [
                86
            ],
            "on_CmsPage": [
                92
            ],
            "on_VirtualProduct": [
                426
            ],
            "on_SimpleProduct": [
                427
            ],
            "on_DownloadableProduct": [
                439
            ],
            "on_BundleProduct": [
                460
            ],
            "on_GroupedProduct": [
                470
            ],
            "on_ConfigurableProduct": [
                473
            ],
            "on_GiftCardProduct": [
                497
            ],
            "__typename": [
                7
            ]
        },
        "UrlRewriteEntityTypeEnum": {},
        "CheckoutAgreement": {
            "agreement_id": [
                9
            ],
            "checkbox_text": [
                7
            ],
            "content": [
                7
            ],
            "content_height": [
                7
            ],
            "is_html": [
                3
            ],
            "mode": [
                90
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CheckoutAgreementMode": {},
        "CmsBlocks": {
            "items": [
                40
            ],
            "__typename": [
                7
            ]
        },
        "CmsPage": {
            "content": [
                7
            ],
            "content_heading": [
                7
            ],
            "identifier": [
                7
            ],
            "meta_description": [
                7
            ],
            "meta_keywords": [
                7
            ],
            "meta_title": [
                7
            ],
            "page_layout": [
                7
            ],
            "redirect_code": [
                9
            ],
            "relative_url": [
                7
            ],
            "title": [
                7
            ],
            "type": [
                88
            ],
            "url_key": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CompareList": {
            "attributes": [
                94
            ],
            "item_count": [
                9
            ],
            "items": [
                95
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ComparableAttribute": {
            "code": [
                7
            ],
            "label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ComparableItem": {
            "attributes": [
                96
            ],
            "product": [
                37
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ProductAttribute": {
            "code": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Country": {
            "available_regions": [
                98
            ],
            "full_name_english": [
                7
            ],
            "full_name_locale": [
                7
            ],
            "id": [
                7
            ],
            "three_letter_abbreviation": [
                7
            ],
            "two_letter_abbreviation": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Region": {
            "code": [
                7
            ],
            "id": [
                9
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Currency": {
            "available_currency_codes": [
                7
            ],
            "base_currency_code": [
                7
            ],
            "base_currency_symbol": [
                7
            ],
            "default_display_currecy_code": [
                7
            ],
            "default_display_currecy_symbol": [
                7
            ],
            "default_display_currency_code": [
                7
            ],
            "default_display_currency_symbol": [
                7
            ],
            "exchange_rates": [
                100
            ],
            "__typename": [
                7
            ]
        },
        "ExchangeRate": {
            "currency_to": [
                7
            ],
            "rate": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "AttributeInput": {
            "attribute_code": [
                7
            ],
            "entity_type": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomAttributeMetadata": {
            "items": [
                103
            ],
            "__typename": [
                7
            ]
        },
        "Attribute": {
            "attribute_code": [
                7
            ],
            "attribute_options": [
                104
            ],
            "attribute_type": [
                7
            ],
            "entity_type": [
                7
            ],
            "input_type": [
                7
            ],
            "storefront_properties": [
                105
            ],
            "__typename": [
                7
            ]
        },
        "AttributeOption": {
            "is_default": [
                3
            ],
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "StorefrontProperties": {
            "position": [
                9
            ],
            "use_in_layered_navigation": [
                106
            ],
            "use_in_product_listing": [
                3
            ],
            "use_in_search_results_layered_navigation": [
                3
            ],
            "visible_on_catalog_pages": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "UseInLayeredNavigationOptions": {},
        "Customer": {
            "addresses": [
                108
            ],
            "allow_remote_shopping_assistance": [
                3
            ],
            "compare_list": [
                93
            ],
            "created_at": [
                7
            ],
            "date_of_birth": [
                7
            ],
            "default_billing": [
                7
            ],
            "default_shipping": [
                7
            ],
            "dob": [
                7
            ],
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "gender": [
                9
            ],
            "gift_registries": [
                112
            ],
            "gift_registry": [
                112,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "group_id": [
                9
            ],
            "id": [
                9
            ],
            "is_subscribed": [
                3
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "orders": [
                125,
                {
                    "filter": [
                        123
                    ],
                    "currentPage": [
                        9
                    ],
                    "pageSize": [
                        9
                    ]
                }
            ],
            "prefix": [
                7
            ],
            "return": [
                143,
                {
                    "uid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "returns": [
                142,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "reward_points": [
                160
            ],
            "store_credit": [
                166
            ],
            "suffix": [
                7
            ],
            "taxvat": [
                7
            ],
            "wishlist": [
                169
            ],
            "wishlist_v2": [
                169,
                {
                    "id": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "wishlists": [
                169,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "__typename": [
                7
            ]
        },
        "CustomerAddress": {
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country_code": [
                109
            ],
            "country_id": [
                7
            ],
            "custom_attributes": [
                110
            ],
            "customer_id": [
                9
            ],
            "default_billing": [
                3
            ],
            "default_shipping": [
                3
            ],
            "extension_attributes": [
                110
            ],
            "fax": [
                7
            ],
            "firstname": [
                7
            ],
            "id": [
                9
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "postcode": [
                7
            ],
            "prefix": [
                7
            ],
            "region": [
                111
            ],
            "region_id": [
                9
            ],
            "street": [
                7
            ],
            "suffix": [
                7
            ],
            "telephone": [
                7
            ],
            "vat_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CountryCodeEnum": {},
        "CustomerAddressAttribute": {
            "attribute_code": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomerAddressRegion": {
            "region": [
                7
            ],
            "region_code": [
                7
            ],
            "region_id": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistry": {
            "created_at": [
                7
            ],
            "dynamic_attributes": [
                113
            ],
            "event_name": [
                7
            ],
            "items": [
                116
            ],
            "message": [
                7
            ],
            "owner_name": [
                7
            ],
            "privacy_settings": [
                117
            ],
            "registrants": [
                118
            ],
            "shipping_address": [
                108
            ],
            "status": [
                120
            ],
            "type": [
                121
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryDynamicAttribute": {
            "code": [
                2
            ],
            "group": [
                115
            ],
            "label": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryDynamicAttributeInterface": {
            "code": [
                2
            ],
            "label": [
                7
            ],
            "value": [
                7
            ],
            "on_GiftRegistryDynamicAttribute": [
                113
            ],
            "on_GiftRegistryRegistrantDynamicAttribute": [
                119
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryDynamicAttributeGroup": {},
        "GiftRegistryItemInterface": {
            "created_at": [
                7
            ],
            "note": [
                7
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "quantity_fulfilled": [
                20
            ],
            "uid": [
                2
            ],
            "on_GiftRegistryItem": [
                457
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryPrivacySettings": {},
        "GiftRegistryRegistrant": {
            "dynamic_attributes": [
                119
            ],
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryRegistrantDynamicAttribute": {
            "code": [
                2
            ],
            "label": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryStatus": {},
        "GiftRegistryType": {
            "dynamic_attributes_metadata": [
                122
            ],
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryDynamicAttributeMetadataInterface": {
            "attribute_group": [
                7
            ],
            "code": [
                2
            ],
            "input_type": [
                7
            ],
            "is_required": [
                3
            ],
            "label": [
                7
            ],
            "sort_order": [
                9
            ],
            "on_GiftRegistryDynamicAttributeMetadata": [
                456
            ],
            "__typename": [
                7
            ]
        },
        "CustomerOrdersFilterInput": {
            "number": [
                124
            ],
            "__typename": [
                7
            ]
        },
        "FilterStringTypeInput": {
            "eq": [
                7
            ],
            "in": [
                7
            ],
            "match": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomerOrders": {
            "items": [
                126
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "CustomerOrder": {
            "billing_address": [
                127
            ],
            "carrier": [
                7
            ],
            "comments": [
                128
            ],
            "created_at": [
                7
            ],
            "credit_memos": [
                129
            ],
            "gift_message": [
                30
            ],
            "gift_receipt_included": [
                3
            ],
            "gift_wrapping": [
                23
            ],
            "grand_total": [
                20
            ],
            "id": [
                2
            ],
            "increment_id": [
                7
            ],
            "invoices": [
                137
            ],
            "items": [
                131
            ],
            "items_eligible_for_return": [
                131
            ],
            "number": [
                7
            ],
            "order_date": [
                7
            ],
            "order_number": [
                7
            ],
            "payment_methods": [
                140
            ],
            "printed_card_included": [
                3
            ],
            "returns": [
                142,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "shipments": [
                156
            ],
            "shipping_address": [
                127
            ],
            "shipping_method": [
                7
            ],
            "status": [
                7
            ],
            "total": [
                159
            ],
            "__typename": [
                7
            ]
        },
        "OrderAddress": {
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country_code": [
                109
            ],
            "fax": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "postcode": [
                7
            ],
            "prefix": [
                7
            ],
            "region": [
                7
            ],
            "region_id": [
                2
            ],
            "street": [
                7
            ],
            "suffix": [
                7
            ],
            "telephone": [
                7
            ],
            "vat_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SalesCommentItem": {
            "message": [
                7
            ],
            "timestamp": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CreditMemo": {
            "comments": [
                128
            ],
            "id": [
                2
            ],
            "items": [
                130
            ],
            "number": [
                7
            ],
            "total": [
                133
            ],
            "__typename": [
                7
            ]
        },
        "CreditMemoItemInterface": {
            "discounts": [
                35
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_refunded": [
                20
            ],
            "on_DownloadableCreditMemoItem": [
                443
            ],
            "on_BundleCreditMemoItem": [
                468
            ],
            "on_CreditMemoItem": [
                487
            ],
            "on_GiftCardCreditMemoItem": [
                503
            ],
            "__typename": [
                7
            ]
        },
        "OrderItemInterface": {
            "discounts": [
                35
            ],
            "eligible_for_return": [
                3
            ],
            "entered_options": [
                132
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                2
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "product_type": [
                7
            ],
            "product_url_key": [
                7
            ],
            "quantity_canceled": [
                20
            ],
            "quantity_invoiced": [
                20
            ],
            "quantity_ordered": [
                20
            ],
            "quantity_refunded": [
                20
            ],
            "quantity_returned": [
                20
            ],
            "quantity_shipped": [
                20
            ],
            "selected_options": [
                132
            ],
            "status": [
                7
            ],
            "on_DownloadableOrderItem": [
                440
            ],
            "on_BundleOrderItem": [
                463
            ],
            "on_OrderItem": [
                484
            ],
            "on_GiftCardOrderItem": [
                500
            ],
            "__typename": [
                7
            ]
        },
        "OrderItemOption": {
            "label": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CreditMemoTotal": {
            "adjustment": [
                18
            ],
            "base_grand_total": [
                18
            ],
            "discounts": [
                35
            ],
            "grand_total": [
                18
            ],
            "shipping_handling": [
                134
            ],
            "subtotal": [
                18
            ],
            "taxes": [
                136
            ],
            "total_shipping": [
                18
            ],
            "total_tax": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "ShippingHandling": {
            "amount_excluding_tax": [
                18
            ],
            "amount_including_tax": [
                18
            ],
            "discounts": [
                135
            ],
            "taxes": [
                136
            ],
            "total_amount": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "ShippingDiscount": {
            "amount": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "TaxItem": {
            "amount": [
                18
            ],
            "rate": [
                20
            ],
            "title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Invoice": {
            "comments": [
                128
            ],
            "id": [
                2
            ],
            "items": [
                138
            ],
            "number": [
                7
            ],
            "total": [
                139
            ],
            "__typename": [
                7
            ]
        },
        "InvoiceItemInterface": {
            "discounts": [
                35
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_invoiced": [
                20
            ],
            "on_DownloadableInvoiceItem": [
                442
            ],
            "on_BundleInvoiceItem": [
                466
            ],
            "on_InvoiceItem": [
                485
            ],
            "on_GiftCardInvoiceItem": [
                502
            ],
            "__typename": [
                7
            ]
        },
        "InvoiceTotal": {
            "base_grand_total": [
                18
            ],
            "discounts": [
                35
            ],
            "grand_total": [
                18
            ],
            "shipping_handling": [
                134
            ],
            "subtotal": [
                18
            ],
            "taxes": [
                136
            ],
            "total_shipping": [
                18
            ],
            "total_tax": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "OrderPaymentMethod": {
            "additional_data": [
                141
            ],
            "name": [
                7
            ],
            "type": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "KeyValue": {
            "name": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Returns": {
            "items": [
                143
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "Return": {
            "available_shipping_carriers": [
                144
            ],
            "comments": [
                145
            ],
            "created_at": [
                7
            ],
            "customer": [
                146
            ],
            "items": [
                147
            ],
            "number": [
                7
            ],
            "order": [
                126
            ],
            "shipping": [
                150
            ],
            "status": [
                155
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ReturnShippingCarrier": {
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ReturnComment": {
            "author_name": [
                7
            ],
            "created_at": [
                7
            ],
            "text": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ReturnCustomer": {
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ReturnItem": {
            "custom_attributes": [
                148
            ],
            "order_item": [
                131
            ],
            "quantity": [
                20
            ],
            "request_quantity": [
                20
            ],
            "status": [
                149
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ReturnCustomAttribute": {
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ReturnItemStatus": {},
        "ReturnShipping": {
            "address": [
                151
            ],
            "tracking": [
                152,
                {
                    "uid": [
                        2
                    ]
                }
            ],
            "__typename": [
                7
            ]
        },
        "ReturnShippingAddress": {
            "city": [
                7
            ],
            "contact_name": [
                7
            ],
            "country": [
                97
            ],
            "postcode": [
                7
            ],
            "region": [
                98
            ],
            "street": [
                7
            ],
            "telephone": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ReturnShippingTracking": {
            "carrier": [
                144
            ],
            "status": [
                153
            ],
            "tracking_number": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ReturnShippingTrackingStatus": {
            "text": [
                7
            ],
            "type": [
                154
            ],
            "__typename": [
                7
            ]
        },
        "ReturnShippingTrackingStatusType": {},
        "ReturnStatus": {},
        "OrderShipment": {
            "comments": [
                128
            ],
            "id": [
                2
            ],
            "items": [
                157
            ],
            "number": [
                7
            ],
            "tracking": [
                158
            ],
            "__typename": [
                7
            ]
        },
        "ShipmentItemInterface": {
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_shipped": [
                20
            ],
            "on_BundleShipmentItem": [
                467
            ],
            "on_ShipmentItem": [
                486
            ],
            "on_GiftCardShipmentItem": [
                504
            ],
            "__typename": [
                7
            ]
        },
        "ShipmentTracking": {
            "carrier": [
                7
            ],
            "number": [
                7
            ],
            "title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "OrderTotal": {
            "base_grand_total": [
                18
            ],
            "discounts": [
                35
            ],
            "grand_total": [
                18
            ],
            "shipping_handling": [
                134
            ],
            "subtotal": [
                18
            ],
            "taxes": [
                136
            ],
            "total_giftcard": [
                18
            ],
            "total_shipping": [
                18
            ],
            "total_tax": [
                18
            ],
            "__typename": [
                7
            ]
        },
        "RewardPoints": {
            "balance": [
                21
            ],
            "balance_history": [
                161
            ],
            "exchange_rates": [
                162
            ],
            "subscription_status": [
                164
            ],
            "__typename": [
                7
            ]
        },
        "RewardPointsBalanceHistoryItem": {
            "balance": [
                21
            ],
            "change_reason": [
                7
            ],
            "date": [
                7
            ],
            "points_change": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "RewardPointsExchangeRates": {
            "earning": [
                163
            ],
            "redemption": [
                163
            ],
            "__typename": [
                7
            ]
        },
        "RewardPointsRate": {
            "currency_amount": [
                20
            ],
            "points": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "RewardPointsSubscriptionStatus": {
            "balance_updates": [
                165
            ],
            "points_expiration_notifications": [
                165
            ],
            "__typename": [
                7
            ]
        },
        "RewardPointsSubscriptionStatusesEnum": {},
        "CustomerStoreCredit": {
            "balance_history": [
                167,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "current_balance": [
                18
            ],
            "enabled": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "CustomerStoreCreditHistory": {
            "items": [
                168
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "CustomerStoreCreditHistoryItem": {
            "action": [
                7
            ],
            "actual_balance": [
                18
            ],
            "balance_change": [
                18
            ],
            "date_time_changed": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Wishlist": {
            "id": [
                2
            ],
            "items": [
                170
            ],
            "items_count": [
                9
            ],
            "items_v2": [
                171,
                {
                    "currentPage": [
                        9
                    ],
                    "pageSize": [
                        9
                    ]
                }
            ],
            "name": [
                7
            ],
            "sharing_code": [
                7
            ],
            "updated_at": [
                7
            ],
            "visibility": [
                177
            ],
            "__typename": [
                7
            ]
        },
        "WishlistItem": {
            "added_at": [
                7
            ],
            "description": [
                7
            ],
            "id": [
                9
            ],
            "product": [
                37
            ],
            "qty": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "WishlistItems": {
            "items": [
                172
            ],
            "page_info": [
                44
            ],
            "__typename": [
                7
            ]
        },
        "WishlistItemInterface": {
            "added_at": [
                7
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "on_SimpleWishlistItem": [
                431
            ],
            "on_VirtualWishlistItem": [
                432
            ],
            "on_DownloadableWishlistItem": [
                444
            ],
            "on_BundleWishlistItem": [
                469
            ],
            "on_GroupedProductWishlistItem": [
                472
            ],
            "on_ConfigurableWishlistItem": [
                483
            ],
            "on_GiftCardWishlistItem": [
                506
            ],
            "__typename": [
                7
            ]
        },
        "SelectedCustomizableOption": {
            "customizable_option_uid": [
                2
            ],
            "id": [
                9
            ],
            "is_required": [
                3
            ],
            "label": [
                7
            ],
            "sort_order": [
                9
            ],
            "type": [
                7
            ],
            "values": [
                174
            ],
            "__typename": [
                7
            ]
        },
        "SelectedCustomizableOptionValue": {
            "customizable_option_value_uid": [
                2
            ],
            "id": [
                9
            ],
            "label": [
                7
            ],
            "price": [
                175
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartItemSelectedOptionValuePrice": {
            "type": [
                176
            ],
            "units": [
                7
            ],
            "value": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "PriceTypeEnum": {},
        "WishlistVisibilityEnum": {},
        "CustomerDownloadableProducts": {
            "items": [
                179
            ],
            "__typename": [
                7
            ]
        },
        "CustomerDownloadableProduct": {
            "date": [
                7
            ],
            "download_url": [
                7
            ],
            "order_increment_id": [
                7
            ],
            "remaining_downloads": [
                7
            ],
            "status": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomerPaymentTokens": {
            "items": [
                181
            ],
            "__typename": [
                7
            ]
        },
        "PaymentToken": {
            "details": [
                7
            ],
            "payment_method_code": [
                7
            ],
            "public_hash": [
                7
            ],
            "type": [
                182
            ],
            "__typename": [
                7
            ]
        },
        "PaymentTokenTypeEnum": {},
        "DynamicBlocksFilterInput": {
            "cart_id": [
                7
            ],
            "dynamic_block_uids": [
                2
            ],
            "locations": [
                184
            ],
            "product_uid": [
                2
            ],
            "type": [
                185
            ],
            "__typename": [
                7
            ]
        },
        "DynamicBlockLocationEnum": {},
        "DynamicBlockTypeEnum": {},
        "DynamicBlocks": {
            "items": [
                187
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "DynamicBlock": {
            "content": [
                49
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "HostedProUrlInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "HostedProUrl": {
            "secure_form_url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowLinkTokenInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowLinkToken": {
            "mode": [
                192
            ],
            "paypal_url": [
                7
            ],
            "secure_token": [
                7
            ],
            "secure_token_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowLinkMode": {},
        "GiftCardAccountInput": {
            "gift_card_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardAccount": {
            "balance": [
                18
            ],
            "code": [
                7
            ],
            "expiration_date": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistrySearchResult": {
            "event_date": [
                7
            ],
            "event_title": [
                7
            ],
            "gift_registry_uid": [
                2
            ],
            "location": [
                7
            ],
            "name": [
                7
            ],
            "type": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "IsEmailAvailableOutput": {
            "is_email_available": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "AreaInput": {
            "radius": [
                9
            ],
            "search_term": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PickupLocationFilterInput": {
            "city": [
                199
            ],
            "country_id": [
                199
            ],
            "name": [
                199
            ],
            "pickup_location_code": [
                199
            ],
            "postcode": [
                199
            ],
            "region": [
                199
            ],
            "region_id": [
                199
            ],
            "street": [
                199
            ],
            "__typename": [
                7
            ]
        },
        "FilterTypeInput": {
            "eq": [
                7
            ],
            "finset": [
                7
            ],
            "from": [
                7
            ],
            "gt": [
                7
            ],
            "gteq": [
                7
            ],
            "in": [
                7
            ],
            "like": [
                7
            ],
            "lt": [
                7
            ],
            "lteq": [
                7
            ],
            "moreq": [
                7
            ],
            "neq": [
                7
            ],
            "nin": [
                7
            ],
            "notnull": [
                7
            ],
            "null": [
                7
            ],
            "to": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PickupLocationSortInput": {
            "city": [
                42
            ],
            "contact_name": [
                42
            ],
            "country_id": [
                42
            ],
            "description": [
                42
            ],
            "distance": [
                42
            ],
            "email": [
                42
            ],
            "fax": [
                42
            ],
            "latitude": [
                42
            ],
            "longitude": [
                42
            ],
            "name": [
                42
            ],
            "phone": [
                42
            ],
            "pickup_location_code": [
                42
            ],
            "postcode": [
                42
            ],
            "region": [
                42
            ],
            "region_id": [
                42
            ],
            "street": [
                42
            ],
            "__typename": [
                7
            ]
        },
        "ProductInfoInput": {
            "sku": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PickupLocations": {
            "items": [
                203
            ],
            "page_info": [
                44
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "PickupLocation": {
            "city": [
                7
            ],
            "contact_name": [
                7
            ],
            "country_id": [
                7
            ],
            "description": [
                7
            ],
            "email": [
                7
            ],
            "fax": [
                7
            ],
            "latitude": [
                20
            ],
            "longitude": [
                20
            ],
            "name": [
                7
            ],
            "phone": [
                7
            ],
            "pickup_location_code": [
                7
            ],
            "postcode": [
                7
            ],
            "region": [
                7
            ],
            "region_id": [
                9
            ],
            "street": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductReviewRatingsMetadata": {
            "items": [
                205
            ],
            "__typename": [
                7
            ]
        },
        "ProductReviewRatingMetadata": {
            "id": [
                7
            ],
            "name": [
                7
            ],
            "values": [
                206
            ],
            "__typename": [
                7
            ]
        },
        "ProductReviewRatingValueMetadata": {
            "value": [
                7
            ],
            "value_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductAttributeFilterInput": {
            "category_id": [
                83
            ],
            "category_uid": [
                83
            ],
            "description": [
                84
            ],
            "fashion_color": [
                83
            ],
            "fashion_material": [
                83
            ],
            "fashion_size": [
                83
            ],
            "fashion_style": [
                83
            ],
            "format": [
                83
            ],
            "has_video": [
                83
            ],
            "name": [
                84
            ],
            "price": [
                208
            ],
            "short_description": [
                84
            ],
            "sku": [
                83
            ],
            "url_key": [
                83
            ],
            "__typename": [
                7
            ]
        },
        "FilterRangeTypeInput": {
            "from": [
                7
            ],
            "to": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Products": {
            "aggregations": [
                212,
                {
                    "filter": [
                        210
                    ]
                }
            ],
            "filters": [
                215
            ],
            "items": [
                37
            ],
            "page_info": [
                44
            ],
            "sort_fields": [
                217
            ],
            "suggestions": [
                219
            ],
            "total_count": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "AggregationsFilterInput": {
            "category": [
                211
            ],
            "__typename": [
                7
            ]
        },
        "AggregationsCategoryFilterInput": {
            "includeDirectChildrenOnly": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "Aggregation": {
            "attribute_code": [
                7
            ],
            "count": [
                9
            ],
            "label": [
                7
            ],
            "options": [
                213
            ],
            "position": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "AggregationOption": {
            "count": [
                9
            ],
            "label": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AggregationOptionInterface": {
            "count": [
                9
            ],
            "label": [
                7
            ],
            "value": [
                7
            ],
            "on_AggregationOption": [
                213
            ],
            "__typename": [
                7
            ]
        },
        "LayerFilter": {
            "filter_items": [
                216
            ],
            "filter_items_count": [
                9
            ],
            "name": [
                7
            ],
            "request_var": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "LayerFilterItemInterface": {
            "items_count": [
                9
            ],
            "label": [
                7
            ],
            "value_string": [
                7
            ],
            "on_LayerFilterItem": [
                430
            ],
            "on_SwatchLayerFilterItem": [
                492
            ],
            "__typename": [
                7
            ]
        },
        "SortFields": {
            "default": [
                7
            ],
            "options": [
                218
            ],
            "__typename": [
                7
            ]
        },
        "SortField": {
            "label": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SearchSuggestion": {
            "search": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ReCaptchaConfigurationV3": {
            "badge_position": [
                7
            ],
            "failure_message": [
                7
            ],
            "forms": [
                221
            ],
            "language_code": [
                7
            ],
            "minimum_score": [
                20
            ],
            "website_key": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ReCaptchaFormEnum": {},
        "EntityUrl": {
            "canonical_url": [
                7
            ],
            "entity_uid": [
                2
            ],
            "id": [
                9
            ],
            "redirectCode": [
                9
            ],
            "relative_url": [
                7
            ],
            "type": [
                88
            ],
            "__typename": [
                7
            ]
        },
        "WishlistOutput": {
            "items": [
                170
            ],
            "items_count": [
                9
            ],
            "name": [
                7
            ],
            "sharing_code": [
                7
            ],
            "updated_at": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "Mutation": {
            "addBundleProductsToCart": [
                231,
                {
                    "input": [
                        225
                    ]
                }
            ],
            "addConfigurableProductsToCart": [
                234,
                {
                    "input": [
                        232
                    ]
                }
            ],
            "addDownloadableProductsToCart": [
                238,
                {
                    "input": [
                        235
                    ]
                }
            ],
            "addGiftRegistryRegistrants": [
                241,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "registrants": [
                        239,
                        "[AddGiftRegistryRegistrantInput!]!"
                    ]
                }
            ],
            "addProductsToCart": [
                242,
                {
                    "cartId": [
                        7,
                        "String!"
                    ],
                    "cartItems": [
                        229,
                        "[CartItemInput!]!"
                    ]
                }
            ],
            "addProductsToCompareList": [
                93,
                {
                    "input": [
                        245
                    ]
                }
            ],
            "addProductsToWishlist": [
                247,
                {
                    "wishlistId": [
                        2,
                        "ID!"
                    ],
                    "wishlistItems": [
                        246,
                        "[WishlistItemInput!]!"
                    ]
                }
            ],
            "addReturnComment": [
                251,
                {
                    "input": [
                        250,
                        "AddReturnCommentInput!"
                    ]
                }
            ],
            "addReturnTracking": [
                253,
                {
                    "input": [
                        252,
                        "AddReturnTrackingInput!"
                    ]
                }
            ],
            "addSimpleProductsToCart": [
                256,
                {
                    "input": [
                        254
                    ]
                }
            ],
            "addVirtualProductsToCart": [
                259,
                {
                    "input": [
                        257
                    ]
                }
            ],
            "addWishlistItemsToCart": [
                260,
                {
                    "wishlistId": [
                        2,
                        "ID!"
                    ],
                    "wishlistItemIds": [
                        2,
                        "[ID!]"
                    ]
                }
            ],
            "applyCouponToCart": [
                264,
                {
                    "input": [
                        263
                    ]
                }
            ],
            "applyGiftCardToCart": [
                266,
                {
                    "input": [
                        265
                    ]
                }
            ],
            "applyRewardPointsToCart": [
                267,
                {
                    "cartId": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "applyStoreCreditToCart": [
                269,
                {
                    "input": [
                        268,
                        "ApplyStoreCreditToCartInput!"
                    ]
                }
            ],
            "assignCompareListToCustomer": [
                270,
                {
                    "uid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "assignCustomerToGuestCart": [
                15,
                {
                    "cart_id": [
                        7,
                        "String!"
                    ]
                }
            ],
            "changeCustomerPassword": [
                107,
                {
                    "currentPassword": [
                        7,
                        "String!"
                    ],
                    "newPassword": [
                        7,
                        "String!"
                    ]
                }
            ],
            "contactUs": [
                272,
                {
                    "input": [
                        271,
                        "ContactUsInput!"
                    ]
                }
            ],
            "copyProductsBetweenWishlists": [
                274,
                {
                    "sourceWishlistUid": [
                        2,
                        "ID!"
                    ],
                    "destinationWishlistUid": [
                        2,
                        "ID!"
                    ],
                    "wishlistItems": [
                        273,
                        "[WishlistItemCopyInput!]!"
                    ]
                }
            ],
            "createBraintreeClientToken": [
                7
            ],
            "createCompareList": [
                93,
                {
                    "input": [
                        275
                    ]
                }
            ],
            "createCustomer": [
                277,
                {
                    "input": [
                        276,
                        "CustomerInput!"
                    ]
                }
            ],
            "createCustomerAddress": [
                108,
                {
                    "input": [
                        278,
                        "CustomerAddressInput!"
                    ]
                }
            ],
            "createCustomerV2": [
                277,
                {
                    "input": [
                        281,
                        "CustomerCreateInput!"
                    ]
                }
            ],
            "createEmptyCart": [
                7,
                {
                    "input": [
                        282
                    ]
                }
            ],
            "createGiftRegistry": [
                285,
                {
                    "giftRegistry": [
                        283,
                        "CreateGiftRegistryInput!"
                    ]
                }
            ],
            "createPayflowProToken": [
                288,
                {
                    "input": [
                        286,
                        "PayflowProTokenInput!"
                    ]
                }
            ],
            "createPaypalExpressToken": [
                291,
                {
                    "input": [
                        289,
                        "PaypalExpressTokenInput!"
                    ]
                }
            ],
            "createProductReview": [
                295,
                {
                    "input": [
                        293,
                        "CreateProductReviewInput!"
                    ]
                }
            ],
            "createWishlist": [
                297,
                {
                    "input": [
                        296,
                        "CreateWishlistInput!"
                    ]
                }
            ],
            "deleteCompareList": [
                298,
                {
                    "uid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "deleteCustomerAddress": [
                3,
                {
                    "id": [
                        9,
                        "Int!"
                    ]
                }
            ],
            "deletePaymentToken": [
                299,
                {
                    "public_hash": [
                        7,
                        "String!"
                    ]
                }
            ],
            "deleteWishlist": [
                300,
                {
                    "wishlistId": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "generateCustomerToken": [
                301,
                {
                    "email": [
                        7,
                        "String!"
                    ],
                    "password": [
                        7,
                        "String!"
                    ]
                }
            ],
            "generateCustomerTokenAsAdmin": [
                303,
                {
                    "input": [
                        302,
                        "GenerateCustomerTokenAsAdminInput!"
                    ]
                }
            ],
            "handlePayflowProResponse": [
                305,
                {
                    "input": [
                        304,
                        "PayflowProResponseInput!"
                    ]
                }
            ],
            "mergeCarts": [
                15,
                {
                    "source_cart_id": [
                        7,
                        "String!"
                    ],
                    "destination_cart_id": [
                        7
                    ]
                }
            ],
            "moveCartItemsToGiftRegistry": [
                306,
                {
                    "cartUid": [
                        2,
                        "ID!"
                    ],
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "moveProductsBetweenWishlists": [
                312,
                {
                    "sourceWishlistUid": [
                        2,
                        "ID!"
                    ],
                    "destinationWishlistUid": [
                        2,
                        "ID!"
                    ],
                    "wishlistItems": [
                        311,
                        "[WishlistItemMoveInput!]!"
                    ]
                }
            ],
            "placeOrder": [
                314,
                {
                    "input": [
                        313
                    ]
                }
            ],
            "redeemGiftCardBalanceAsStoreCredit": [
                194,
                {
                    "input": [
                        193,
                        "GiftCardAccountInput!"
                    ]
                }
            ],
            "removeCouponFromCart": [
                317,
                {
                    "input": [
                        316
                    ]
                }
            ],
            "removeGiftCardFromCart": [
                319,
                {
                    "input": [
                        318
                    ]
                }
            ],
            "removeGiftRegistry": [
                320,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "removeGiftRegistryItems": [
                321,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "itemsUid": [
                        2,
                        "[ID!]!"
                    ]
                }
            ],
            "removeGiftRegistryRegistrants": [
                322,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "registrantsUid": [
                        2,
                        "[ID!]!"
                    ]
                }
            ],
            "removeItemFromCart": [
                324,
                {
                    "input": [
                        323
                    ]
                }
            ],
            "removeProductsFromCompareList": [
                93,
                {
                    "input": [
                        325
                    ]
                }
            ],
            "removeProductsFromWishlist": [
                326,
                {
                    "wishlistId": [
                        2,
                        "ID!"
                    ],
                    "wishlistItemsIds": [
                        2,
                        "[ID!]!"
                    ]
                }
            ],
            "removeReturnTracking": [
                328,
                {
                    "input": [
                        327,
                        "RemoveReturnTrackingInput!"
                    ]
                }
            ],
            "removeRewardPointsFromCart": [
                329,
                {
                    "cartId": [
                        2,
                        "ID!"
                    ]
                }
            ],
            "removeStoreCreditFromCart": [
                331,
                {
                    "input": [
                        330,
                        "RemoveStoreCreditFromCartInput!"
                    ]
                }
            ],
            "reorderItems": [
                332,
                {
                    "orderNumber": [
                        7,
                        "String!"
                    ]
                }
            ],
            "requestPasswordResetEmail": [
                3,
                {
                    "email": [
                        7,
                        "String!"
                    ]
                }
            ],
            "requestReturn": [
                339,
                {
                    "input": [
                        335,
                        "RequestReturnInput!"
                    ]
                }
            ],
            "resetPassword": [
                3,
                {
                    "email": [
                        7,
                        "String!"
                    ],
                    "resetPasswordToken": [
                        7,
                        "String!"
                    ],
                    "newPassword": [
                        7,
                        "String!"
                    ]
                }
            ],
            "revokeCustomerToken": [
                340
            ],
            "sendEmailToFriend": [
                344,
                {
                    "input": [
                        341
                    ]
                }
            ],
            "setBillingAddressOnCart": [
                350,
                {
                    "input": [
                        347
                    ]
                }
            ],
            "setGiftOptionsOnCart": [
                353,
                {
                    "input": [
                        351
                    ]
                }
            ],
            "setGuestEmailOnCart": [
                355,
                {
                    "input": [
                        354
                    ]
                }
            ],
            "setPaymentMethodAndPlaceOrder": [
                314,
                {
                    "input": [
                        356
                    ]
                }
            ],
            "setPaymentMethodOnCart": [
                368,
                {
                    "input": [
                        367
                    ]
                }
            ],
            "setShippingAddressesOnCart": [
                371,
                {
                    "input": [
                        369
                    ]
                }
            ],
            "setShippingMethodsOnCart": [
                374,
                {
                    "input": [
                        372
                    ]
                }
            ],
            "shareGiftRegistry": [
                377,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "sender": [
                        375,
                        "ShareGiftRegistrySenderInput!"
                    ],
                    "invitees": [
                        376,
                        "[ShareGiftRegistryInviteeInput!]!"
                    ]
                }
            ],
            "subscribeEmailToNewsletter": [
                378,
                {
                    "email": [
                        7,
                        "String!"
                    ]
                }
            ],
            "updateCartItems": [
                382,
                {
                    "input": [
                        380
                    ]
                }
            ],
            "updateCustomer": [
                277,
                {
                    "input": [
                        276,
                        "CustomerInput!"
                    ]
                }
            ],
            "updateCustomerAddress": [
                108,
                {
                    "id": [
                        9,
                        "Int!"
                    ],
                    "input": [
                        278
                    ]
                }
            ],
            "updateCustomerEmail": [
                277,
                {
                    "email": [
                        7,
                        "String!"
                    ],
                    "password": [
                        7,
                        "String!"
                    ]
                }
            ],
            "updateCustomerV2": [
                277,
                {
                    "input": [
                        383,
                        "CustomerUpdateInput!"
                    ]
                }
            ],
            "updateGiftRegistry": [
                385,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "giftRegistry": [
                        384,
                        "UpdateGiftRegistryInput!"
                    ]
                }
            ],
            "updateGiftRegistryItems": [
                387,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "items": [
                        386,
                        "[UpdateGiftRegistryItemInput!]!"
                    ]
                }
            ],
            "updateGiftRegistryRegistrants": [
                389,
                {
                    "giftRegistryUid": [
                        2,
                        "ID!"
                    ],
                    "registrants": [
                        388,
                        "[UpdateGiftRegistryRegistrantInput!]!"
                    ]
                }
            ],
            "updateProductsInWishlist": [
                391,
                {
                    "wishlistId": [
                        2,
                        "ID!"
                    ],
                    "wishlistItems": [
                        390,
                        "[WishlistItemUpdateInput!]!"
                    ]
                }
            ],
            "updateWishlist": [
                392,
                {
                    "wishlistId": [
                        2,
                        "ID!"
                    ],
                    "name": [
                        7
                    ],
                    "visibility": [
                        177
                    ]
                }
            ],
            "__typename": [
                7
            ]
        },
        "AddBundleProductsToCartInput": {
            "cart_id": [
                7
            ],
            "cart_items": [
                226
            ],
            "__typename": [
                7
            ]
        },
        "BundleProductCartItemInput": {
            "bundle_options": [
                227
            ],
            "customizable_options": [
                228
            ],
            "data": [
                229
            ],
            "__typename": [
                7
            ]
        },
        "BundleOptionInput": {
            "id": [
                9
            ],
            "quantity": [
                20
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableOptionInput": {
            "id": [
                9
            ],
            "value_string": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartItemInput": {
            "entered_options": [
                230
            ],
            "parent_sku": [
                7
            ],
            "quantity": [
                20
            ],
            "selected_options": [
                2
            ],
            "sku": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "EnteredOptionInput": {
            "uid": [
                2
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AddBundleProductsToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "AddConfigurableProductsToCartInput": {
            "cart_id": [
                7
            ],
            "cart_items": [
                233
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProductCartItemInput": {
            "customizable_options": [
                228
            ],
            "data": [
                229
            ],
            "parent_sku": [
                7
            ],
            "variant_sku": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AddConfigurableProductsToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "AddDownloadableProductsToCartInput": {
            "cart_id": [
                7
            ],
            "cart_items": [
                236
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableProductCartItemInput": {
            "customizable_options": [
                228
            ],
            "data": [
                229
            ],
            "downloadable_product_links": [
                237
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableProductLinksInput": {
            "link_id": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "AddDownloadableProductsToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "AddGiftRegistryRegistrantInput": {
            "dynamic_attributes": [
                240
            ],
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryDynamicAttributeInput": {
            "code": [
                2
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AddGiftRegistryRegistrantsOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "AddProductsToCartOutput": {
            "cart": [
                15
            ],
            "user_errors": [
                243
            ],
            "__typename": [
                7
            ]
        },
        "CartUserInputError": {
            "code": [
                244
            ],
            "message": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CartUserInputErrorType": {},
        "AddProductsToCompareListInput": {
            "products": [
                2
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "WishlistItemInput": {
            "entered_options": [
                230
            ],
            "parent_sku": [
                7
            ],
            "quantity": [
                20
            ],
            "selected_options": [
                2
            ],
            "sku": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AddProductsToWishlistOutput": {
            "user_errors": [
                248
            ],
            "wishlist": [
                169
            ],
            "__typename": [
                7
            ]
        },
        "WishListUserInputError": {
            "code": [
                249
            ],
            "message": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "WishListUserInputErrorType": {},
        "AddReturnCommentInput": {
            "comment_text": [
                7
            ],
            "return_uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "AddReturnCommentOutput": {
            "return": [
                143
            ],
            "__typename": [
                7
            ]
        },
        "AddReturnTrackingInput": {
            "carrier_uid": [
                2
            ],
            "return_uid": [
                2
            ],
            "tracking_number": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AddReturnTrackingOutput": {
            "return": [
                143
            ],
            "return_shipping_tracking": [
                152
            ],
            "__typename": [
                7
            ]
        },
        "AddSimpleProductsToCartInput": {
            "cart_id": [
                7
            ],
            "cart_items": [
                255
            ],
            "__typename": [
                7
            ]
        },
        "SimpleProductCartItemInput": {
            "customizable_options": [
                228
            ],
            "data": [
                229
            ],
            "__typename": [
                7
            ]
        },
        "AddSimpleProductsToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "AddVirtualProductsToCartInput": {
            "cart_id": [
                7
            ],
            "cart_items": [
                258
            ],
            "__typename": [
                7
            ]
        },
        "VirtualProductCartItemInput": {
            "customizable_options": [
                228
            ],
            "data": [
                229
            ],
            "__typename": [
                7
            ]
        },
        "AddVirtualProductsToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "AddWishlistItemsToCartOutput": {
            "add_wishlist_items_to_cart_user_errors": [
                261
            ],
            "status": [
                3
            ],
            "wishlist": [
                169
            ],
            "__typename": [
                7
            ]
        },
        "WishlistCartUserInputError": {
            "code": [
                262
            ],
            "message": [
                7
            ],
            "wishlistId": [
                2
            ],
            "wishlistItemId": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "WishlistCartUserInputErrorType": {},
        "ApplyCouponToCartInput": {
            "cart_id": [
                7
            ],
            "coupon_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ApplyCouponToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "ApplyGiftCardToCartInput": {
            "cart_id": [
                7
            ],
            "gift_card_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ApplyGiftCardToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "ApplyRewardPointsToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "ApplyStoreCreditToCartInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ApplyStoreCreditToCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "AssignCompareListToCustomerOutput": {
            "compare_list": [
                93
            ],
            "result": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "ContactUsInput": {
            "comment": [
                7
            ],
            "email": [
                7
            ],
            "name": [
                7
            ],
            "telephone": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ContactUsOutput": {
            "status": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "WishlistItemCopyInput": {
            "quantity": [
                20
            ],
            "wishlist_item_id": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CopyProductsBetweenWishlistsOutput": {
            "destination_wishlist": [
                169
            ],
            "source_wishlist": [
                169
            ],
            "user_errors": [
                248
            ],
            "__typename": [
                7
            ]
        },
        "CreateCompareListInput": {
            "products": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomerInput": {
            "date_of_birth": [
                7
            ],
            "dob": [
                7
            ],
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "gender": [
                9
            ],
            "is_subscribed": [
                3
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "password": [
                7
            ],
            "prefix": [
                7
            ],
            "suffix": [
                7
            ],
            "taxvat": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomerOutput": {
            "customer": [
                107
            ],
            "__typename": [
                7
            ]
        },
        "CustomerAddressInput": {
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country_code": [
                109
            ],
            "country_id": [
                109
            ],
            "custom_attributes": [
                279
            ],
            "default_billing": [
                3
            ],
            "default_shipping": [
                3
            ],
            "fax": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "postcode": [
                7
            ],
            "prefix": [
                7
            ],
            "region": [
                280
            ],
            "street": [
                7
            ],
            "suffix": [
                7
            ],
            "telephone": [
                7
            ],
            "vat_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomerAddressAttributeInput": {
            "attribute_code": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CustomerAddressRegionInput": {
            "region": [
                7
            ],
            "region_code": [
                7
            ],
            "region_id": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "CustomerCreateInput": {
            "allow_remote_shopping_assistance": [
                3
            ],
            "date_of_birth": [
                7
            ],
            "dob": [
                7
            ],
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "gender": [
                9
            ],
            "is_subscribed": [
                3
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "password": [
                7
            ],
            "prefix": [
                7
            ],
            "suffix": [
                7
            ],
            "taxvat": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "createEmptyCartInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CreateGiftRegistryInput": {
            "dynamic_attributes": [
                240
            ],
            "event_name": [
                7
            ],
            "gift_registry_type_uid": [
                2
            ],
            "message": [
                7
            ],
            "privacy_settings": [
                117
            ],
            "registrants": [
                239
            ],
            "shipping_address": [
                284
            ],
            "status": [
                120
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryShippingAddressInput": {
            "address_data": [
                278
            ],
            "address_id": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CreateGiftRegistryOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "PayflowProTokenInput": {
            "cart_id": [
                7
            ],
            "urls": [
                287
            ],
            "__typename": [
                7
            ]
        },
        "PayflowProUrlInput": {
            "cancel_url": [
                7
            ],
            "error_url": [
                7
            ],
            "return_url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CreatePayflowProTokenOutput": {
            "response_message": [
                7
            ],
            "result": [
                9
            ],
            "result_code": [
                9
            ],
            "secure_token": [
                7
            ],
            "secure_token_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PaypalExpressTokenInput": {
            "cart_id": [
                7
            ],
            "code": [
                7
            ],
            "express_button": [
                3
            ],
            "urls": [
                290
            ],
            "use_paypal_credit": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "PaypalExpressUrlsInput": {
            "cancel_url": [
                7
            ],
            "pending_url": [
                7
            ],
            "return_url": [
                7
            ],
            "success_url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PaypalExpressTokenOutput": {
            "paypal_urls": [
                292
            ],
            "token": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PaypalExpressUrlList": {
            "edit": [
                7
            ],
            "start": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CreateProductReviewInput": {
            "nickname": [
                7
            ],
            "ratings": [
                294
            ],
            "sku": [
                7
            ],
            "summary": [
                7
            ],
            "text": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ProductReviewRatingInput": {
            "id": [
                7
            ],
            "value_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CreateProductReviewOutput": {
            "review": [
                66
            ],
            "__typename": [
                7
            ]
        },
        "CreateWishlistInput": {
            "name": [
                7
            ],
            "visibility": [
                177
            ],
            "__typename": [
                7
            ]
        },
        "CreateWishlistOutput": {
            "wishlist": [
                169
            ],
            "__typename": [
                7
            ]
        },
        "DeleteCompareListOutput": {
            "result": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "DeletePaymentTokenOutput": {
            "customerPaymentTokens": [
                180
            ],
            "result": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "DeleteWishlistOutput": {
            "status": [
                3
            ],
            "wishlists": [
                169
            ],
            "__typename": [
                7
            ]
        },
        "CustomerToken": {
            "token": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GenerateCustomerTokenAsAdminInput": {
            "customer_email": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GenerateCustomerTokenAsAdminOutput": {
            "customer_token": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowProResponseInput": {
            "cart_id": [
                7
            ],
            "paypal_payload": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowProResponseOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "MoveCartItemsToGiftRegistryOutput": {
            "gift_registry": [
                112
            ],
            "status": [
                3
            ],
            "user_errors": [
                309
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryOutputInterface": {
            "gift_registry": [
                112
            ],
            "on_MoveCartItemsToGiftRegistryOutput": [
                306
            ],
            "on_GiftRegistryOutput": [
                454
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryItemUserErrorInterface": {
            "status": [
                3
            ],
            "user_errors": [
                309
            ],
            "on_MoveCartItemsToGiftRegistryOutput": [
                306
            ],
            "on_GiftRegistryItemUserErrors": [
                455
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryItemsUserError": {
            "code": [
                310
            ],
            "gift_registry_item_uid": [
                2
            ],
            "gift_registry_uid": [
                2
            ],
            "message": [
                7
            ],
            "product_uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryItemsUserErrorType": {},
        "WishlistItemMoveInput": {
            "quantity": [
                20
            ],
            "wishlist_item_id": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "MoveProductsBetweenWishlistsOutput": {
            "destination_wishlist": [
                169
            ],
            "source_wishlist": [
                169
            ],
            "user_errors": [
                248
            ],
            "__typename": [
                7
            ]
        },
        "PlaceOrderInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PlaceOrderOutput": {
            "order": [
                315
            ],
            "__typename": [
                7
            ]
        },
        "Order": {
            "order_id": [
                7
            ],
            "order_number": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "RemoveCouponFromCartInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "RemoveCouponFromCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "RemoveGiftCardFromCartInput": {
            "cart_id": [
                7
            ],
            "gift_card_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "RemoveGiftCardFromCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "RemoveGiftRegistryOutput": {
            "success": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "RemoveGiftRegistryItemsOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "RemoveGiftRegistryRegistrantsOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "RemoveItemFromCartInput": {
            "cart_id": [
                7
            ],
            "cart_item_id": [
                9
            ],
            "cart_item_uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "RemoveItemFromCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "RemoveProductsFromCompareListInput": {
            "products": [
                2
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "RemoveProductsFromWishlistOutput": {
            "user_errors": [
                248
            ],
            "wishlist": [
                169
            ],
            "__typename": [
                7
            ]
        },
        "RemoveReturnTrackingInput": {
            "return_shipping_tracking_uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "RemoveReturnTrackingOutput": {
            "return": [
                143
            ],
            "__typename": [
                7
            ]
        },
        "RemoveRewardPointsFromCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "RemoveStoreCreditFromCartInput": {
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "RemoveStoreCreditFromCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "ReorderItemsOutput": {
            "cart": [
                15
            ],
            "userInputErrors": [
                333
            ],
            "__typename": [
                7
            ]
        },
        "CheckoutUserInputError": {
            "code": [
                334
            ],
            "message": [
                7
            ],
            "path": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "CheckoutUserInputErrorCodes": {},
        "RequestReturnInput": {
            "comment_text": [
                7
            ],
            "contact_email": [
                7
            ],
            "items": [
                336
            ],
            "order_uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "RequestReturnItemInput": {
            "entered_custom_attributes": [
                337
            ],
            "order_item_uid": [
                2
            ],
            "quantity_to_return": [
                20
            ],
            "selected_custom_attributes": [
                338
            ],
            "__typename": [
                7
            ]
        },
        "EnteredCustomAttributeInput": {
            "attribute_code": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SelectedCustomAttributeInput": {
            "attribute_code": [
                7
            ],
            "value": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "RequestReturnOutput": {
            "return": [
                143
            ],
            "returns": [
                142,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "__typename": [
                7
            ]
        },
        "RevokeCustomerTokenOutput": {
            "result": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "SendEmailToFriendInput": {
            "product_id": [
                9
            ],
            "recipients": [
                342
            ],
            "sender": [
                343
            ],
            "__typename": [
                7
            ]
        },
        "SendEmailToFriendRecipientInput": {
            "email": [
                7
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SendEmailToFriendSenderInput": {
            "email": [
                7
            ],
            "message": [
                7
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SendEmailToFriendOutput": {
            "recipients": [
                345
            ],
            "sender": [
                346
            ],
            "__typename": [
                7
            ]
        },
        "SendEmailToFriendRecipient": {
            "email": [
                7
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SendEmailToFriendSender": {
            "email": [
                7
            ],
            "message": [
                7
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetBillingAddressOnCartInput": {
            "billing_address": [
                348
            ],
            "cart_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "BillingAddressInput": {
            "address": [
                349
            ],
            "customer_address_id": [
                9
            ],
            "same_as_shipping": [
                3
            ],
            "use_for_shipping": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "CartAddressInput": {
            "city": [
                7
            ],
            "company": [
                7
            ],
            "country_code": [
                7
            ],
            "firstname": [
                7
            ],
            "lastname": [
                7
            ],
            "postcode": [
                7
            ],
            "region": [
                7
            ],
            "region_id": [
                9
            ],
            "save_in_address_book": [
                3
            ],
            "street": [
                7
            ],
            "telephone": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetBillingAddressOnCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "SetGiftOptionsOnCartInput": {
            "cart_id": [
                7
            ],
            "gift_message": [
                352
            ],
            "gift_receipt_included": [
                3
            ],
            "gift_wrapping_id": [
                2
            ],
            "printed_card_included": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "GiftMessageInput": {
            "from": [
                7
            ],
            "message": [
                7
            ],
            "to": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetGiftOptionsOnCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "SetGuestEmailOnCartInput": {
            "cart_id": [
                7
            ],
            "email": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetGuestEmailOnCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "SetPaymentMethodAndPlaceOrderInput": {
            "cart_id": [
                7
            ],
            "payment_method": [
                357
            ],
            "__typename": [
                7
            ]
        },
        "PaymentMethodInput": {
            "braintree": [
                358
            ],
            "braintree_cc_vault": [
                359
            ],
            "code": [
                7
            ],
            "hosted_pro": [
                360
            ],
            "payflow_express": [
                361
            ],
            "payflow_link": [
                362
            ],
            "payflowpro": [
                363
            ],
            "payflowpro_cc_vault": [
                365
            ],
            "paypal_express": [
                366
            ],
            "purchase_order_number": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "BraintreeInput": {
            "device_data": [
                7
            ],
            "is_active_payment_token_enabler": [
                3
            ],
            "payment_method_nonce": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "BraintreeCcVaultInput": {
            "device_data": [
                7
            ],
            "public_hash": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "HostedProInput": {
            "cancel_url": [
                7
            ],
            "return_url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowExpressInput": {
            "payer_id": [
                7
            ],
            "token": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowLinkInput": {
            "cancel_url": [
                7
            ],
            "error_url": [
                7
            ],
            "return_url": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowProInput": {
            "cc_details": [
                364
            ],
            "is_active_payment_token_enabler": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "CreditCardDetailsInput": {
            "cc_exp_month": [
                9
            ],
            "cc_exp_year": [
                9
            ],
            "cc_last_4": [
                9
            ],
            "cc_type": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "VaultTokenInput": {
            "public_hash": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PaypalExpressInput": {
            "payer_id": [
                7
            ],
            "token": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetPaymentMethodOnCartInput": {
            "cart_id": [
                7
            ],
            "payment_method": [
                357
            ],
            "__typename": [
                7
            ]
        },
        "SetPaymentMethodOnCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "SetShippingAddressesOnCartInput": {
            "cart_id": [
                7
            ],
            "shipping_addresses": [
                370
            ],
            "__typename": [
                7
            ]
        },
        "ShippingAddressInput": {
            "address": [
                349
            ],
            "customer_address_id": [
                9
            ],
            "customer_notes": [
                7
            ],
            "pickup_location_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetShippingAddressesOnCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "SetShippingMethodsOnCartInput": {
            "cart_id": [
                7
            ],
            "shipping_methods": [
                373
            ],
            "__typename": [
                7
            ]
        },
        "ShippingMethodInput": {
            "carrier_code": [
                7
            ],
            "method_code": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SetShippingMethodsOnCartOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "ShareGiftRegistrySenderInput": {
            "message": [
                7
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ShareGiftRegistryInviteeInput": {
            "email": [
                7
            ],
            "name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ShareGiftRegistryOutput": {
            "is_shared": [
                3
            ],
            "__typename": [
                7
            ]
        },
        "SubscribeEmailToNewsletterOutput": {
            "status": [
                379
            ],
            "__typename": [
                7
            ]
        },
        "SubscriptionStatusesEnum": {},
        "UpdateCartItemsInput": {
            "cart_id": [
                7
            ],
            "cart_items": [
                381
            ],
            "__typename": [
                7
            ]
        },
        "CartItemUpdateInput": {
            "cart_item_id": [
                9
            ],
            "cart_item_uid": [
                2
            ],
            "customizable_options": [
                228
            ],
            "gift_message": [
                352
            ],
            "gift_wrapping_id": [
                2
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "UpdateCartItemsOutput": {
            "cart": [
                15
            ],
            "__typename": [
                7
            ]
        },
        "CustomerUpdateInput": {
            "allow_remote_shopping_assistance": [
                3
            ],
            "date_of_birth": [
                7
            ],
            "dob": [
                7
            ],
            "firstname": [
                7
            ],
            "gender": [
                9
            ],
            "is_subscribed": [
                3
            ],
            "lastname": [
                7
            ],
            "middlename": [
                7
            ],
            "prefix": [
                7
            ],
            "suffix": [
                7
            ],
            "taxvat": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "UpdateGiftRegistryInput": {
            "dynamic_attributes": [
                240
            ],
            "event_name": [
                7
            ],
            "message": [
                7
            ],
            "privacy_settings": [
                117
            ],
            "shipping_address": [
                284
            ],
            "status": [
                120
            ],
            "__typename": [
                7
            ]
        },
        "UpdateGiftRegistryOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "UpdateGiftRegistryItemInput": {
            "gift_registry_item_uid": [
                2
            ],
            "note": [
                7
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "UpdateGiftRegistryItemsOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "UpdateGiftRegistryRegistrantInput": {
            "dynamic_attributes": [
                240
            ],
            "email": [
                7
            ],
            "firstname": [
                7
            ],
            "gift_registry_registrant_uid": [
                2
            ],
            "lastname": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "UpdateGiftRegistryRegistrantsOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "WishlistItemUpdateInput": {
            "description": [
                7
            ],
            "entered_options": [
                230
            ],
            "quantity": [
                20
            ],
            "selected_options": [
                2
            ],
            "wishlist_item_id": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "UpdateProductsInWishlistOutput": {
            "user_errors": [
                248
            ],
            "wishlist": [
                169
            ],
            "__typename": [
                7
            ]
        },
        "UpdateWishlistOutput": {
            "name": [
                7
            ],
            "uid": [
                2
            ],
            "visibility": [
                177
            ],
            "__typename": [
                7
            ]
        },
        "BatchMutationStatus": {},
        "ErrorInterface": {
            "message": [
                7
            ],
            "on_NoSuchEntityUidError": [
                395
            ],
            "on_InternalError": [
                396
            ],
            "__typename": [
                7
            ]
        },
        "NoSuchEntityUidError": {
            "message": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "InternalError": {
            "message": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "AttributeOptionsInterface": {
            "attribute_options": [
                48
            ],
            "on_AttributeOptions": [
                399
            ],
            "on_UiAttributeTypeSelect": [
                400
            ],
            "on_UiAttributeTypeMultiSelect": [
                401
            ],
            "on_UiAttributeTypeBoolean": [
                402
            ],
            "__typename": [
                7
            ]
        },
        "SelectableInputTypeInterface": {
            "attribute_options": [
                48
            ],
            "on_UiAttributeTypeSelect": [
                400
            ],
            "on_UiAttributeTypeMultiSelect": [
                401
            ],
            "on_UiAttributeTypeBoolean": [
                402
            ],
            "__typename": [
                7
            ]
        },
        "AttributeOptions": {
            "attribute_options": [
                48
            ],
            "__typename": [
                7
            ]
        },
        "UiAttributeTypeSelect": {
            "attribute_options": [
                48
            ],
            "is_html_allowed": [
                3
            ],
            "ui_input_type": [
                11
            ],
            "__typename": [
                7
            ]
        },
        "UiAttributeTypeMultiSelect": {
            "attribute_options": [
                48
            ],
            "is_html_allowed": [
                3
            ],
            "ui_input_type": [
                11
            ],
            "__typename": [
                7
            ]
        },
        "UiAttributeTypeBoolean": {
            "attribute_options": [
                48
            ],
            "is_html_allowed": [
                3
            ],
            "ui_input_type": [
                11
            ],
            "__typename": [
                7
            ]
        },
        "UiAttributeTypeAny": {
            "is_html_allowed": [
                3
            ],
            "ui_input_type": [
                11
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableDateTypeEnum": {},
        "ProductLinks": {
            "link_type": [
                7
            ],
            "linked_product_sku": [
                7
            ],
            "linked_product_type": [
                7
            ],
            "position": [
                9
            ],
            "sku": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PhysicalProductInterface": {
            "weight": [
                20
            ],
            "on_SimpleProduct": [
                427
            ],
            "on_BundleProduct": [
                460
            ],
            "on_GroupedProduct": [
                470
            ],
            "on_ConfigurableProduct": [
                473
            ],
            "on_GiftCardProduct": [
                497
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableAreaOption": {
            "option_id": [
                9
            ],
            "product_sku": [
                7
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                409
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableOptionInterface": {
            "option_id": [
                9
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "on_CustomizableAreaOption": [
                407
            ],
            "on_CustomizableDateOption": [
                410
            ],
            "on_CustomizableDropDownOption": [
                412
            ],
            "on_CustomizableMultipleOption": [
                414
            ],
            "on_CustomizableFieldOption": [
                416
            ],
            "on_CustomizableFileOption": [
                418
            ],
            "on_CustomizableRadioOption": [
                422
            ],
            "on_CustomizableCheckboxOption": [
                424
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableAreaValue": {
            "max_characters": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableDateOption": {
            "option_id": [
                9
            ],
            "product_sku": [
                7
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                411
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableDateValue": {
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "type": [
                404
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableDropDownOption": {
            "option_id": [
                9
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                413
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableDropDownValue": {
            "option_type_id": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableMultipleOption": {
            "option_id": [
                9
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                415
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableMultipleValue": {
            "option_type_id": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableFieldOption": {
            "option_id": [
                9
            ],
            "product_sku": [
                7
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                417
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableFieldValue": {
            "max_characters": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableFileOption": {
            "option_id": [
                9
            ],
            "product_sku": [
                7
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                419
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableFileValue": {
            "file_extension": [
                7
            ],
            "image_size_x": [
                9
            ],
            "image_size_y": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ProductVideo": {
            "disabled": [
                3
            ],
            "label": [
                7
            ],
            "position": [
                9
            ],
            "url": [
                7
            ],
            "video_content": [
                54
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableProductInterface": {
            "options": [
                408
            ],
            "on_VirtualProduct": [
                426
            ],
            "on_SimpleProduct": [
                427
            ],
            "on_DownloadableProduct": [
                439
            ],
            "on_BundleProduct": [
                460
            ],
            "on_ConfigurableProduct": [
                473
            ],
            "on_GiftCardProduct": [
                497
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableRadioOption": {
            "option_id": [
                9
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                423
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableRadioValue": {
            "option_type_id": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableCheckboxOption": {
            "option_id": [
                9
            ],
            "required": [
                3
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "value": [
                425
            ],
            "__typename": [
                7
            ]
        },
        "CustomizableCheckboxValue": {
            "option_type_id": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "sku": [
                7
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "VirtualProduct": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options": [
                408
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "__typename": [
                7
            ]
        },
        "SimpleProduct": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options": [
                408
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "weight": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "ProductFilterInput": {
            "category_id": [
                199
            ],
            "country_of_manufacture": [
                199
            ],
            "created_at": [
                199
            ],
            "custom_layout": [
                199
            ],
            "custom_layout_update": [
                199
            ],
            "description": [
                199
            ],
            "gift_message_available": [
                199
            ],
            "has_options": [
                199
            ],
            "image": [
                199
            ],
            "image_label": [
                199
            ],
            "is_returnable": [
                199
            ],
            "manufacturer": [
                199
            ],
            "max_price": [
                199
            ],
            "meta_description": [
                199
            ],
            "meta_keyword": [
                199
            ],
            "meta_title": [
                199
            ],
            "min_price": [
                199
            ],
            "name": [
                199
            ],
            "news_from_date": [
                199
            ],
            "news_to_date": [
                199
            ],
            "options_container": [
                199
            ],
            "or": [
                428
            ],
            "price": [
                199
            ],
            "required_options": [
                199
            ],
            "short_description": [
                199
            ],
            "sku": [
                199
            ],
            "small_image": [
                199
            ],
            "small_image_label": [
                199
            ],
            "special_from_date": [
                199
            ],
            "special_price": [
                199
            ],
            "special_to_date": [
                199
            ],
            "swatch_image": [
                199
            ],
            "thumbnail": [
                199
            ],
            "thumbnail_label": [
                199
            ],
            "tier_price": [
                199
            ],
            "updated_at": [
                199
            ],
            "url_key": [
                199
            ],
            "url_path": [
                199
            ],
            "weight": [
                199
            ],
            "__typename": [
                7
            ]
        },
        "ProductSortInput": {
            "country_of_manufacture": [
                42
            ],
            "created_at": [
                42
            ],
            "custom_layout": [
                42
            ],
            "custom_layout_update": [
                42
            ],
            "description": [
                42
            ],
            "gift_message_available": [
                42
            ],
            "has_options": [
                42
            ],
            "image": [
                42
            ],
            "image_label": [
                42
            ],
            "is_returnable": [
                42
            ],
            "manufacturer": [
                42
            ],
            "meta_description": [
                42
            ],
            "meta_keyword": [
                42
            ],
            "meta_title": [
                42
            ],
            "name": [
                42
            ],
            "news_from_date": [
                42
            ],
            "news_to_date": [
                42
            ],
            "options_container": [
                42
            ],
            "price": [
                42
            ],
            "required_options": [
                42
            ],
            "short_description": [
                42
            ],
            "sku": [
                42
            ],
            "small_image": [
                42
            ],
            "small_image_label": [
                42
            ],
            "special_from_date": [
                42
            ],
            "special_price": [
                42
            ],
            "special_to_date": [
                42
            ],
            "swatch_image": [
                42
            ],
            "thumbnail": [
                42
            ],
            "thumbnail_label": [
                42
            ],
            "tier_price": [
                42
            ],
            "updated_at": [
                42
            ],
            "url_key": [
                42
            ],
            "url_path": [
                42
            ],
            "weight": [
                42
            ],
            "__typename": [
                7
            ]
        },
        "LayerFilterItem": {
            "items_count": [
                9
            ],
            "label": [
                7
            ],
            "value_string": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SimpleWishlistItem": {
            "added_at": [
                7
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "VirtualWishlistItem": {
            "added_at": [
                7
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "SimpleCartItem": {
            "available_gift_wrapping": [
                23
            ],
            "customizable_options": [
                173
            ],
            "errors": [
                32
            ],
            "gift_message": [
                30
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                7
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "VirtualCartItem": {
            "customizable_options": [
                173
            ],
            "errors": [
                32
            ],
            "id": [
                7
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableCartItem": {
            "customizable_options": [
                173
            ],
            "errors": [
                32
            ],
            "id": [
                7
            ],
            "links": [
                436
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "samples": [
                438
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableProductLinks": {
            "id": [
                9
            ],
            "is_shareable": [
                3
            ],
            "link_type": [
                437
            ],
            "number_of_downloads": [
                9
            ],
            "price": [
                20
            ],
            "sample_file": [
                7
            ],
            "sample_type": [
                437
            ],
            "sample_url": [
                7
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableFileTypeEnum": {},
        "DownloadableProductSamples": {
            "id": [
                9
            ],
            "sample_file": [
                7
            ],
            "sample_type": [
                437
            ],
            "sample_url": [
                7
            ],
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableProduct": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "downloadable_product_links": [
                436
            ],
            "downloadable_product_samples": [
                438
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "links_purchased_separately": [
                9
            ],
            "links_title": [
                7
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options": [
                408
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableOrderItem": {
            "discounts": [
                35
            ],
            "downloadable_links": [
                441
            ],
            "eligible_for_return": [
                3
            ],
            "entered_options": [
                132
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                2
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "product_type": [
                7
            ],
            "product_url_key": [
                7
            ],
            "quantity_canceled": [
                20
            ],
            "quantity_invoiced": [
                20
            ],
            "quantity_ordered": [
                20
            ],
            "quantity_refunded": [
                20
            ],
            "quantity_returned": [
                20
            ],
            "quantity_shipped": [
                20
            ],
            "selected_options": [
                132
            ],
            "status": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableItemsLinks": {
            "sort_order": [
                9
            ],
            "title": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableInvoiceItem": {
            "discounts": [
                35
            ],
            "downloadable_links": [
                441
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_invoiced": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableCreditMemoItem": {
            "discounts": [
                35
            ],
            "downloadable_links": [
                441
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_refunded": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "DownloadableWishlistItem": {
            "added_at": [
                7
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "links_v2": [
                436
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "samples": [
                438
            ],
            "__typename": [
                7
            ]
        },
        "ProductAttributeMetadata": {
            "attribute_labels": [
                6
            ],
            "code": [
                7
            ],
            "data_type": [
                8
            ],
            "entity_type": [
                1
            ],
            "is_system": [
                3
            ],
            "label": [
                7
            ],
            "sort_order": [
                9
            ],
            "ui_input": [
                10
            ],
            "uid": [
                2
            ],
            "used_in_components": [
                446
            ],
            "__typename": [
                7
            ]
        },
        "CustomAttributesListsEnum": {},
        "ConfigurableCartItem": {
            "available_gift_wrapping": [
                23
            ],
            "configurable_options": [
                448
            ],
            "configured_variant": [
                37
            ],
            "customizable_options": [
                173
            ],
            "errors": [
                32
            ],
            "gift_message": [
                30
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                7
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "SelectedConfigurableOption": {
            "configurable_product_option_uid": [
                2
            ],
            "configurable_product_option_value_uid": [
                2
            ],
            "id": [
                9
            ],
            "option_label": [
                7
            ],
            "value_id": [
                9
            ],
            "value_label": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "BundleCartItem": {
            "available_gift_wrapping": [
                23
            ],
            "bundle_options": [
                450
            ],
            "customizable_options": [
                173
            ],
            "errors": [
                32
            ],
            "gift_message": [
                30
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                7
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "SelectedBundleOption": {
            "id": [
                9
            ],
            "label": [
                7
            ],
            "type": [
                7
            ],
            "uid": [
                2
            ],
            "values": [
                451
            ],
            "__typename": [
                7
            ]
        },
        "SelectedBundleOptionValue": {
            "id": [
                9
            ],
            "label": [
                7
            ],
            "price": [
                20
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "SalesItemInterface": {
            "gift_message": [
                30
            ],
            "__typename": [
                7
            ]
        },
        "AddGiftRegistryItemInput": {
            "entered_options": [
                230
            ],
            "note": [
                7
            ],
            "parent_sku": [
                7
            ],
            "quantity": [
                20
            ],
            "selected_options": [
                7
            ],
            "sku": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryOutput": {
            "gift_registry": [
                112
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryItemUserErrors": {
            "status": [
                3
            ],
            "user_errors": [
                309
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryDynamicAttributeMetadata": {
            "attribute_group": [
                7
            ],
            "code": [
                2
            ],
            "input_type": [
                7
            ],
            "is_required": [
                3
            ],
            "label": [
                7
            ],
            "sort_order": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "GiftRegistryItem": {
            "created_at": [
                7
            ],
            "note": [
                7
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "quantity_fulfilled": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "BundleItem": {
            "option_id": [
                9
            ],
            "options": [
                459
            ],
            "position": [
                9
            ],
            "required": [
                3
            ],
            "sku": [
                7
            ],
            "title": [
                7
            ],
            "type": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "BundleItemOption": {
            "can_change_quantity": [
                3
            ],
            "id": [
                9
            ],
            "is_default": [
                3
            ],
            "label": [
                7
            ],
            "position": [
                9
            ],
            "price": [
                20
            ],
            "price_type": [
                176
            ],
            "product": [
                37
            ],
            "qty": [
                20
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "BundleProduct": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "dynamic_price": [
                3
            ],
            "dynamic_sku": [
                3
            ],
            "dynamic_weight": [
                3
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "items": [
                458
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options": [
                408
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "price_view": [
                461
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "ship_bundle_items": [
                462
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "weight": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "PriceViewEnum": {},
        "ShipBundleItemsEnum": {},
        "BundleOrderItem": {
            "bundle_options": [
                464
            ],
            "discounts": [
                35
            ],
            "eligible_for_return": [
                3
            ],
            "entered_options": [
                132
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                2
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "product_type": [
                7
            ],
            "product_url_key": [
                7
            ],
            "quantity_canceled": [
                20
            ],
            "quantity_invoiced": [
                20
            ],
            "quantity_ordered": [
                20
            ],
            "quantity_refunded": [
                20
            ],
            "quantity_returned": [
                20
            ],
            "quantity_shipped": [
                20
            ],
            "selected_options": [
                132
            ],
            "status": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ItemSelectedBundleOption": {
            "id": [
                2
            ],
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "values": [
                465
            ],
            "__typename": [
                7
            ]
        },
        "ItemSelectedBundleOptionValue": {
            "id": [
                2
            ],
            "price": [
                18
            ],
            "product_name": [
                7
            ],
            "product_sku": [
                7
            ],
            "quantity": [
                20
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "BundleInvoiceItem": {
            "bundle_options": [
                464
            ],
            "discounts": [
                35
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_invoiced": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "BundleShipmentItem": {
            "bundle_options": [
                464
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_shipped": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "BundleCreditMemoItem": {
            "bundle_options": [
                464
            ],
            "discounts": [
                35
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_refunded": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "BundleWishlistItem": {
            "added_at": [
                7
            ],
            "bundle_options": [
                450
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GroupedProduct": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "items": [
                471
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "weight": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GroupedProductItem": {
            "position": [
                9
            ],
            "product": [
                37
            ],
            "qty": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GroupedProductWishlistItem": {
            "added_at": [
                7
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProduct": {
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "configurable_options": [
                474
            ],
            "configurable_product_options_selection": [
                477,
                {
                    "configurableOptionValueUids": [
                        2,
                        "[ID!]"
                    ]
                }
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_message_available": [
                7
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_returnable": [
                7
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "options": [
                408
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "variants": [
                481
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "weight": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProductOptions": {
            "attribute_code": [
                7
            ],
            "attribute_id": [
                7
            ],
            "attribute_id_v2": [
                9
            ],
            "attribute_uid": [
                2
            ],
            "id": [
                9
            ],
            "label": [
                7
            ],
            "position": [
                9
            ],
            "product_id": [
                9
            ],
            "uid": [
                2
            ],
            "use_default": [
                3
            ],
            "values": [
                475
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProductOptionsValues": {
            "default_label": [
                7
            ],
            "label": [
                7
            ],
            "store_label": [
                7
            ],
            "swatch_data": [
                476
            ],
            "uid": [
                2
            ],
            "use_default_value": [
                3
            ],
            "value_index": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "SwatchDataInterface": {
            "value": [
                7
            ],
            "on_ImageSwatchData": [
                493
            ],
            "on_TextSwatchData": [
                494
            ],
            "on_ColorSwatchData": [
                495
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProductOptionsSelection": {
            "configurable_options": [
                478
            ],
            "media_gallery": [
                51
            ],
            "options_available_for_selection": [
                480
            ],
            "variant": [
                427
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProductOption": {
            "attribute_code": [
                7
            ],
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "values": [
                479
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableProductOptionValue": {
            "is_available": [
                3
            ],
            "is_use_default": [
                3
            ],
            "label": [
                7
            ],
            "swatch": [
                476
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableOptionAvailableForSelection": {
            "attribute_code": [
                7
            ],
            "option_value_uids": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableVariant": {
            "attributes": [
                482
            ],
            "product": [
                427
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableAttributeOption": {
            "code": [
                7
            ],
            "label": [
                7
            ],
            "uid": [
                2
            ],
            "value_index": [
                9
            ],
            "__typename": [
                7
            ]
        },
        "ConfigurableWishlistItem": {
            "added_at": [
                7
            ],
            "child_sku": [
                7
            ],
            "configurable_options": [
                448
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "OrderItem": {
            "discounts": [
                35
            ],
            "eligible_for_return": [
                3
            ],
            "entered_options": [
                132
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                2
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "product_type": [
                7
            ],
            "product_url_key": [
                7
            ],
            "quantity_canceled": [
                20
            ],
            "quantity_invoiced": [
                20
            ],
            "quantity_ordered": [
                20
            ],
            "quantity_refunded": [
                20
            ],
            "quantity_returned": [
                20
            ],
            "quantity_shipped": [
                20
            ],
            "selected_options": [
                132
            ],
            "status": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "InvoiceItem": {
            "discounts": [
                35
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_invoiced": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "ShipmentItem": {
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_shipped": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "CreditMemoItem": {
            "discounts": [
                35
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_refunded": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "PaypalExpressToken": {
            "paypal_urls": [
                292
            ],
            "token": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "PayflowProToken": {
            "response_message": [
                7
            ],
            "result": [
                9
            ],
            "result_code": [
                9
            ],
            "secure_token": [
                7
            ],
            "secure_token_id": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SwatchLayerFilterItemInterface": {
            "swatch_data": [
                491
            ],
            "on_SwatchLayerFilterItem": [
                492
            ],
            "__typename": [
                7
            ]
        },
        "SwatchData": {
            "type": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "SwatchLayerFilterItem": {
            "items_count": [
                9
            ],
            "label": [
                7
            ],
            "swatch_data": [
                491
            ],
            "value_string": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ImageSwatchData": {
            "thumbnail": [
                7
            ],
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "TextSwatchData": {
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "ColorSwatchData": {
            "value": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "UiAttributeTypeFixedProductTax": {
            "is_html_allowed": [
                3
            ],
            "ui_input_type": [
                11
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardProduct": {
            "allow_message": [
                3
            ],
            "allow_open_amount": [
                3
            ],
            "attribute_set_id": [
                9
            ],
            "canonical_url": [
                7
            ],
            "categories": [
                38
            ],
            "color": [
                9
            ],
            "country_of_manufacture": [
                7
            ],
            "created_at": [
                7
            ],
            "crosssell_products": [
                37
            ],
            "custom_attributes": [
                45
            ],
            "description": [
                49
            ],
            "fashion_color": [
                9
            ],
            "fashion_material": [
                7
            ],
            "fashion_size": [
                9
            ],
            "fashion_style": [
                7
            ],
            "format": [
                9
            ],
            "gift_card_options": [
                408
            ],
            "gift_message_available": [
                7
            ],
            "giftcard_amounts": [
                498
            ],
            "giftcard_type": [
                499
            ],
            "has_video": [
                9
            ],
            "id": [
                9
            ],
            "image": [
                50
            ],
            "is_redeemable": [
                3
            ],
            "is_returnable": [
                7
            ],
            "lifetime": [
                9
            ],
            "manufacturer": [
                9
            ],
            "media_gallery": [
                51
            ],
            "media_gallery_entries": [
                52
            ],
            "message_max_length": [
                9
            ],
            "meta_description": [
                7
            ],
            "meta_keyword": [
                7
            ],
            "meta_title": [
                7
            ],
            "name": [
                7
            ],
            "new_from_date": [
                7
            ],
            "new_to_date": [
                7
            ],
            "only_x_left_in_stock": [
                20
            ],
            "open_amount_max": [
                20
            ],
            "open_amount_min": [
                20
            ],
            "options": [
                408
            ],
            "options_container": [
                7
            ],
            "price": [
                55
            ],
            "price_range": [
                60
            ],
            "price_tiers": [
                63
            ],
            "product_links": [
                64
            ],
            "rating_summary": [
                20
            ],
            "redirect_code": [
                9
            ],
            "related_products": [
                37
            ],
            "relative_url": [
                7
            ],
            "review_count": [
                9
            ],
            "reviews": [
                65,
                {
                    "pageSize": [
                        9
                    ],
                    "currentPage": [
                        9
                    ]
                }
            ],
            "short_description": [
                49
            ],
            "sku": [
                7
            ],
            "small_image": [
                50
            ],
            "special_from_date": [
                7
            ],
            "special_price": [
                20
            ],
            "special_to_date": [
                7
            ],
            "staged": [
                3
            ],
            "stock_status": [
                68
            ],
            "swatch_image": [
                7
            ],
            "thumbnail": [
                50
            ],
            "tier_price": [
                20
            ],
            "tier_prices": [
                69
            ],
            "type": [
                88
            ],
            "type_id": [
                7
            ],
            "uid": [
                2
            ],
            "updated_at": [
                7
            ],
            "upsell_products": [
                37
            ],
            "url_key": [
                7
            ],
            "url_path": [
                7
            ],
            "url_rewrites": [
                70
            ],
            "url_suffix": [
                7
            ],
            "video_file": [
                7
            ],
            "websites": [
                72
            ],
            "weight": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardAmounts": {
            "attribute_id": [
                9
            ],
            "uid": [
                2
            ],
            "value": [
                20
            ],
            "value_id": [
                9
            ],
            "website_id": [
                9
            ],
            "website_value": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardTypeEnum": {},
        "GiftCardOrderItem": {
            "discounts": [
                35
            ],
            "eligible_for_return": [
                3
            ],
            "entered_options": [
                132
            ],
            "gift_card": [
                501
            ],
            "gift_wrapping": [
                23
            ],
            "id": [
                2
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "product_type": [
                7
            ],
            "product_url_key": [
                7
            ],
            "quantity_canceled": [
                20
            ],
            "quantity_invoiced": [
                20
            ],
            "quantity_ordered": [
                20
            ],
            "quantity_refunded": [
                20
            ],
            "quantity_returned": [
                20
            ],
            "quantity_shipped": [
                20
            ],
            "selected_options": [
                132
            ],
            "status": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardItem": {
            "message": [
                7
            ],
            "recipient_email": [
                7
            ],
            "recipient_name": [
                7
            ],
            "sender_email": [
                7
            ],
            "sender_name": [
                7
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardInvoiceItem": {
            "discounts": [
                35
            ],
            "gift_card": [
                501
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_invoiced": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardCreditMemoItem": {
            "discounts": [
                35
            ],
            "gift_card": [
                501
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_refunded": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardShipmentItem": {
            "gift_card": [
                501
            ],
            "id": [
                2
            ],
            "order_item": [
                131
            ],
            "product_name": [
                7
            ],
            "product_sale_price": [
                18
            ],
            "product_sku": [
                7
            ],
            "quantity_shipped": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardCartItem": {
            "amount": [
                18
            ],
            "customizable_options": [
                173
            ],
            "errors": [
                32
            ],
            "id": [
                7
            ],
            "message": [
                7
            ],
            "prices": [
                34
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "recipient_email": [
                7
            ],
            "recipient_name": [
                7
            ],
            "sender_email": [
                7
            ],
            "sender_name": [
                7
            ],
            "uid": [
                2
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardWishlistItem": {
            "added_at": [
                7
            ],
            "customizable_options": [
                173
            ],
            "description": [
                7
            ],
            "gift_card_options": [
                507
            ],
            "id": [
                2
            ],
            "product": [
                37
            ],
            "quantity": [
                20
            ],
            "__typename": [
                7
            ]
        },
        "GiftCardOptions": {
            "amount": [
                18
            ],
            "custom_giftcard_amount": [
                18
            ],
            "message": [
                7
            ],
            "recipient_email": [
                7
            ],
            "recipient_name": [
                7
            ],
            "sender_email": [
                7
            ],
            "sender_name": [
                7
            ],
            "__typename": [
                7
            ]
        }
    }
}