
var Query_possibleTypes = ['Query']
module.exports.isQuery = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isQuery"')
  return Query_possibleTypes.includes(obj.__typename)
}



var AttributesMetadata_possibleTypes = ['AttributesMetadata']
module.exports.isAttributesMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttributesMetadata"')
  return AttributesMetadata_possibleTypes.includes(obj.__typename)
}



var AttributeMetadataInterface_possibleTypes = ['ProductAttributeMetadata']
module.exports.isAttributeMetadataInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttributeMetadataInterface"')
  return AttributeMetadataInterface_possibleTypes.includes(obj.__typename)
}



var StoreLabels_possibleTypes = ['StoreLabels']
module.exports.isStoreLabels = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isStoreLabels"')
  return StoreLabels_possibleTypes.includes(obj.__typename)
}



var UiInputTypeInterface_possibleTypes = ['UiAttributeTypeSelect','UiAttributeTypeMultiSelect','UiAttributeTypeBoolean','UiAttributeTypeAny','UiAttributeTypeFixedProductTax']
module.exports.isUiInputTypeInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUiInputTypeInterface"')
  return UiInputTypeInterface_possibleTypes.includes(obj.__typename)
}



var StoreConfig_possibleTypes = ['StoreConfig']
module.exports.isStoreConfig = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isStoreConfig"')
  return StoreConfig_possibleTypes.includes(obj.__typename)
}



var SendFriendConfiguration_possibleTypes = ['SendFriendConfiguration']
module.exports.isSendFriendConfiguration = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSendFriendConfiguration"')
  return SendFriendConfiguration_possibleTypes.includes(obj.__typename)
}



var Cart_possibleTypes = ['Cart']
module.exports.isCart = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCart"')
  return Cart_possibleTypes.includes(obj.__typename)
}



var AppliedCoupon_possibleTypes = ['AppliedCoupon']
module.exports.isAppliedCoupon = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAppliedCoupon"')
  return AppliedCoupon_possibleTypes.includes(obj.__typename)
}



var AppliedGiftCard_possibleTypes = ['AppliedGiftCard']
module.exports.isAppliedGiftCard = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAppliedGiftCard"')
  return AppliedGiftCard_possibleTypes.includes(obj.__typename)
}



var Money_possibleTypes = ['Money']
module.exports.isMoney = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMoney"')
  return Money_possibleTypes.includes(obj.__typename)
}



var RewardPointsAmount_possibleTypes = ['RewardPointsAmount']
module.exports.isRewardPointsAmount = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRewardPointsAmount"')
  return RewardPointsAmount_possibleTypes.includes(obj.__typename)
}



var AppliedStoreCredit_possibleTypes = ['AppliedStoreCredit']
module.exports.isAppliedStoreCredit = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAppliedStoreCredit"')
  return AppliedStoreCredit_possibleTypes.includes(obj.__typename)
}



var GiftWrapping_possibleTypes = ['GiftWrapping']
module.exports.isGiftWrapping = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftWrapping"')
  return GiftWrapping_possibleTypes.includes(obj.__typename)
}



var GiftWrappingImage_possibleTypes = ['GiftWrappingImage']
module.exports.isGiftWrappingImage = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftWrappingImage"')
  return GiftWrappingImage_possibleTypes.includes(obj.__typename)
}



var AvailablePaymentMethod_possibleTypes = ['AvailablePaymentMethod']
module.exports.isAvailablePaymentMethod = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAvailablePaymentMethod"')
  return AvailablePaymentMethod_possibleTypes.includes(obj.__typename)
}



var BillingCartAddress_possibleTypes = ['BillingCartAddress']
module.exports.isBillingCartAddress = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBillingCartAddress"')
  return BillingCartAddress_possibleTypes.includes(obj.__typename)
}



var CartAddressInterface_possibleTypes = ['BillingCartAddress','ShippingCartAddress']
module.exports.isCartAddressInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartAddressInterface"')
  return CartAddressInterface_possibleTypes.includes(obj.__typename)
}



var CartAddressCountry_possibleTypes = ['CartAddressCountry']
module.exports.isCartAddressCountry = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartAddressCountry"')
  return CartAddressCountry_possibleTypes.includes(obj.__typename)
}



var CartAddressRegion_possibleTypes = ['CartAddressRegion']
module.exports.isCartAddressRegion = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartAddressRegion"')
  return CartAddressRegion_possibleTypes.includes(obj.__typename)
}



var GiftMessage_possibleTypes = ['GiftMessage']
module.exports.isGiftMessage = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftMessage"')
  return GiftMessage_possibleTypes.includes(obj.__typename)
}



var CartItemInterface_possibleTypes = ['SimpleCartItem','VirtualCartItem','DownloadableCartItem','ConfigurableCartItem','BundleCartItem','GiftCardCartItem']
module.exports.isCartItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartItemInterface"')
  return CartItemInterface_possibleTypes.includes(obj.__typename)
}



var CartItemError_possibleTypes = ['CartItemError']
module.exports.isCartItemError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartItemError"')
  return CartItemError_possibleTypes.includes(obj.__typename)
}



var CartItemPrices_possibleTypes = ['CartItemPrices']
module.exports.isCartItemPrices = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartItemPrices"')
  return CartItemPrices_possibleTypes.includes(obj.__typename)
}



var Discount_possibleTypes = ['Discount']
module.exports.isDiscount = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDiscount"')
  return Discount_possibleTypes.includes(obj.__typename)
}



var FixedProductTax_possibleTypes = ['FixedProductTax']
module.exports.isFixedProductTax = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isFixedProductTax"')
  return FixedProductTax_possibleTypes.includes(obj.__typename)
}



var ProductInterface_possibleTypes = ['VirtualProduct','SimpleProduct','DownloadableProduct','BundleProduct','GroupedProduct','ConfigurableProduct','GiftCardProduct']
module.exports.isProductInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductInterface"')
  return ProductInterface_possibleTypes.includes(obj.__typename)
}



var CategoryInterface_possibleTypes = ['CategoryTree']
module.exports.isCategoryInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCategoryInterface"')
  return CategoryInterface_possibleTypes.includes(obj.__typename)
}



var Breadcrumb_possibleTypes = ['Breadcrumb']
module.exports.isBreadcrumb = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBreadcrumb"')
  return Breadcrumb_possibleTypes.includes(obj.__typename)
}



var CmsBlock_possibleTypes = ['CmsBlock']
module.exports.isCmsBlock = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCmsBlock"')
  return CmsBlock_possibleTypes.includes(obj.__typename)
}



var CategoryProducts_possibleTypes = ['CategoryProducts']
module.exports.isCategoryProducts = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCategoryProducts"')
  return CategoryProducts_possibleTypes.includes(obj.__typename)
}



var SearchResultPageInfo_possibleTypes = ['SearchResultPageInfo']
module.exports.isSearchResultPageInfo = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSearchResultPageInfo"')
  return SearchResultPageInfo_possibleTypes.includes(obj.__typename)
}



var CustomAttribute_possibleTypes = ['CustomAttribute']
module.exports.isCustomAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomAttribute"')
  return CustomAttribute_possibleTypes.includes(obj.__typename)
}



var EnteredAttributeValue_possibleTypes = ['EnteredAttributeValue']
module.exports.isEnteredAttributeValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isEnteredAttributeValue"')
  return EnteredAttributeValue_possibleTypes.includes(obj.__typename)
}



var SelectedAttributeOption_possibleTypes = ['SelectedAttributeOption']
module.exports.isSelectedAttributeOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedAttributeOption"')
  return SelectedAttributeOption_possibleTypes.includes(obj.__typename)
}



var AttributeOptionInterface_possibleTypes = ['AttributeOption']
module.exports.isAttributeOptionInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttributeOptionInterface"')
  return AttributeOptionInterface_possibleTypes.includes(obj.__typename)
}



var ComplexTextValue_possibleTypes = ['ComplexTextValue']
module.exports.isComplexTextValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isComplexTextValue"')
  return ComplexTextValue_possibleTypes.includes(obj.__typename)
}



var ProductImage_possibleTypes = ['ProductImage']
module.exports.isProductImage = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductImage"')
  return ProductImage_possibleTypes.includes(obj.__typename)
}



var MediaGalleryInterface_possibleTypes = ['ProductImage','ProductVideo']
module.exports.isMediaGalleryInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMediaGalleryInterface"')
  return MediaGalleryInterface_possibleTypes.includes(obj.__typename)
}



var MediaGalleryEntry_possibleTypes = ['MediaGalleryEntry']
module.exports.isMediaGalleryEntry = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMediaGalleryEntry"')
  return MediaGalleryEntry_possibleTypes.includes(obj.__typename)
}



var ProductMediaGalleryEntriesContent_possibleTypes = ['ProductMediaGalleryEntriesContent']
module.exports.isProductMediaGalleryEntriesContent = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductMediaGalleryEntriesContent"')
  return ProductMediaGalleryEntriesContent_possibleTypes.includes(obj.__typename)
}



var ProductMediaGalleryEntriesVideoContent_possibleTypes = ['ProductMediaGalleryEntriesVideoContent']
module.exports.isProductMediaGalleryEntriesVideoContent = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductMediaGalleryEntriesVideoContent"')
  return ProductMediaGalleryEntriesVideoContent_possibleTypes.includes(obj.__typename)
}



var ProductPrices_possibleTypes = ['ProductPrices']
module.exports.isProductPrices = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductPrices"')
  return ProductPrices_possibleTypes.includes(obj.__typename)
}



var Price_possibleTypes = ['Price']
module.exports.isPrice = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPrice"')
  return Price_possibleTypes.includes(obj.__typename)
}



var PriceAdjustment_possibleTypes = ['PriceAdjustment']
module.exports.isPriceAdjustment = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPriceAdjustment"')
  return PriceAdjustment_possibleTypes.includes(obj.__typename)
}



var PriceRange_possibleTypes = ['PriceRange']
module.exports.isPriceRange = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPriceRange"')
  return PriceRange_possibleTypes.includes(obj.__typename)
}



var ProductPrice_possibleTypes = ['ProductPrice']
module.exports.isProductPrice = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductPrice"')
  return ProductPrice_possibleTypes.includes(obj.__typename)
}



var ProductDiscount_possibleTypes = ['ProductDiscount']
module.exports.isProductDiscount = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductDiscount"')
  return ProductDiscount_possibleTypes.includes(obj.__typename)
}



var TierPrice_possibleTypes = ['TierPrice']
module.exports.isTierPrice = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isTierPrice"')
  return TierPrice_possibleTypes.includes(obj.__typename)
}



var ProductLinksInterface_possibleTypes = ['ProductLinks']
module.exports.isProductLinksInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductLinksInterface"')
  return ProductLinksInterface_possibleTypes.includes(obj.__typename)
}



var ProductReviews_possibleTypes = ['ProductReviews']
module.exports.isProductReviews = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductReviews"')
  return ProductReviews_possibleTypes.includes(obj.__typename)
}



var ProductReview_possibleTypes = ['ProductReview']
module.exports.isProductReview = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductReview"')
  return ProductReview_possibleTypes.includes(obj.__typename)
}



var ProductReviewRating_possibleTypes = ['ProductReviewRating']
module.exports.isProductReviewRating = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductReviewRating"')
  return ProductReviewRating_possibleTypes.includes(obj.__typename)
}



var ProductTierPrices_possibleTypes = ['ProductTierPrices']
module.exports.isProductTierPrices = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductTierPrices"')
  return ProductTierPrices_possibleTypes.includes(obj.__typename)
}



var UrlRewrite_possibleTypes = ['UrlRewrite']
module.exports.isUrlRewrite = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUrlRewrite"')
  return UrlRewrite_possibleTypes.includes(obj.__typename)
}



var HttpQueryParameter_possibleTypes = ['HttpQueryParameter']
module.exports.isHttpQueryParameter = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isHttpQueryParameter"')
  return HttpQueryParameter_possibleTypes.includes(obj.__typename)
}



var Website_possibleTypes = ['Website']
module.exports.isWebsite = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWebsite"')
  return Website_possibleTypes.includes(obj.__typename)
}



var CartPrices_possibleTypes = ['CartPrices']
module.exports.isCartPrices = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartPrices"')
  return CartPrices_possibleTypes.includes(obj.__typename)
}



var CartTaxItem_possibleTypes = ['CartTaxItem']
module.exports.isCartTaxItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartTaxItem"')
  return CartTaxItem_possibleTypes.includes(obj.__typename)
}



var CartDiscount_possibleTypes = ['CartDiscount']
module.exports.isCartDiscount = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartDiscount"')
  return CartDiscount_possibleTypes.includes(obj.__typename)
}



var GiftOptionsPrices_possibleTypes = ['GiftOptionsPrices']
module.exports.isGiftOptionsPrices = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftOptionsPrices"')
  return GiftOptionsPrices_possibleTypes.includes(obj.__typename)
}



var SelectedPaymentMethod_possibleTypes = ['SelectedPaymentMethod']
module.exports.isSelectedPaymentMethod = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedPaymentMethod"')
  return SelectedPaymentMethod_possibleTypes.includes(obj.__typename)
}



var ShippingCartAddress_possibleTypes = ['ShippingCartAddress']
module.exports.isShippingCartAddress = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShippingCartAddress"')
  return ShippingCartAddress_possibleTypes.includes(obj.__typename)
}



var AvailableShippingMethod_possibleTypes = ['AvailableShippingMethod']
module.exports.isAvailableShippingMethod = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAvailableShippingMethod"')
  return AvailableShippingMethod_possibleTypes.includes(obj.__typename)
}



var CartItemQuantity_possibleTypes = ['CartItemQuantity']
module.exports.isCartItemQuantity = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartItemQuantity"')
  return CartItemQuantity_possibleTypes.includes(obj.__typename)
}



var SelectedShippingMethod_possibleTypes = ['SelectedShippingMethod']
module.exports.isSelectedShippingMethod = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedShippingMethod"')
  return SelectedShippingMethod_possibleTypes.includes(obj.__typename)
}



var CategoryResult_possibleTypes = ['CategoryResult']
module.exports.isCategoryResult = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCategoryResult"')
  return CategoryResult_possibleTypes.includes(obj.__typename)
}



var CategoryTree_possibleTypes = ['CategoryTree']
module.exports.isCategoryTree = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCategoryTree"')
  return CategoryTree_possibleTypes.includes(obj.__typename)
}



var RoutableInterface_possibleTypes = ['CategoryTree','CmsPage','VirtualProduct','SimpleProduct','DownloadableProduct','BundleProduct','GroupedProduct','ConfigurableProduct','GiftCardProduct']
module.exports.isRoutableInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRoutableInterface"')
  return RoutableInterface_possibleTypes.includes(obj.__typename)
}



var CheckoutAgreement_possibleTypes = ['CheckoutAgreement']
module.exports.isCheckoutAgreement = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCheckoutAgreement"')
  return CheckoutAgreement_possibleTypes.includes(obj.__typename)
}



var CmsBlocks_possibleTypes = ['CmsBlocks']
module.exports.isCmsBlocks = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCmsBlocks"')
  return CmsBlocks_possibleTypes.includes(obj.__typename)
}



var CmsPage_possibleTypes = ['CmsPage']
module.exports.isCmsPage = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCmsPage"')
  return CmsPage_possibleTypes.includes(obj.__typename)
}



var CompareList_possibleTypes = ['CompareList']
module.exports.isCompareList = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCompareList"')
  return CompareList_possibleTypes.includes(obj.__typename)
}



var ComparableAttribute_possibleTypes = ['ComparableAttribute']
module.exports.isComparableAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isComparableAttribute"')
  return ComparableAttribute_possibleTypes.includes(obj.__typename)
}



var ComparableItem_possibleTypes = ['ComparableItem']
module.exports.isComparableItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isComparableItem"')
  return ComparableItem_possibleTypes.includes(obj.__typename)
}



var ProductAttribute_possibleTypes = ['ProductAttribute']
module.exports.isProductAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductAttribute"')
  return ProductAttribute_possibleTypes.includes(obj.__typename)
}



var Country_possibleTypes = ['Country']
module.exports.isCountry = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCountry"')
  return Country_possibleTypes.includes(obj.__typename)
}



var Region_possibleTypes = ['Region']
module.exports.isRegion = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRegion"')
  return Region_possibleTypes.includes(obj.__typename)
}



var Currency_possibleTypes = ['Currency']
module.exports.isCurrency = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCurrency"')
  return Currency_possibleTypes.includes(obj.__typename)
}



var ExchangeRate_possibleTypes = ['ExchangeRate']
module.exports.isExchangeRate = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isExchangeRate"')
  return ExchangeRate_possibleTypes.includes(obj.__typename)
}



var CustomAttributeMetadata_possibleTypes = ['CustomAttributeMetadata']
module.exports.isCustomAttributeMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomAttributeMetadata"')
  return CustomAttributeMetadata_possibleTypes.includes(obj.__typename)
}



var Attribute_possibleTypes = ['Attribute']
module.exports.isAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttribute"')
  return Attribute_possibleTypes.includes(obj.__typename)
}



var AttributeOption_possibleTypes = ['AttributeOption']
module.exports.isAttributeOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttributeOption"')
  return AttributeOption_possibleTypes.includes(obj.__typename)
}



var StorefrontProperties_possibleTypes = ['StorefrontProperties']
module.exports.isStorefrontProperties = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isStorefrontProperties"')
  return StorefrontProperties_possibleTypes.includes(obj.__typename)
}



var Customer_possibleTypes = ['Customer']
module.exports.isCustomer = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomer"')
  return Customer_possibleTypes.includes(obj.__typename)
}



var CustomerAddress_possibleTypes = ['CustomerAddress']
module.exports.isCustomerAddress = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerAddress"')
  return CustomerAddress_possibleTypes.includes(obj.__typename)
}



var CustomerAddressAttribute_possibleTypes = ['CustomerAddressAttribute']
module.exports.isCustomerAddressAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerAddressAttribute"')
  return CustomerAddressAttribute_possibleTypes.includes(obj.__typename)
}



var CustomerAddressRegion_possibleTypes = ['CustomerAddressRegion']
module.exports.isCustomerAddressRegion = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerAddressRegion"')
  return CustomerAddressRegion_possibleTypes.includes(obj.__typename)
}



var GiftRegistry_possibleTypes = ['GiftRegistry']
module.exports.isGiftRegistry = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistry"')
  return GiftRegistry_possibleTypes.includes(obj.__typename)
}



var GiftRegistryDynamicAttribute_possibleTypes = ['GiftRegistryDynamicAttribute']
module.exports.isGiftRegistryDynamicAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryDynamicAttribute"')
  return GiftRegistryDynamicAttribute_possibleTypes.includes(obj.__typename)
}



var GiftRegistryDynamicAttributeInterface_possibleTypes = ['GiftRegistryDynamicAttribute','GiftRegistryRegistrantDynamicAttribute']
module.exports.isGiftRegistryDynamicAttributeInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryDynamicAttributeInterface"')
  return GiftRegistryDynamicAttributeInterface_possibleTypes.includes(obj.__typename)
}



var GiftRegistryItemInterface_possibleTypes = ['GiftRegistryItem']
module.exports.isGiftRegistryItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryItemInterface"')
  return GiftRegistryItemInterface_possibleTypes.includes(obj.__typename)
}



var GiftRegistryRegistrant_possibleTypes = ['GiftRegistryRegistrant']
module.exports.isGiftRegistryRegistrant = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryRegistrant"')
  return GiftRegistryRegistrant_possibleTypes.includes(obj.__typename)
}



var GiftRegistryRegistrantDynamicAttribute_possibleTypes = ['GiftRegistryRegistrantDynamicAttribute']
module.exports.isGiftRegistryRegistrantDynamicAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryRegistrantDynamicAttribute"')
  return GiftRegistryRegistrantDynamicAttribute_possibleTypes.includes(obj.__typename)
}



var GiftRegistryType_possibleTypes = ['GiftRegistryType']
module.exports.isGiftRegistryType = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryType"')
  return GiftRegistryType_possibleTypes.includes(obj.__typename)
}



var GiftRegistryDynamicAttributeMetadataInterface_possibleTypes = ['GiftRegistryDynamicAttributeMetadata']
module.exports.isGiftRegistryDynamicAttributeMetadataInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryDynamicAttributeMetadataInterface"')
  return GiftRegistryDynamicAttributeMetadataInterface_possibleTypes.includes(obj.__typename)
}



var CustomerOrders_possibleTypes = ['CustomerOrders']
module.exports.isCustomerOrders = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerOrders"')
  return CustomerOrders_possibleTypes.includes(obj.__typename)
}



var CustomerOrder_possibleTypes = ['CustomerOrder']
module.exports.isCustomerOrder = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerOrder"')
  return CustomerOrder_possibleTypes.includes(obj.__typename)
}



var OrderAddress_possibleTypes = ['OrderAddress']
module.exports.isOrderAddress = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderAddress"')
  return OrderAddress_possibleTypes.includes(obj.__typename)
}



var SalesCommentItem_possibleTypes = ['SalesCommentItem']
module.exports.isSalesCommentItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSalesCommentItem"')
  return SalesCommentItem_possibleTypes.includes(obj.__typename)
}



var CreditMemo_possibleTypes = ['CreditMemo']
module.exports.isCreditMemo = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreditMemo"')
  return CreditMemo_possibleTypes.includes(obj.__typename)
}



var CreditMemoItemInterface_possibleTypes = ['DownloadableCreditMemoItem','BundleCreditMemoItem','CreditMemoItem','GiftCardCreditMemoItem']
module.exports.isCreditMemoItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreditMemoItemInterface"')
  return CreditMemoItemInterface_possibleTypes.includes(obj.__typename)
}



var OrderItemInterface_possibleTypes = ['DownloadableOrderItem','BundleOrderItem','OrderItem','GiftCardOrderItem']
module.exports.isOrderItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderItemInterface"')
  return OrderItemInterface_possibleTypes.includes(obj.__typename)
}



var OrderItemOption_possibleTypes = ['OrderItemOption']
module.exports.isOrderItemOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderItemOption"')
  return OrderItemOption_possibleTypes.includes(obj.__typename)
}



var CreditMemoTotal_possibleTypes = ['CreditMemoTotal']
module.exports.isCreditMemoTotal = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreditMemoTotal"')
  return CreditMemoTotal_possibleTypes.includes(obj.__typename)
}



var ShippingHandling_possibleTypes = ['ShippingHandling']
module.exports.isShippingHandling = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShippingHandling"')
  return ShippingHandling_possibleTypes.includes(obj.__typename)
}



var ShippingDiscount_possibleTypes = ['ShippingDiscount']
module.exports.isShippingDiscount = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShippingDiscount"')
  return ShippingDiscount_possibleTypes.includes(obj.__typename)
}



var TaxItem_possibleTypes = ['TaxItem']
module.exports.isTaxItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isTaxItem"')
  return TaxItem_possibleTypes.includes(obj.__typename)
}



var Invoice_possibleTypes = ['Invoice']
module.exports.isInvoice = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isInvoice"')
  return Invoice_possibleTypes.includes(obj.__typename)
}



var InvoiceItemInterface_possibleTypes = ['DownloadableInvoiceItem','BundleInvoiceItem','InvoiceItem','GiftCardInvoiceItem']
module.exports.isInvoiceItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isInvoiceItemInterface"')
  return InvoiceItemInterface_possibleTypes.includes(obj.__typename)
}



var InvoiceTotal_possibleTypes = ['InvoiceTotal']
module.exports.isInvoiceTotal = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isInvoiceTotal"')
  return InvoiceTotal_possibleTypes.includes(obj.__typename)
}



var OrderPaymentMethod_possibleTypes = ['OrderPaymentMethod']
module.exports.isOrderPaymentMethod = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderPaymentMethod"')
  return OrderPaymentMethod_possibleTypes.includes(obj.__typename)
}



var KeyValue_possibleTypes = ['KeyValue']
module.exports.isKeyValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isKeyValue"')
  return KeyValue_possibleTypes.includes(obj.__typename)
}



var Returns_possibleTypes = ['Returns']
module.exports.isReturns = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturns"')
  return Returns_possibleTypes.includes(obj.__typename)
}



var Return_possibleTypes = ['Return']
module.exports.isReturn = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturn"')
  return Return_possibleTypes.includes(obj.__typename)
}



var ReturnShippingCarrier_possibleTypes = ['ReturnShippingCarrier']
module.exports.isReturnShippingCarrier = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnShippingCarrier"')
  return ReturnShippingCarrier_possibleTypes.includes(obj.__typename)
}



var ReturnComment_possibleTypes = ['ReturnComment']
module.exports.isReturnComment = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnComment"')
  return ReturnComment_possibleTypes.includes(obj.__typename)
}



var ReturnCustomer_possibleTypes = ['ReturnCustomer']
module.exports.isReturnCustomer = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnCustomer"')
  return ReturnCustomer_possibleTypes.includes(obj.__typename)
}



var ReturnItem_possibleTypes = ['ReturnItem']
module.exports.isReturnItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnItem"')
  return ReturnItem_possibleTypes.includes(obj.__typename)
}



var ReturnCustomAttribute_possibleTypes = ['ReturnCustomAttribute']
module.exports.isReturnCustomAttribute = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnCustomAttribute"')
  return ReturnCustomAttribute_possibleTypes.includes(obj.__typename)
}



var ReturnShipping_possibleTypes = ['ReturnShipping']
module.exports.isReturnShipping = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnShipping"')
  return ReturnShipping_possibleTypes.includes(obj.__typename)
}



var ReturnShippingAddress_possibleTypes = ['ReturnShippingAddress']
module.exports.isReturnShippingAddress = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnShippingAddress"')
  return ReturnShippingAddress_possibleTypes.includes(obj.__typename)
}



var ReturnShippingTracking_possibleTypes = ['ReturnShippingTracking']
module.exports.isReturnShippingTracking = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnShippingTracking"')
  return ReturnShippingTracking_possibleTypes.includes(obj.__typename)
}



var ReturnShippingTrackingStatus_possibleTypes = ['ReturnShippingTrackingStatus']
module.exports.isReturnShippingTrackingStatus = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReturnShippingTrackingStatus"')
  return ReturnShippingTrackingStatus_possibleTypes.includes(obj.__typename)
}



var OrderShipment_possibleTypes = ['OrderShipment']
module.exports.isOrderShipment = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderShipment"')
  return OrderShipment_possibleTypes.includes(obj.__typename)
}



var ShipmentItemInterface_possibleTypes = ['BundleShipmentItem','ShipmentItem','GiftCardShipmentItem']
module.exports.isShipmentItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShipmentItemInterface"')
  return ShipmentItemInterface_possibleTypes.includes(obj.__typename)
}



var ShipmentTracking_possibleTypes = ['ShipmentTracking']
module.exports.isShipmentTracking = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShipmentTracking"')
  return ShipmentTracking_possibleTypes.includes(obj.__typename)
}



var OrderTotal_possibleTypes = ['OrderTotal']
module.exports.isOrderTotal = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderTotal"')
  return OrderTotal_possibleTypes.includes(obj.__typename)
}



var RewardPoints_possibleTypes = ['RewardPoints']
module.exports.isRewardPoints = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRewardPoints"')
  return RewardPoints_possibleTypes.includes(obj.__typename)
}



var RewardPointsBalanceHistoryItem_possibleTypes = ['RewardPointsBalanceHistoryItem']
module.exports.isRewardPointsBalanceHistoryItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRewardPointsBalanceHistoryItem"')
  return RewardPointsBalanceHistoryItem_possibleTypes.includes(obj.__typename)
}



var RewardPointsExchangeRates_possibleTypes = ['RewardPointsExchangeRates']
module.exports.isRewardPointsExchangeRates = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRewardPointsExchangeRates"')
  return RewardPointsExchangeRates_possibleTypes.includes(obj.__typename)
}



var RewardPointsRate_possibleTypes = ['RewardPointsRate']
module.exports.isRewardPointsRate = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRewardPointsRate"')
  return RewardPointsRate_possibleTypes.includes(obj.__typename)
}



var RewardPointsSubscriptionStatus_possibleTypes = ['RewardPointsSubscriptionStatus']
module.exports.isRewardPointsSubscriptionStatus = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRewardPointsSubscriptionStatus"')
  return RewardPointsSubscriptionStatus_possibleTypes.includes(obj.__typename)
}



var CustomerStoreCredit_possibleTypes = ['CustomerStoreCredit']
module.exports.isCustomerStoreCredit = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerStoreCredit"')
  return CustomerStoreCredit_possibleTypes.includes(obj.__typename)
}



var CustomerStoreCreditHistory_possibleTypes = ['CustomerStoreCreditHistory']
module.exports.isCustomerStoreCreditHistory = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerStoreCreditHistory"')
  return CustomerStoreCreditHistory_possibleTypes.includes(obj.__typename)
}



var CustomerStoreCreditHistoryItem_possibleTypes = ['CustomerStoreCreditHistoryItem']
module.exports.isCustomerStoreCreditHistoryItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerStoreCreditHistoryItem"')
  return CustomerStoreCreditHistoryItem_possibleTypes.includes(obj.__typename)
}



var Wishlist_possibleTypes = ['Wishlist']
module.exports.isWishlist = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishlist"')
  return Wishlist_possibleTypes.includes(obj.__typename)
}



var WishlistItem_possibleTypes = ['WishlistItem']
module.exports.isWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishlistItem"')
  return WishlistItem_possibleTypes.includes(obj.__typename)
}



var WishlistItems_possibleTypes = ['WishlistItems']
module.exports.isWishlistItems = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishlistItems"')
  return WishlistItems_possibleTypes.includes(obj.__typename)
}



var WishlistItemInterface_possibleTypes = ['SimpleWishlistItem','VirtualWishlistItem','DownloadableWishlistItem','BundleWishlistItem','GroupedProductWishlistItem','ConfigurableWishlistItem','GiftCardWishlistItem']
module.exports.isWishlistItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishlistItemInterface"')
  return WishlistItemInterface_possibleTypes.includes(obj.__typename)
}



var SelectedCustomizableOption_possibleTypes = ['SelectedCustomizableOption']
module.exports.isSelectedCustomizableOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedCustomizableOption"')
  return SelectedCustomizableOption_possibleTypes.includes(obj.__typename)
}



var SelectedCustomizableOptionValue_possibleTypes = ['SelectedCustomizableOptionValue']
module.exports.isSelectedCustomizableOptionValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedCustomizableOptionValue"')
  return SelectedCustomizableOptionValue_possibleTypes.includes(obj.__typename)
}



var CartItemSelectedOptionValuePrice_possibleTypes = ['CartItemSelectedOptionValuePrice']
module.exports.isCartItemSelectedOptionValuePrice = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartItemSelectedOptionValuePrice"')
  return CartItemSelectedOptionValuePrice_possibleTypes.includes(obj.__typename)
}



var CustomerDownloadableProducts_possibleTypes = ['CustomerDownloadableProducts']
module.exports.isCustomerDownloadableProducts = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerDownloadableProducts"')
  return CustomerDownloadableProducts_possibleTypes.includes(obj.__typename)
}



var CustomerDownloadableProduct_possibleTypes = ['CustomerDownloadableProduct']
module.exports.isCustomerDownloadableProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerDownloadableProduct"')
  return CustomerDownloadableProduct_possibleTypes.includes(obj.__typename)
}



var CustomerPaymentTokens_possibleTypes = ['CustomerPaymentTokens']
module.exports.isCustomerPaymentTokens = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerPaymentTokens"')
  return CustomerPaymentTokens_possibleTypes.includes(obj.__typename)
}



var PaymentToken_possibleTypes = ['PaymentToken']
module.exports.isPaymentToken = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPaymentToken"')
  return PaymentToken_possibleTypes.includes(obj.__typename)
}



var DynamicBlocks_possibleTypes = ['DynamicBlocks']
module.exports.isDynamicBlocks = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDynamicBlocks"')
  return DynamicBlocks_possibleTypes.includes(obj.__typename)
}



var DynamicBlock_possibleTypes = ['DynamicBlock']
module.exports.isDynamicBlock = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDynamicBlock"')
  return DynamicBlock_possibleTypes.includes(obj.__typename)
}



var HostedProUrl_possibleTypes = ['HostedProUrl']
module.exports.isHostedProUrl = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isHostedProUrl"')
  return HostedProUrl_possibleTypes.includes(obj.__typename)
}



var PayflowLinkToken_possibleTypes = ['PayflowLinkToken']
module.exports.isPayflowLinkToken = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPayflowLinkToken"')
  return PayflowLinkToken_possibleTypes.includes(obj.__typename)
}



var GiftCardAccount_possibleTypes = ['GiftCardAccount']
module.exports.isGiftCardAccount = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardAccount"')
  return GiftCardAccount_possibleTypes.includes(obj.__typename)
}



var GiftRegistrySearchResult_possibleTypes = ['GiftRegistrySearchResult']
module.exports.isGiftRegistrySearchResult = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistrySearchResult"')
  return GiftRegistrySearchResult_possibleTypes.includes(obj.__typename)
}



var IsEmailAvailableOutput_possibleTypes = ['IsEmailAvailableOutput']
module.exports.isIsEmailAvailableOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isIsEmailAvailableOutput"')
  return IsEmailAvailableOutput_possibleTypes.includes(obj.__typename)
}



var PickupLocations_possibleTypes = ['PickupLocations']
module.exports.isPickupLocations = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPickupLocations"')
  return PickupLocations_possibleTypes.includes(obj.__typename)
}



var PickupLocation_possibleTypes = ['PickupLocation']
module.exports.isPickupLocation = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPickupLocation"')
  return PickupLocation_possibleTypes.includes(obj.__typename)
}



var ProductReviewRatingsMetadata_possibleTypes = ['ProductReviewRatingsMetadata']
module.exports.isProductReviewRatingsMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductReviewRatingsMetadata"')
  return ProductReviewRatingsMetadata_possibleTypes.includes(obj.__typename)
}



var ProductReviewRatingMetadata_possibleTypes = ['ProductReviewRatingMetadata']
module.exports.isProductReviewRatingMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductReviewRatingMetadata"')
  return ProductReviewRatingMetadata_possibleTypes.includes(obj.__typename)
}



var ProductReviewRatingValueMetadata_possibleTypes = ['ProductReviewRatingValueMetadata']
module.exports.isProductReviewRatingValueMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductReviewRatingValueMetadata"')
  return ProductReviewRatingValueMetadata_possibleTypes.includes(obj.__typename)
}



var Products_possibleTypes = ['Products']
module.exports.isProducts = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProducts"')
  return Products_possibleTypes.includes(obj.__typename)
}



var Aggregation_possibleTypes = ['Aggregation']
module.exports.isAggregation = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAggregation"')
  return Aggregation_possibleTypes.includes(obj.__typename)
}



var AggregationOption_possibleTypes = ['AggregationOption']
module.exports.isAggregationOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAggregationOption"')
  return AggregationOption_possibleTypes.includes(obj.__typename)
}



var AggregationOptionInterface_possibleTypes = ['AggregationOption']
module.exports.isAggregationOptionInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAggregationOptionInterface"')
  return AggregationOptionInterface_possibleTypes.includes(obj.__typename)
}



var LayerFilter_possibleTypes = ['LayerFilter']
module.exports.isLayerFilter = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isLayerFilter"')
  return LayerFilter_possibleTypes.includes(obj.__typename)
}



var LayerFilterItemInterface_possibleTypes = ['LayerFilterItem','SwatchLayerFilterItem']
module.exports.isLayerFilterItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isLayerFilterItemInterface"')
  return LayerFilterItemInterface_possibleTypes.includes(obj.__typename)
}



var SortFields_possibleTypes = ['SortFields']
module.exports.isSortFields = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSortFields"')
  return SortFields_possibleTypes.includes(obj.__typename)
}



var SortField_possibleTypes = ['SortField']
module.exports.isSortField = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSortField"')
  return SortField_possibleTypes.includes(obj.__typename)
}



var SearchSuggestion_possibleTypes = ['SearchSuggestion']
module.exports.isSearchSuggestion = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSearchSuggestion"')
  return SearchSuggestion_possibleTypes.includes(obj.__typename)
}



var ReCaptchaConfigurationV3_possibleTypes = ['ReCaptchaConfigurationV3']
module.exports.isReCaptchaConfigurationV3 = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReCaptchaConfigurationV3"')
  return ReCaptchaConfigurationV3_possibleTypes.includes(obj.__typename)
}



var EntityUrl_possibleTypes = ['EntityUrl']
module.exports.isEntityUrl = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isEntityUrl"')
  return EntityUrl_possibleTypes.includes(obj.__typename)
}



var WishlistOutput_possibleTypes = ['WishlistOutput']
module.exports.isWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishlistOutput"')
  return WishlistOutput_possibleTypes.includes(obj.__typename)
}



var Mutation_possibleTypes = ['Mutation']
module.exports.isMutation = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMutation"')
  return Mutation_possibleTypes.includes(obj.__typename)
}



var AddBundleProductsToCartOutput_possibleTypes = ['AddBundleProductsToCartOutput']
module.exports.isAddBundleProductsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddBundleProductsToCartOutput"')
  return AddBundleProductsToCartOutput_possibleTypes.includes(obj.__typename)
}



var AddConfigurableProductsToCartOutput_possibleTypes = ['AddConfigurableProductsToCartOutput']
module.exports.isAddConfigurableProductsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddConfigurableProductsToCartOutput"')
  return AddConfigurableProductsToCartOutput_possibleTypes.includes(obj.__typename)
}



var AddDownloadableProductsToCartOutput_possibleTypes = ['AddDownloadableProductsToCartOutput']
module.exports.isAddDownloadableProductsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddDownloadableProductsToCartOutput"')
  return AddDownloadableProductsToCartOutput_possibleTypes.includes(obj.__typename)
}



var AddGiftRegistryRegistrantsOutput_possibleTypes = ['AddGiftRegistryRegistrantsOutput']
module.exports.isAddGiftRegistryRegistrantsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddGiftRegistryRegistrantsOutput"')
  return AddGiftRegistryRegistrantsOutput_possibleTypes.includes(obj.__typename)
}



var AddProductsToCartOutput_possibleTypes = ['AddProductsToCartOutput']
module.exports.isAddProductsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddProductsToCartOutput"')
  return AddProductsToCartOutput_possibleTypes.includes(obj.__typename)
}



var CartUserInputError_possibleTypes = ['CartUserInputError']
module.exports.isCartUserInputError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCartUserInputError"')
  return CartUserInputError_possibleTypes.includes(obj.__typename)
}



var AddProductsToWishlistOutput_possibleTypes = ['AddProductsToWishlistOutput']
module.exports.isAddProductsToWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddProductsToWishlistOutput"')
  return AddProductsToWishlistOutput_possibleTypes.includes(obj.__typename)
}



var WishListUserInputError_possibleTypes = ['WishListUserInputError']
module.exports.isWishListUserInputError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishListUserInputError"')
  return WishListUserInputError_possibleTypes.includes(obj.__typename)
}



var AddReturnCommentOutput_possibleTypes = ['AddReturnCommentOutput']
module.exports.isAddReturnCommentOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddReturnCommentOutput"')
  return AddReturnCommentOutput_possibleTypes.includes(obj.__typename)
}



var AddReturnTrackingOutput_possibleTypes = ['AddReturnTrackingOutput']
module.exports.isAddReturnTrackingOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddReturnTrackingOutput"')
  return AddReturnTrackingOutput_possibleTypes.includes(obj.__typename)
}



var AddSimpleProductsToCartOutput_possibleTypes = ['AddSimpleProductsToCartOutput']
module.exports.isAddSimpleProductsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddSimpleProductsToCartOutput"')
  return AddSimpleProductsToCartOutput_possibleTypes.includes(obj.__typename)
}



var AddVirtualProductsToCartOutput_possibleTypes = ['AddVirtualProductsToCartOutput']
module.exports.isAddVirtualProductsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddVirtualProductsToCartOutput"')
  return AddVirtualProductsToCartOutput_possibleTypes.includes(obj.__typename)
}



var AddWishlistItemsToCartOutput_possibleTypes = ['AddWishlistItemsToCartOutput']
module.exports.isAddWishlistItemsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAddWishlistItemsToCartOutput"')
  return AddWishlistItemsToCartOutput_possibleTypes.includes(obj.__typename)
}



var WishlistCartUserInputError_possibleTypes = ['WishlistCartUserInputError']
module.exports.isWishlistCartUserInputError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isWishlistCartUserInputError"')
  return WishlistCartUserInputError_possibleTypes.includes(obj.__typename)
}



var ApplyCouponToCartOutput_possibleTypes = ['ApplyCouponToCartOutput']
module.exports.isApplyCouponToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isApplyCouponToCartOutput"')
  return ApplyCouponToCartOutput_possibleTypes.includes(obj.__typename)
}



var ApplyGiftCardToCartOutput_possibleTypes = ['ApplyGiftCardToCartOutput']
module.exports.isApplyGiftCardToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isApplyGiftCardToCartOutput"')
  return ApplyGiftCardToCartOutput_possibleTypes.includes(obj.__typename)
}



var ApplyRewardPointsToCartOutput_possibleTypes = ['ApplyRewardPointsToCartOutput']
module.exports.isApplyRewardPointsToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isApplyRewardPointsToCartOutput"')
  return ApplyRewardPointsToCartOutput_possibleTypes.includes(obj.__typename)
}



var ApplyStoreCreditToCartOutput_possibleTypes = ['ApplyStoreCreditToCartOutput']
module.exports.isApplyStoreCreditToCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isApplyStoreCreditToCartOutput"')
  return ApplyStoreCreditToCartOutput_possibleTypes.includes(obj.__typename)
}



var AssignCompareListToCustomerOutput_possibleTypes = ['AssignCompareListToCustomerOutput']
module.exports.isAssignCompareListToCustomerOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAssignCompareListToCustomerOutput"')
  return AssignCompareListToCustomerOutput_possibleTypes.includes(obj.__typename)
}



var ContactUsOutput_possibleTypes = ['ContactUsOutput']
module.exports.isContactUsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isContactUsOutput"')
  return ContactUsOutput_possibleTypes.includes(obj.__typename)
}



var CopyProductsBetweenWishlistsOutput_possibleTypes = ['CopyProductsBetweenWishlistsOutput']
module.exports.isCopyProductsBetweenWishlistsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCopyProductsBetweenWishlistsOutput"')
  return CopyProductsBetweenWishlistsOutput_possibleTypes.includes(obj.__typename)
}



var CustomerOutput_possibleTypes = ['CustomerOutput']
module.exports.isCustomerOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerOutput"')
  return CustomerOutput_possibleTypes.includes(obj.__typename)
}



var CreateGiftRegistryOutput_possibleTypes = ['CreateGiftRegistryOutput']
module.exports.isCreateGiftRegistryOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreateGiftRegistryOutput"')
  return CreateGiftRegistryOutput_possibleTypes.includes(obj.__typename)
}



var CreatePayflowProTokenOutput_possibleTypes = ['CreatePayflowProTokenOutput']
module.exports.isCreatePayflowProTokenOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreatePayflowProTokenOutput"')
  return CreatePayflowProTokenOutput_possibleTypes.includes(obj.__typename)
}



var PaypalExpressTokenOutput_possibleTypes = ['PaypalExpressTokenOutput']
module.exports.isPaypalExpressTokenOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPaypalExpressTokenOutput"')
  return PaypalExpressTokenOutput_possibleTypes.includes(obj.__typename)
}



var PaypalExpressUrlList_possibleTypes = ['PaypalExpressUrlList']
module.exports.isPaypalExpressUrlList = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPaypalExpressUrlList"')
  return PaypalExpressUrlList_possibleTypes.includes(obj.__typename)
}



var CreateProductReviewOutput_possibleTypes = ['CreateProductReviewOutput']
module.exports.isCreateProductReviewOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreateProductReviewOutput"')
  return CreateProductReviewOutput_possibleTypes.includes(obj.__typename)
}



var CreateWishlistOutput_possibleTypes = ['CreateWishlistOutput']
module.exports.isCreateWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreateWishlistOutput"')
  return CreateWishlistOutput_possibleTypes.includes(obj.__typename)
}



var DeleteCompareListOutput_possibleTypes = ['DeleteCompareListOutput']
module.exports.isDeleteCompareListOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDeleteCompareListOutput"')
  return DeleteCompareListOutput_possibleTypes.includes(obj.__typename)
}



var DeletePaymentTokenOutput_possibleTypes = ['DeletePaymentTokenOutput']
module.exports.isDeletePaymentTokenOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDeletePaymentTokenOutput"')
  return DeletePaymentTokenOutput_possibleTypes.includes(obj.__typename)
}



var DeleteWishlistOutput_possibleTypes = ['DeleteWishlistOutput']
module.exports.isDeleteWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDeleteWishlistOutput"')
  return DeleteWishlistOutput_possibleTypes.includes(obj.__typename)
}



var CustomerToken_possibleTypes = ['CustomerToken']
module.exports.isCustomerToken = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomerToken"')
  return CustomerToken_possibleTypes.includes(obj.__typename)
}



var GenerateCustomerTokenAsAdminOutput_possibleTypes = ['GenerateCustomerTokenAsAdminOutput']
module.exports.isGenerateCustomerTokenAsAdminOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGenerateCustomerTokenAsAdminOutput"')
  return GenerateCustomerTokenAsAdminOutput_possibleTypes.includes(obj.__typename)
}



var PayflowProResponseOutput_possibleTypes = ['PayflowProResponseOutput']
module.exports.isPayflowProResponseOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPayflowProResponseOutput"')
  return PayflowProResponseOutput_possibleTypes.includes(obj.__typename)
}



var MoveCartItemsToGiftRegistryOutput_possibleTypes = ['MoveCartItemsToGiftRegistryOutput']
module.exports.isMoveCartItemsToGiftRegistryOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMoveCartItemsToGiftRegistryOutput"')
  return MoveCartItemsToGiftRegistryOutput_possibleTypes.includes(obj.__typename)
}



var GiftRegistryOutputInterface_possibleTypes = ['MoveCartItemsToGiftRegistryOutput','GiftRegistryOutput']
module.exports.isGiftRegistryOutputInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryOutputInterface"')
  return GiftRegistryOutputInterface_possibleTypes.includes(obj.__typename)
}



var GiftRegistryItemUserErrorInterface_possibleTypes = ['MoveCartItemsToGiftRegistryOutput','GiftRegistryItemUserErrors']
module.exports.isGiftRegistryItemUserErrorInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryItemUserErrorInterface"')
  return GiftRegistryItemUserErrorInterface_possibleTypes.includes(obj.__typename)
}



var GiftRegistryItemsUserError_possibleTypes = ['GiftRegistryItemsUserError']
module.exports.isGiftRegistryItemsUserError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryItemsUserError"')
  return GiftRegistryItemsUserError_possibleTypes.includes(obj.__typename)
}



var MoveProductsBetweenWishlistsOutput_possibleTypes = ['MoveProductsBetweenWishlistsOutput']
module.exports.isMoveProductsBetweenWishlistsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isMoveProductsBetweenWishlistsOutput"')
  return MoveProductsBetweenWishlistsOutput_possibleTypes.includes(obj.__typename)
}



var PlaceOrderOutput_possibleTypes = ['PlaceOrderOutput']
module.exports.isPlaceOrderOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPlaceOrderOutput"')
  return PlaceOrderOutput_possibleTypes.includes(obj.__typename)
}



var Order_possibleTypes = ['Order']
module.exports.isOrder = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrder"')
  return Order_possibleTypes.includes(obj.__typename)
}



var RemoveCouponFromCartOutput_possibleTypes = ['RemoveCouponFromCartOutput']
module.exports.isRemoveCouponFromCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveCouponFromCartOutput"')
  return RemoveCouponFromCartOutput_possibleTypes.includes(obj.__typename)
}



var RemoveGiftCardFromCartOutput_possibleTypes = ['RemoveGiftCardFromCartOutput']
module.exports.isRemoveGiftCardFromCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveGiftCardFromCartOutput"')
  return RemoveGiftCardFromCartOutput_possibleTypes.includes(obj.__typename)
}



var RemoveGiftRegistryOutput_possibleTypes = ['RemoveGiftRegistryOutput']
module.exports.isRemoveGiftRegistryOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveGiftRegistryOutput"')
  return RemoveGiftRegistryOutput_possibleTypes.includes(obj.__typename)
}



var RemoveGiftRegistryItemsOutput_possibleTypes = ['RemoveGiftRegistryItemsOutput']
module.exports.isRemoveGiftRegistryItemsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveGiftRegistryItemsOutput"')
  return RemoveGiftRegistryItemsOutput_possibleTypes.includes(obj.__typename)
}



var RemoveGiftRegistryRegistrantsOutput_possibleTypes = ['RemoveGiftRegistryRegistrantsOutput']
module.exports.isRemoveGiftRegistryRegistrantsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveGiftRegistryRegistrantsOutput"')
  return RemoveGiftRegistryRegistrantsOutput_possibleTypes.includes(obj.__typename)
}



var RemoveItemFromCartOutput_possibleTypes = ['RemoveItemFromCartOutput']
module.exports.isRemoveItemFromCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveItemFromCartOutput"')
  return RemoveItemFromCartOutput_possibleTypes.includes(obj.__typename)
}



var RemoveProductsFromWishlistOutput_possibleTypes = ['RemoveProductsFromWishlistOutput']
module.exports.isRemoveProductsFromWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveProductsFromWishlistOutput"')
  return RemoveProductsFromWishlistOutput_possibleTypes.includes(obj.__typename)
}



var RemoveReturnTrackingOutput_possibleTypes = ['RemoveReturnTrackingOutput']
module.exports.isRemoveReturnTrackingOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveReturnTrackingOutput"')
  return RemoveReturnTrackingOutput_possibleTypes.includes(obj.__typename)
}



var RemoveRewardPointsFromCartOutput_possibleTypes = ['RemoveRewardPointsFromCartOutput']
module.exports.isRemoveRewardPointsFromCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveRewardPointsFromCartOutput"')
  return RemoveRewardPointsFromCartOutput_possibleTypes.includes(obj.__typename)
}



var RemoveStoreCreditFromCartOutput_possibleTypes = ['RemoveStoreCreditFromCartOutput']
module.exports.isRemoveStoreCreditFromCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRemoveStoreCreditFromCartOutput"')
  return RemoveStoreCreditFromCartOutput_possibleTypes.includes(obj.__typename)
}



var ReorderItemsOutput_possibleTypes = ['ReorderItemsOutput']
module.exports.isReorderItemsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isReorderItemsOutput"')
  return ReorderItemsOutput_possibleTypes.includes(obj.__typename)
}



var CheckoutUserInputError_possibleTypes = ['CheckoutUserInputError']
module.exports.isCheckoutUserInputError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCheckoutUserInputError"')
  return CheckoutUserInputError_possibleTypes.includes(obj.__typename)
}



var RequestReturnOutput_possibleTypes = ['RequestReturnOutput']
module.exports.isRequestReturnOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRequestReturnOutput"')
  return RequestReturnOutput_possibleTypes.includes(obj.__typename)
}



var RevokeCustomerTokenOutput_possibleTypes = ['RevokeCustomerTokenOutput']
module.exports.isRevokeCustomerTokenOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isRevokeCustomerTokenOutput"')
  return RevokeCustomerTokenOutput_possibleTypes.includes(obj.__typename)
}



var SendEmailToFriendOutput_possibleTypes = ['SendEmailToFriendOutput']
module.exports.isSendEmailToFriendOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSendEmailToFriendOutput"')
  return SendEmailToFriendOutput_possibleTypes.includes(obj.__typename)
}



var SendEmailToFriendRecipient_possibleTypes = ['SendEmailToFriendRecipient']
module.exports.isSendEmailToFriendRecipient = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSendEmailToFriendRecipient"')
  return SendEmailToFriendRecipient_possibleTypes.includes(obj.__typename)
}



var SendEmailToFriendSender_possibleTypes = ['SendEmailToFriendSender']
module.exports.isSendEmailToFriendSender = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSendEmailToFriendSender"')
  return SendEmailToFriendSender_possibleTypes.includes(obj.__typename)
}



var SetBillingAddressOnCartOutput_possibleTypes = ['SetBillingAddressOnCartOutput']
module.exports.isSetBillingAddressOnCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSetBillingAddressOnCartOutput"')
  return SetBillingAddressOnCartOutput_possibleTypes.includes(obj.__typename)
}



var SetGiftOptionsOnCartOutput_possibleTypes = ['SetGiftOptionsOnCartOutput']
module.exports.isSetGiftOptionsOnCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSetGiftOptionsOnCartOutput"')
  return SetGiftOptionsOnCartOutput_possibleTypes.includes(obj.__typename)
}



var SetGuestEmailOnCartOutput_possibleTypes = ['SetGuestEmailOnCartOutput']
module.exports.isSetGuestEmailOnCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSetGuestEmailOnCartOutput"')
  return SetGuestEmailOnCartOutput_possibleTypes.includes(obj.__typename)
}



var SetPaymentMethodOnCartOutput_possibleTypes = ['SetPaymentMethodOnCartOutput']
module.exports.isSetPaymentMethodOnCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSetPaymentMethodOnCartOutput"')
  return SetPaymentMethodOnCartOutput_possibleTypes.includes(obj.__typename)
}



var SetShippingAddressesOnCartOutput_possibleTypes = ['SetShippingAddressesOnCartOutput']
module.exports.isSetShippingAddressesOnCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSetShippingAddressesOnCartOutput"')
  return SetShippingAddressesOnCartOutput_possibleTypes.includes(obj.__typename)
}



var SetShippingMethodsOnCartOutput_possibleTypes = ['SetShippingMethodsOnCartOutput']
module.exports.isSetShippingMethodsOnCartOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSetShippingMethodsOnCartOutput"')
  return SetShippingMethodsOnCartOutput_possibleTypes.includes(obj.__typename)
}



var ShareGiftRegistryOutput_possibleTypes = ['ShareGiftRegistryOutput']
module.exports.isShareGiftRegistryOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShareGiftRegistryOutput"')
  return ShareGiftRegistryOutput_possibleTypes.includes(obj.__typename)
}



var SubscribeEmailToNewsletterOutput_possibleTypes = ['SubscribeEmailToNewsletterOutput']
module.exports.isSubscribeEmailToNewsletterOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSubscribeEmailToNewsletterOutput"')
  return SubscribeEmailToNewsletterOutput_possibleTypes.includes(obj.__typename)
}



var UpdateCartItemsOutput_possibleTypes = ['UpdateCartItemsOutput']
module.exports.isUpdateCartItemsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUpdateCartItemsOutput"')
  return UpdateCartItemsOutput_possibleTypes.includes(obj.__typename)
}



var UpdateGiftRegistryOutput_possibleTypes = ['UpdateGiftRegistryOutput']
module.exports.isUpdateGiftRegistryOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUpdateGiftRegistryOutput"')
  return UpdateGiftRegistryOutput_possibleTypes.includes(obj.__typename)
}



var UpdateGiftRegistryItemsOutput_possibleTypes = ['UpdateGiftRegistryItemsOutput']
module.exports.isUpdateGiftRegistryItemsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUpdateGiftRegistryItemsOutput"')
  return UpdateGiftRegistryItemsOutput_possibleTypes.includes(obj.__typename)
}



var UpdateGiftRegistryRegistrantsOutput_possibleTypes = ['UpdateGiftRegistryRegistrantsOutput']
module.exports.isUpdateGiftRegistryRegistrantsOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUpdateGiftRegistryRegistrantsOutput"')
  return UpdateGiftRegistryRegistrantsOutput_possibleTypes.includes(obj.__typename)
}



var UpdateProductsInWishlistOutput_possibleTypes = ['UpdateProductsInWishlistOutput']
module.exports.isUpdateProductsInWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUpdateProductsInWishlistOutput"')
  return UpdateProductsInWishlistOutput_possibleTypes.includes(obj.__typename)
}



var UpdateWishlistOutput_possibleTypes = ['UpdateWishlistOutput']
module.exports.isUpdateWishlistOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUpdateWishlistOutput"')
  return UpdateWishlistOutput_possibleTypes.includes(obj.__typename)
}



var ErrorInterface_possibleTypes = ['NoSuchEntityUidError','InternalError']
module.exports.isErrorInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isErrorInterface"')
  return ErrorInterface_possibleTypes.includes(obj.__typename)
}



var NoSuchEntityUidError_possibleTypes = ['NoSuchEntityUidError']
module.exports.isNoSuchEntityUidError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isNoSuchEntityUidError"')
  return NoSuchEntityUidError_possibleTypes.includes(obj.__typename)
}



var InternalError_possibleTypes = ['InternalError']
module.exports.isInternalError = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isInternalError"')
  return InternalError_possibleTypes.includes(obj.__typename)
}



var AttributeOptionsInterface_possibleTypes = ['AttributeOptions','UiAttributeTypeSelect','UiAttributeTypeMultiSelect','UiAttributeTypeBoolean']
module.exports.isAttributeOptionsInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttributeOptionsInterface"')
  return AttributeOptionsInterface_possibleTypes.includes(obj.__typename)
}



var SelectableInputTypeInterface_possibleTypes = ['UiAttributeTypeSelect','UiAttributeTypeMultiSelect','UiAttributeTypeBoolean']
module.exports.isSelectableInputTypeInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectableInputTypeInterface"')
  return SelectableInputTypeInterface_possibleTypes.includes(obj.__typename)
}



var AttributeOptions_possibleTypes = ['AttributeOptions']
module.exports.isAttributeOptions = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isAttributeOptions"')
  return AttributeOptions_possibleTypes.includes(obj.__typename)
}



var UiAttributeTypeSelect_possibleTypes = ['UiAttributeTypeSelect']
module.exports.isUiAttributeTypeSelect = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUiAttributeTypeSelect"')
  return UiAttributeTypeSelect_possibleTypes.includes(obj.__typename)
}



var UiAttributeTypeMultiSelect_possibleTypes = ['UiAttributeTypeMultiSelect']
module.exports.isUiAttributeTypeMultiSelect = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUiAttributeTypeMultiSelect"')
  return UiAttributeTypeMultiSelect_possibleTypes.includes(obj.__typename)
}



var UiAttributeTypeBoolean_possibleTypes = ['UiAttributeTypeBoolean']
module.exports.isUiAttributeTypeBoolean = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUiAttributeTypeBoolean"')
  return UiAttributeTypeBoolean_possibleTypes.includes(obj.__typename)
}



var UiAttributeTypeAny_possibleTypes = ['UiAttributeTypeAny']
module.exports.isUiAttributeTypeAny = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUiAttributeTypeAny"')
  return UiAttributeTypeAny_possibleTypes.includes(obj.__typename)
}



var ProductLinks_possibleTypes = ['ProductLinks']
module.exports.isProductLinks = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductLinks"')
  return ProductLinks_possibleTypes.includes(obj.__typename)
}



var PhysicalProductInterface_possibleTypes = ['SimpleProduct','BundleProduct','GroupedProduct','ConfigurableProduct','GiftCardProduct']
module.exports.isPhysicalProductInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPhysicalProductInterface"')
  return PhysicalProductInterface_possibleTypes.includes(obj.__typename)
}



var CustomizableAreaOption_possibleTypes = ['CustomizableAreaOption']
module.exports.isCustomizableAreaOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableAreaOption"')
  return CustomizableAreaOption_possibleTypes.includes(obj.__typename)
}



var CustomizableOptionInterface_possibleTypes = ['CustomizableAreaOption','CustomizableDateOption','CustomizableDropDownOption','CustomizableMultipleOption','CustomizableFieldOption','CustomizableFileOption','CustomizableRadioOption','CustomizableCheckboxOption']
module.exports.isCustomizableOptionInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableOptionInterface"')
  return CustomizableOptionInterface_possibleTypes.includes(obj.__typename)
}



var CustomizableAreaValue_possibleTypes = ['CustomizableAreaValue']
module.exports.isCustomizableAreaValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableAreaValue"')
  return CustomizableAreaValue_possibleTypes.includes(obj.__typename)
}



var CustomizableDateOption_possibleTypes = ['CustomizableDateOption']
module.exports.isCustomizableDateOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableDateOption"')
  return CustomizableDateOption_possibleTypes.includes(obj.__typename)
}



var CustomizableDateValue_possibleTypes = ['CustomizableDateValue']
module.exports.isCustomizableDateValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableDateValue"')
  return CustomizableDateValue_possibleTypes.includes(obj.__typename)
}



var CustomizableDropDownOption_possibleTypes = ['CustomizableDropDownOption']
module.exports.isCustomizableDropDownOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableDropDownOption"')
  return CustomizableDropDownOption_possibleTypes.includes(obj.__typename)
}



var CustomizableDropDownValue_possibleTypes = ['CustomizableDropDownValue']
module.exports.isCustomizableDropDownValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableDropDownValue"')
  return CustomizableDropDownValue_possibleTypes.includes(obj.__typename)
}



var CustomizableMultipleOption_possibleTypes = ['CustomizableMultipleOption']
module.exports.isCustomizableMultipleOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableMultipleOption"')
  return CustomizableMultipleOption_possibleTypes.includes(obj.__typename)
}



var CustomizableMultipleValue_possibleTypes = ['CustomizableMultipleValue']
module.exports.isCustomizableMultipleValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableMultipleValue"')
  return CustomizableMultipleValue_possibleTypes.includes(obj.__typename)
}



var CustomizableFieldOption_possibleTypes = ['CustomizableFieldOption']
module.exports.isCustomizableFieldOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableFieldOption"')
  return CustomizableFieldOption_possibleTypes.includes(obj.__typename)
}



var CustomizableFieldValue_possibleTypes = ['CustomizableFieldValue']
module.exports.isCustomizableFieldValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableFieldValue"')
  return CustomizableFieldValue_possibleTypes.includes(obj.__typename)
}



var CustomizableFileOption_possibleTypes = ['CustomizableFileOption']
module.exports.isCustomizableFileOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableFileOption"')
  return CustomizableFileOption_possibleTypes.includes(obj.__typename)
}



var CustomizableFileValue_possibleTypes = ['CustomizableFileValue']
module.exports.isCustomizableFileValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableFileValue"')
  return CustomizableFileValue_possibleTypes.includes(obj.__typename)
}



var ProductVideo_possibleTypes = ['ProductVideo']
module.exports.isProductVideo = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductVideo"')
  return ProductVideo_possibleTypes.includes(obj.__typename)
}



var CustomizableProductInterface_possibleTypes = ['VirtualProduct','SimpleProduct','DownloadableProduct','BundleProduct','ConfigurableProduct','GiftCardProduct']
module.exports.isCustomizableProductInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableProductInterface"')
  return CustomizableProductInterface_possibleTypes.includes(obj.__typename)
}



var CustomizableRadioOption_possibleTypes = ['CustomizableRadioOption']
module.exports.isCustomizableRadioOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableRadioOption"')
  return CustomizableRadioOption_possibleTypes.includes(obj.__typename)
}



var CustomizableRadioValue_possibleTypes = ['CustomizableRadioValue']
module.exports.isCustomizableRadioValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableRadioValue"')
  return CustomizableRadioValue_possibleTypes.includes(obj.__typename)
}



var CustomizableCheckboxOption_possibleTypes = ['CustomizableCheckboxOption']
module.exports.isCustomizableCheckboxOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableCheckboxOption"')
  return CustomizableCheckboxOption_possibleTypes.includes(obj.__typename)
}



var CustomizableCheckboxValue_possibleTypes = ['CustomizableCheckboxValue']
module.exports.isCustomizableCheckboxValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCustomizableCheckboxValue"')
  return CustomizableCheckboxValue_possibleTypes.includes(obj.__typename)
}



var VirtualProduct_possibleTypes = ['VirtualProduct']
module.exports.isVirtualProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isVirtualProduct"')
  return VirtualProduct_possibleTypes.includes(obj.__typename)
}



var SimpleProduct_possibleTypes = ['SimpleProduct']
module.exports.isSimpleProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSimpleProduct"')
  return SimpleProduct_possibleTypes.includes(obj.__typename)
}



var LayerFilterItem_possibleTypes = ['LayerFilterItem']
module.exports.isLayerFilterItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isLayerFilterItem"')
  return LayerFilterItem_possibleTypes.includes(obj.__typename)
}



var SimpleWishlistItem_possibleTypes = ['SimpleWishlistItem']
module.exports.isSimpleWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSimpleWishlistItem"')
  return SimpleWishlistItem_possibleTypes.includes(obj.__typename)
}



var VirtualWishlistItem_possibleTypes = ['VirtualWishlistItem']
module.exports.isVirtualWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isVirtualWishlistItem"')
  return VirtualWishlistItem_possibleTypes.includes(obj.__typename)
}



var SimpleCartItem_possibleTypes = ['SimpleCartItem']
module.exports.isSimpleCartItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSimpleCartItem"')
  return SimpleCartItem_possibleTypes.includes(obj.__typename)
}



var VirtualCartItem_possibleTypes = ['VirtualCartItem']
module.exports.isVirtualCartItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isVirtualCartItem"')
  return VirtualCartItem_possibleTypes.includes(obj.__typename)
}



var DownloadableCartItem_possibleTypes = ['DownloadableCartItem']
module.exports.isDownloadableCartItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableCartItem"')
  return DownloadableCartItem_possibleTypes.includes(obj.__typename)
}



var DownloadableProductLinks_possibleTypes = ['DownloadableProductLinks']
module.exports.isDownloadableProductLinks = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableProductLinks"')
  return DownloadableProductLinks_possibleTypes.includes(obj.__typename)
}



var DownloadableProductSamples_possibleTypes = ['DownloadableProductSamples']
module.exports.isDownloadableProductSamples = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableProductSamples"')
  return DownloadableProductSamples_possibleTypes.includes(obj.__typename)
}



var DownloadableProduct_possibleTypes = ['DownloadableProduct']
module.exports.isDownloadableProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableProduct"')
  return DownloadableProduct_possibleTypes.includes(obj.__typename)
}



var DownloadableOrderItem_possibleTypes = ['DownloadableOrderItem']
module.exports.isDownloadableOrderItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableOrderItem"')
  return DownloadableOrderItem_possibleTypes.includes(obj.__typename)
}



var DownloadableItemsLinks_possibleTypes = ['DownloadableItemsLinks']
module.exports.isDownloadableItemsLinks = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableItemsLinks"')
  return DownloadableItemsLinks_possibleTypes.includes(obj.__typename)
}



var DownloadableInvoiceItem_possibleTypes = ['DownloadableInvoiceItem']
module.exports.isDownloadableInvoiceItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableInvoiceItem"')
  return DownloadableInvoiceItem_possibleTypes.includes(obj.__typename)
}



var DownloadableCreditMemoItem_possibleTypes = ['DownloadableCreditMemoItem']
module.exports.isDownloadableCreditMemoItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableCreditMemoItem"')
  return DownloadableCreditMemoItem_possibleTypes.includes(obj.__typename)
}



var DownloadableWishlistItem_possibleTypes = ['DownloadableWishlistItem']
module.exports.isDownloadableWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isDownloadableWishlistItem"')
  return DownloadableWishlistItem_possibleTypes.includes(obj.__typename)
}



var ProductAttributeMetadata_possibleTypes = ['ProductAttributeMetadata']
module.exports.isProductAttributeMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isProductAttributeMetadata"')
  return ProductAttributeMetadata_possibleTypes.includes(obj.__typename)
}



var ConfigurableCartItem_possibleTypes = ['ConfigurableCartItem']
module.exports.isConfigurableCartItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableCartItem"')
  return ConfigurableCartItem_possibleTypes.includes(obj.__typename)
}



var SelectedConfigurableOption_possibleTypes = ['SelectedConfigurableOption']
module.exports.isSelectedConfigurableOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedConfigurableOption"')
  return SelectedConfigurableOption_possibleTypes.includes(obj.__typename)
}



var BundleCartItem_possibleTypes = ['BundleCartItem']
module.exports.isBundleCartItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleCartItem"')
  return BundleCartItem_possibleTypes.includes(obj.__typename)
}



var SelectedBundleOption_possibleTypes = ['SelectedBundleOption']
module.exports.isSelectedBundleOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedBundleOption"')
  return SelectedBundleOption_possibleTypes.includes(obj.__typename)
}



var SelectedBundleOptionValue_possibleTypes = ['SelectedBundleOptionValue']
module.exports.isSelectedBundleOptionValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSelectedBundleOptionValue"')
  return SelectedBundleOptionValue_possibleTypes.includes(obj.__typename)
}



var SalesItemInterface_possibleTypes = ['SalesItemInterface']
module.exports.isSalesItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSalesItemInterface"')
  return SalesItemInterface_possibleTypes.includes(obj.__typename)
}



var GiftRegistryOutput_possibleTypes = ['GiftRegistryOutput']
module.exports.isGiftRegistryOutput = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryOutput"')
  return GiftRegistryOutput_possibleTypes.includes(obj.__typename)
}



var GiftRegistryItemUserErrors_possibleTypes = ['GiftRegistryItemUserErrors']
module.exports.isGiftRegistryItemUserErrors = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryItemUserErrors"')
  return GiftRegistryItemUserErrors_possibleTypes.includes(obj.__typename)
}



var GiftRegistryDynamicAttributeMetadata_possibleTypes = ['GiftRegistryDynamicAttributeMetadata']
module.exports.isGiftRegistryDynamicAttributeMetadata = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryDynamicAttributeMetadata"')
  return GiftRegistryDynamicAttributeMetadata_possibleTypes.includes(obj.__typename)
}



var GiftRegistryItem_possibleTypes = ['GiftRegistryItem']
module.exports.isGiftRegistryItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftRegistryItem"')
  return GiftRegistryItem_possibleTypes.includes(obj.__typename)
}



var BundleItem_possibleTypes = ['BundleItem']
module.exports.isBundleItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleItem"')
  return BundleItem_possibleTypes.includes(obj.__typename)
}



var BundleItemOption_possibleTypes = ['BundleItemOption']
module.exports.isBundleItemOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleItemOption"')
  return BundleItemOption_possibleTypes.includes(obj.__typename)
}



var BundleProduct_possibleTypes = ['BundleProduct']
module.exports.isBundleProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleProduct"')
  return BundleProduct_possibleTypes.includes(obj.__typename)
}



var BundleOrderItem_possibleTypes = ['BundleOrderItem']
module.exports.isBundleOrderItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleOrderItem"')
  return BundleOrderItem_possibleTypes.includes(obj.__typename)
}



var ItemSelectedBundleOption_possibleTypes = ['ItemSelectedBundleOption']
module.exports.isItemSelectedBundleOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isItemSelectedBundleOption"')
  return ItemSelectedBundleOption_possibleTypes.includes(obj.__typename)
}



var ItemSelectedBundleOptionValue_possibleTypes = ['ItemSelectedBundleOptionValue']
module.exports.isItemSelectedBundleOptionValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isItemSelectedBundleOptionValue"')
  return ItemSelectedBundleOptionValue_possibleTypes.includes(obj.__typename)
}



var BundleInvoiceItem_possibleTypes = ['BundleInvoiceItem']
module.exports.isBundleInvoiceItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleInvoiceItem"')
  return BundleInvoiceItem_possibleTypes.includes(obj.__typename)
}



var BundleShipmentItem_possibleTypes = ['BundleShipmentItem']
module.exports.isBundleShipmentItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleShipmentItem"')
  return BundleShipmentItem_possibleTypes.includes(obj.__typename)
}



var BundleCreditMemoItem_possibleTypes = ['BundleCreditMemoItem']
module.exports.isBundleCreditMemoItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleCreditMemoItem"')
  return BundleCreditMemoItem_possibleTypes.includes(obj.__typename)
}



var BundleWishlistItem_possibleTypes = ['BundleWishlistItem']
module.exports.isBundleWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isBundleWishlistItem"')
  return BundleWishlistItem_possibleTypes.includes(obj.__typename)
}



var GroupedProduct_possibleTypes = ['GroupedProduct']
module.exports.isGroupedProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGroupedProduct"')
  return GroupedProduct_possibleTypes.includes(obj.__typename)
}



var GroupedProductItem_possibleTypes = ['GroupedProductItem']
module.exports.isGroupedProductItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGroupedProductItem"')
  return GroupedProductItem_possibleTypes.includes(obj.__typename)
}



var GroupedProductWishlistItem_possibleTypes = ['GroupedProductWishlistItem']
module.exports.isGroupedProductWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGroupedProductWishlistItem"')
  return GroupedProductWishlistItem_possibleTypes.includes(obj.__typename)
}



var ConfigurableProduct_possibleTypes = ['ConfigurableProduct']
module.exports.isConfigurableProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableProduct"')
  return ConfigurableProduct_possibleTypes.includes(obj.__typename)
}



var ConfigurableProductOptions_possibleTypes = ['ConfigurableProductOptions']
module.exports.isConfigurableProductOptions = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableProductOptions"')
  return ConfigurableProductOptions_possibleTypes.includes(obj.__typename)
}



var ConfigurableProductOptionsValues_possibleTypes = ['ConfigurableProductOptionsValues']
module.exports.isConfigurableProductOptionsValues = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableProductOptionsValues"')
  return ConfigurableProductOptionsValues_possibleTypes.includes(obj.__typename)
}



var SwatchDataInterface_possibleTypes = ['ImageSwatchData','TextSwatchData','ColorSwatchData']
module.exports.isSwatchDataInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSwatchDataInterface"')
  return SwatchDataInterface_possibleTypes.includes(obj.__typename)
}



var ConfigurableProductOptionsSelection_possibleTypes = ['ConfigurableProductOptionsSelection']
module.exports.isConfigurableProductOptionsSelection = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableProductOptionsSelection"')
  return ConfigurableProductOptionsSelection_possibleTypes.includes(obj.__typename)
}



var ConfigurableProductOption_possibleTypes = ['ConfigurableProductOption']
module.exports.isConfigurableProductOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableProductOption"')
  return ConfigurableProductOption_possibleTypes.includes(obj.__typename)
}



var ConfigurableProductOptionValue_possibleTypes = ['ConfigurableProductOptionValue']
module.exports.isConfigurableProductOptionValue = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableProductOptionValue"')
  return ConfigurableProductOptionValue_possibleTypes.includes(obj.__typename)
}



var ConfigurableOptionAvailableForSelection_possibleTypes = ['ConfigurableOptionAvailableForSelection']
module.exports.isConfigurableOptionAvailableForSelection = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableOptionAvailableForSelection"')
  return ConfigurableOptionAvailableForSelection_possibleTypes.includes(obj.__typename)
}



var ConfigurableVariant_possibleTypes = ['ConfigurableVariant']
module.exports.isConfigurableVariant = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableVariant"')
  return ConfigurableVariant_possibleTypes.includes(obj.__typename)
}



var ConfigurableAttributeOption_possibleTypes = ['ConfigurableAttributeOption']
module.exports.isConfigurableAttributeOption = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableAttributeOption"')
  return ConfigurableAttributeOption_possibleTypes.includes(obj.__typename)
}



var ConfigurableWishlistItem_possibleTypes = ['ConfigurableWishlistItem']
module.exports.isConfigurableWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isConfigurableWishlistItem"')
  return ConfigurableWishlistItem_possibleTypes.includes(obj.__typename)
}



var OrderItem_possibleTypes = ['OrderItem']
module.exports.isOrderItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isOrderItem"')
  return OrderItem_possibleTypes.includes(obj.__typename)
}



var InvoiceItem_possibleTypes = ['InvoiceItem']
module.exports.isInvoiceItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isInvoiceItem"')
  return InvoiceItem_possibleTypes.includes(obj.__typename)
}



var ShipmentItem_possibleTypes = ['ShipmentItem']
module.exports.isShipmentItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isShipmentItem"')
  return ShipmentItem_possibleTypes.includes(obj.__typename)
}



var CreditMemoItem_possibleTypes = ['CreditMemoItem']
module.exports.isCreditMemoItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isCreditMemoItem"')
  return CreditMemoItem_possibleTypes.includes(obj.__typename)
}



var PaypalExpressToken_possibleTypes = ['PaypalExpressToken']
module.exports.isPaypalExpressToken = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPaypalExpressToken"')
  return PaypalExpressToken_possibleTypes.includes(obj.__typename)
}



var PayflowProToken_possibleTypes = ['PayflowProToken']
module.exports.isPayflowProToken = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isPayflowProToken"')
  return PayflowProToken_possibleTypes.includes(obj.__typename)
}



var SwatchLayerFilterItemInterface_possibleTypes = ['SwatchLayerFilterItem']
module.exports.isSwatchLayerFilterItemInterface = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSwatchLayerFilterItemInterface"')
  return SwatchLayerFilterItemInterface_possibleTypes.includes(obj.__typename)
}



var SwatchData_possibleTypes = ['SwatchData']
module.exports.isSwatchData = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSwatchData"')
  return SwatchData_possibleTypes.includes(obj.__typename)
}



var SwatchLayerFilterItem_possibleTypes = ['SwatchLayerFilterItem']
module.exports.isSwatchLayerFilterItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isSwatchLayerFilterItem"')
  return SwatchLayerFilterItem_possibleTypes.includes(obj.__typename)
}



var ImageSwatchData_possibleTypes = ['ImageSwatchData']
module.exports.isImageSwatchData = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isImageSwatchData"')
  return ImageSwatchData_possibleTypes.includes(obj.__typename)
}



var TextSwatchData_possibleTypes = ['TextSwatchData']
module.exports.isTextSwatchData = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isTextSwatchData"')
  return TextSwatchData_possibleTypes.includes(obj.__typename)
}



var ColorSwatchData_possibleTypes = ['ColorSwatchData']
module.exports.isColorSwatchData = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isColorSwatchData"')
  return ColorSwatchData_possibleTypes.includes(obj.__typename)
}



var UiAttributeTypeFixedProductTax_possibleTypes = ['UiAttributeTypeFixedProductTax']
module.exports.isUiAttributeTypeFixedProductTax = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isUiAttributeTypeFixedProductTax"')
  return UiAttributeTypeFixedProductTax_possibleTypes.includes(obj.__typename)
}



var GiftCardProduct_possibleTypes = ['GiftCardProduct']
module.exports.isGiftCardProduct = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardProduct"')
  return GiftCardProduct_possibleTypes.includes(obj.__typename)
}



var GiftCardAmounts_possibleTypes = ['GiftCardAmounts']
module.exports.isGiftCardAmounts = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardAmounts"')
  return GiftCardAmounts_possibleTypes.includes(obj.__typename)
}



var GiftCardOrderItem_possibleTypes = ['GiftCardOrderItem']
module.exports.isGiftCardOrderItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardOrderItem"')
  return GiftCardOrderItem_possibleTypes.includes(obj.__typename)
}



var GiftCardItem_possibleTypes = ['GiftCardItem']
module.exports.isGiftCardItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardItem"')
  return GiftCardItem_possibleTypes.includes(obj.__typename)
}



var GiftCardInvoiceItem_possibleTypes = ['GiftCardInvoiceItem']
module.exports.isGiftCardInvoiceItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardInvoiceItem"')
  return GiftCardInvoiceItem_possibleTypes.includes(obj.__typename)
}



var GiftCardCreditMemoItem_possibleTypes = ['GiftCardCreditMemoItem']
module.exports.isGiftCardCreditMemoItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardCreditMemoItem"')
  return GiftCardCreditMemoItem_possibleTypes.includes(obj.__typename)
}



var GiftCardShipmentItem_possibleTypes = ['GiftCardShipmentItem']
module.exports.isGiftCardShipmentItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardShipmentItem"')
  return GiftCardShipmentItem_possibleTypes.includes(obj.__typename)
}



var GiftCardCartItem_possibleTypes = ['GiftCardCartItem']
module.exports.isGiftCardCartItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardCartItem"')
  return GiftCardCartItem_possibleTypes.includes(obj.__typename)
}



var GiftCardWishlistItem_possibleTypes = ['GiftCardWishlistItem']
module.exports.isGiftCardWishlistItem = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardWishlistItem"')
  return GiftCardWishlistItem_possibleTypes.includes(obj.__typename)
}



var GiftCardOptions_possibleTypes = ['GiftCardOptions']
module.exports.isGiftCardOptions = function(obj) {
  if (!obj || !obj.__typename) throw new Error('__typename is missing in "isGiftCardOptions"')
  return GiftCardOptions_possibleTypes.includes(obj.__typename)
}
