import {
  FieldsSelection,
  GraphqlOperation,
  ClientOptions,
  Observable,
} from '@genql/runtime'
import { SubscriptionClient } from 'subscriptions-transport-ws'
export * from './schema'
import {
  QueryRequest,
  QueryPromiseChain,
  Query,
  MutationRequest,
  MutationPromiseChain,
  Mutation,
} from './schema'
export declare const createClient: (options?: ClientOptions) => Client
export declare const everything: { __scalar: boolean }
export declare const version: string

export interface Client {
  wsClient?: SubscriptionClient

  query<R extends QueryRequest>(
    request: R & { __name?: string },
  ): Promise<FieldsSelection<Query, R>>

  mutation<R extends MutationRequest>(
    request: R & { __name?: string },
  ): Promise<FieldsSelection<Mutation, R>>

  chain: {
    query: QueryPromiseChain

    mutation: MutationPromiseChain
  }
}

export type QueryResult<fields extends QueryRequest> = FieldsSelection<
  Query,
  fields
>

export declare const generateQueryOp: (
  fields: QueryRequest & { __name?: string },
) => GraphqlOperation
export type MutationResult<fields extends MutationRequest> = FieldsSelection<
  Mutation,
  fields
>

export declare const generateMutationOp: (
  fields: MutationRequest & { __name?: string },
) => GraphqlOperation

export declare const enumAttributeEntityTypeEnum: {
  readonly PRODUCT: 'PRODUCT'
}

export declare const enumObjectDataTypeEnum: {
  readonly STRING: 'STRING'
  readonly FLOAT: 'FLOAT'
  readonly INT: 'INT'
  readonly BOOLEAN: 'BOOLEAN'
  readonly COMPLEX: 'COMPLEX'
}

export declare const enumUiInputTypeEnum: {
  readonly BOOLEAN: 'BOOLEAN'
  readonly DATE: 'DATE'
  readonly DATETIME: 'DATETIME'
  readonly GALLERY: 'GALLERY'
  readonly IMAGE: 'IMAGE'
  readonly MEDIA_IMAGE: 'MEDIA_IMAGE'
  readonly MULTISELECT: 'MULTISELECT'
  readonly PRICE: 'PRICE'
  readonly SELECT: 'SELECT'
  readonly TEXT: 'TEXT'
  readonly TEXTAREA: 'TEXTAREA'
  readonly WEIGHT: 'WEIGHT'
  readonly FIXED_PRODUCT_TAX: 'FIXED_PRODUCT_TAX'
}

export declare const enumFixedProductTaxDisplaySettings: {
  readonly INCLUDE_FPT_WITHOUT_DETAILS: 'INCLUDE_FPT_WITHOUT_DETAILS'
  readonly INCLUDE_FPT_WITH_DETAILS: 'INCLUDE_FPT_WITH_DETAILS'
  readonly EXCLUDE_FPT_AND_INCLUDE_WITH_DETAILS: 'EXCLUDE_FPT_AND_INCLUDE_WITH_DETAILS'
  readonly EXCLUDE_FPT_WITHOUT_DETAILS: 'EXCLUDE_FPT_WITHOUT_DETAILS'
  readonly FPT_DISABLED: 'FPT_DISABLED'
}

export declare const enumCurrencyEnum: {
  readonly AFN: 'AFN'
  readonly ALL: 'ALL'
  readonly AZN: 'AZN'
  readonly DZD: 'DZD'
  readonly AOA: 'AOA'
  readonly ARS: 'ARS'
  readonly AMD: 'AMD'
  readonly AWG: 'AWG'
  readonly AUD: 'AUD'
  readonly BSD: 'BSD'
  readonly BHD: 'BHD'
  readonly BDT: 'BDT'
  readonly BBD: 'BBD'
  readonly BYN: 'BYN'
  readonly BZD: 'BZD'
  readonly BMD: 'BMD'
  readonly BTN: 'BTN'
  readonly BOB: 'BOB'
  readonly BAM: 'BAM'
  readonly BWP: 'BWP'
  readonly BRL: 'BRL'
  readonly GBP: 'GBP'
  readonly BND: 'BND'
  readonly BGN: 'BGN'
  readonly BUK: 'BUK'
  readonly BIF: 'BIF'
  readonly KHR: 'KHR'
  readonly CAD: 'CAD'
  readonly CVE: 'CVE'
  readonly CZK: 'CZK'
  readonly KYD: 'KYD'
  readonly GQE: 'GQE'
  readonly CLP: 'CLP'
  readonly CNY: 'CNY'
  readonly COP: 'COP'
  readonly KMF: 'KMF'
  readonly CDF: 'CDF'
  readonly CRC: 'CRC'
  readonly HRK: 'HRK'
  readonly CUP: 'CUP'
  readonly DKK: 'DKK'
  readonly DJF: 'DJF'
  readonly DOP: 'DOP'
  readonly XCD: 'XCD'
  readonly EGP: 'EGP'
  readonly SVC: 'SVC'
  readonly ERN: 'ERN'
  readonly EEK: 'EEK'
  readonly ETB: 'ETB'
  readonly EUR: 'EUR'
  readonly FKP: 'FKP'
  readonly FJD: 'FJD'
  readonly GMD: 'GMD'
  readonly GEK: 'GEK'
  readonly GEL: 'GEL'
  readonly GHS: 'GHS'
  readonly GIP: 'GIP'
  readonly GTQ: 'GTQ'
  readonly GNF: 'GNF'
  readonly GYD: 'GYD'
  readonly HTG: 'HTG'
  readonly HNL: 'HNL'
  readonly HKD: 'HKD'
  readonly HUF: 'HUF'
  readonly ISK: 'ISK'
  readonly INR: 'INR'
  readonly IDR: 'IDR'
  readonly IRR: 'IRR'
  readonly IQD: 'IQD'
  readonly ILS: 'ILS'
  readonly JMD: 'JMD'
  readonly JPY: 'JPY'
  readonly JOD: 'JOD'
  readonly KZT: 'KZT'
  readonly KES: 'KES'
  readonly KWD: 'KWD'
  readonly KGS: 'KGS'
  readonly LAK: 'LAK'
  readonly LVL: 'LVL'
  readonly LBP: 'LBP'
  readonly LSL: 'LSL'
  readonly LRD: 'LRD'
  readonly LYD: 'LYD'
  readonly LTL: 'LTL'
  readonly MOP: 'MOP'
  readonly MKD: 'MKD'
  readonly MGA: 'MGA'
  readonly MWK: 'MWK'
  readonly MYR: 'MYR'
  readonly MVR: 'MVR'
  readonly LSM: 'LSM'
  readonly MRO: 'MRO'
  readonly MUR: 'MUR'
  readonly MXN: 'MXN'
  readonly MDL: 'MDL'
  readonly MNT: 'MNT'
  readonly MAD: 'MAD'
  readonly MZN: 'MZN'
  readonly MMK: 'MMK'
  readonly NAD: 'NAD'
  readonly NPR: 'NPR'
  readonly ANG: 'ANG'
  readonly YTL: 'YTL'
  readonly NZD: 'NZD'
  readonly NIC: 'NIC'
  readonly NGN: 'NGN'
  readonly KPW: 'KPW'
  readonly NOK: 'NOK'
  readonly OMR: 'OMR'
  readonly PKR: 'PKR'
  readonly PAB: 'PAB'
  readonly PGK: 'PGK'
  readonly PYG: 'PYG'
  readonly PEN: 'PEN'
  readonly PHP: 'PHP'
  readonly PLN: 'PLN'
  readonly QAR: 'QAR'
  readonly RHD: 'RHD'
  readonly RON: 'RON'
  readonly RUB: 'RUB'
  readonly RWF: 'RWF'
  readonly SHP: 'SHP'
  readonly STD: 'STD'
  readonly SAR: 'SAR'
  readonly RSD: 'RSD'
  readonly SCR: 'SCR'
  readonly SLL: 'SLL'
  readonly SGD: 'SGD'
  readonly SKK: 'SKK'
  readonly SBD: 'SBD'
  readonly SOS: 'SOS'
  readonly ZAR: 'ZAR'
  readonly KRW: 'KRW'
  readonly LKR: 'LKR'
  readonly SDG: 'SDG'
  readonly SRD: 'SRD'
  readonly SZL: 'SZL'
  readonly SEK: 'SEK'
  readonly CHF: 'CHF'
  readonly SYP: 'SYP'
  readonly TWD: 'TWD'
  readonly TJS: 'TJS'
  readonly TZS: 'TZS'
  readonly THB: 'THB'
  readonly TOP: 'TOP'
  readonly TTD: 'TTD'
  readonly TND: 'TND'
  readonly TMM: 'TMM'
  readonly USD: 'USD'
  readonly UGX: 'UGX'
  readonly UAH: 'UAH'
  readonly AED: 'AED'
  readonly UYU: 'UYU'
  readonly UZS: 'UZS'
  readonly VUV: 'VUV'
  readonly VEB: 'VEB'
  readonly VEF: 'VEF'
  readonly VND: 'VND'
  readonly CHE: 'CHE'
  readonly CHW: 'CHW'
  readonly XOF: 'XOF'
  readonly WST: 'WST'
  readonly YER: 'YER'
  readonly ZMK: 'ZMK'
  readonly ZWD: 'ZWD'
  readonly TRY: 'TRY'
  readonly AZM: 'AZM'
  readonly ROL: 'ROL'
  readonly TRL: 'TRL'
  readonly XPF: 'XPF'
}

export declare const enumCartItemErrorType: {
  readonly UNDEFINED: 'UNDEFINED'
  readonly ITEM_QTY: 'ITEM_QTY'
  readonly ITEM_INCREMENTS: 'ITEM_INCREMENTS'
}

export declare const enumSortEnum: {
  readonly ASC: 'ASC'
  readonly DESC: 'DESC'
}

export declare const enumPriceAdjustmentCodesEnum: {
  readonly TAX: 'TAX'
  readonly WEEE: 'WEEE'
  readonly WEEE_TAX: 'WEEE_TAX'
}

export declare const enumPriceAdjustmentDescriptionEnum: {
  readonly INCLUDED: 'INCLUDED'
  readonly EXCLUDED: 'EXCLUDED'
}

export declare const enumProductStockStatus: {
  readonly IN_STOCK: 'IN_STOCK'
  readonly OUT_OF_STOCK: 'OUT_OF_STOCK'
}

export declare const enumUrlRewriteEntityTypeEnum: {
  readonly CMS_PAGE: 'CMS_PAGE'
  readonly PRODUCT: 'PRODUCT'
  readonly CATEGORY: 'CATEGORY'
}

export declare const enumCheckoutAgreementMode: {
  readonly AUTO: 'AUTO'
  readonly MANUAL: 'MANUAL'
}

export declare const enumUseInLayeredNavigationOptions: {
  readonly NO: 'NO'
  readonly FILTERABLE_WITH_RESULTS: 'FILTERABLE_WITH_RESULTS'
  readonly FILTERABLE_NO_RESULT: 'FILTERABLE_NO_RESULT'
}

export declare const enumCountryCodeEnum: {
  readonly AF: 'AF'
  readonly AX: 'AX'
  readonly AL: 'AL'
  readonly DZ: 'DZ'
  readonly AS: 'AS'
  readonly AD: 'AD'
  readonly AO: 'AO'
  readonly AI: 'AI'
  readonly AQ: 'AQ'
  readonly AG: 'AG'
  readonly AR: 'AR'
  readonly AM: 'AM'
  readonly AW: 'AW'
  readonly AU: 'AU'
  readonly AT: 'AT'
  readonly AZ: 'AZ'
  readonly BS: 'BS'
  readonly BH: 'BH'
  readonly BD: 'BD'
  readonly BB: 'BB'
  readonly BY: 'BY'
  readonly BE: 'BE'
  readonly BZ: 'BZ'
  readonly BJ: 'BJ'
  readonly BM: 'BM'
  readonly BT: 'BT'
  readonly BO: 'BO'
  readonly BA: 'BA'
  readonly BW: 'BW'
  readonly BV: 'BV'
  readonly BR: 'BR'
  readonly IO: 'IO'
  readonly VG: 'VG'
  readonly BN: 'BN'
  readonly BG: 'BG'
  readonly BF: 'BF'
  readonly BI: 'BI'
  readonly KH: 'KH'
  readonly CM: 'CM'
  readonly CA: 'CA'
  readonly CV: 'CV'
  readonly KY: 'KY'
  readonly CF: 'CF'
  readonly TD: 'TD'
  readonly CL: 'CL'
  readonly CN: 'CN'
  readonly CX: 'CX'
  readonly CC: 'CC'
  readonly CO: 'CO'
  readonly KM: 'KM'
  readonly CG: 'CG'
  readonly CD: 'CD'
  readonly CK: 'CK'
  readonly CR: 'CR'
  readonly CI: 'CI'
  readonly HR: 'HR'
  readonly CU: 'CU'
  readonly CY: 'CY'
  readonly CZ: 'CZ'
  readonly DK: 'DK'
  readonly DJ: 'DJ'
  readonly DM: 'DM'
  readonly DO: 'DO'
  readonly EC: 'EC'
  readonly EG: 'EG'
  readonly SV: 'SV'
  readonly GQ: 'GQ'
  readonly ER: 'ER'
  readonly EE: 'EE'
  readonly ET: 'ET'
  readonly FK: 'FK'
  readonly FO: 'FO'
  readonly FJ: 'FJ'
  readonly FI: 'FI'
  readonly FR: 'FR'
  readonly GF: 'GF'
  readonly PF: 'PF'
  readonly TF: 'TF'
  readonly GA: 'GA'
  readonly GM: 'GM'
  readonly GE: 'GE'
  readonly DE: 'DE'
  readonly GH: 'GH'
  readonly GI: 'GI'
  readonly GR: 'GR'
  readonly GL: 'GL'
  readonly GD: 'GD'
  readonly GP: 'GP'
  readonly GU: 'GU'
  readonly GT: 'GT'
  readonly GG: 'GG'
  readonly GN: 'GN'
  readonly GW: 'GW'
  readonly GY: 'GY'
  readonly HT: 'HT'
  readonly HM: 'HM'
  readonly HN: 'HN'
  readonly HK: 'HK'
  readonly HU: 'HU'
  readonly IS: 'IS'
  readonly IN: 'IN'
  readonly ID: 'ID'
  readonly IR: 'IR'
  readonly IQ: 'IQ'
  readonly IE: 'IE'
  readonly IM: 'IM'
  readonly IL: 'IL'
  readonly IT: 'IT'
  readonly JM: 'JM'
  readonly JP: 'JP'
  readonly JE: 'JE'
  readonly JO: 'JO'
  readonly KZ: 'KZ'
  readonly KE: 'KE'
  readonly KI: 'KI'
  readonly KW: 'KW'
  readonly KG: 'KG'
  readonly LA: 'LA'
  readonly LV: 'LV'
  readonly LB: 'LB'
  readonly LS: 'LS'
  readonly LR: 'LR'
  readonly LY: 'LY'
  readonly LI: 'LI'
  readonly LT: 'LT'
  readonly LU: 'LU'
  readonly MO: 'MO'
  readonly MK: 'MK'
  readonly MG: 'MG'
  readonly MW: 'MW'
  readonly MY: 'MY'
  readonly MV: 'MV'
  readonly ML: 'ML'
  readonly MT: 'MT'
  readonly MH: 'MH'
  readonly MQ: 'MQ'
  readonly MR: 'MR'
  readonly MU: 'MU'
  readonly YT: 'YT'
  readonly MX: 'MX'
  readonly FM: 'FM'
  readonly MD: 'MD'
  readonly MC: 'MC'
  readonly MN: 'MN'
  readonly ME: 'ME'
  readonly MS: 'MS'
  readonly MA: 'MA'
  readonly MZ: 'MZ'
  readonly MM: 'MM'
  readonly NA: 'NA'
  readonly NR: 'NR'
  readonly NP: 'NP'
  readonly NL: 'NL'
  readonly AN: 'AN'
  readonly NC: 'NC'
  readonly NZ: 'NZ'
  readonly NI: 'NI'
  readonly NE: 'NE'
  readonly NG: 'NG'
  readonly NU: 'NU'
  readonly NF: 'NF'
  readonly MP: 'MP'
  readonly KP: 'KP'
  readonly NO: 'NO'
  readonly OM: 'OM'
  readonly PK: 'PK'
  readonly PW: 'PW'
  readonly PS: 'PS'
  readonly PA: 'PA'
  readonly PG: 'PG'
  readonly PY: 'PY'
  readonly PE: 'PE'
  readonly PH: 'PH'
  readonly PN: 'PN'
  readonly PL: 'PL'
  readonly PT: 'PT'
  readonly QA: 'QA'
  readonly RE: 'RE'
  readonly RO: 'RO'
  readonly RU: 'RU'
  readonly RW: 'RW'
  readonly WS: 'WS'
  readonly SM: 'SM'
  readonly ST: 'ST'
  readonly SA: 'SA'
  readonly SN: 'SN'
  readonly RS: 'RS'
  readonly SC: 'SC'
  readonly SL: 'SL'
  readonly SG: 'SG'
  readonly SK: 'SK'
  readonly SI: 'SI'
  readonly SB: 'SB'
  readonly SO: 'SO'
  readonly ZA: 'ZA'
  readonly GS: 'GS'
  readonly KR: 'KR'
  readonly ES: 'ES'
  readonly LK: 'LK'
  readonly BL: 'BL'
  readonly SH: 'SH'
  readonly KN: 'KN'
  readonly LC: 'LC'
  readonly MF: 'MF'
  readonly PM: 'PM'
  readonly VC: 'VC'
  readonly SD: 'SD'
  readonly SR: 'SR'
  readonly SJ: 'SJ'
  readonly SZ: 'SZ'
  readonly SE: 'SE'
  readonly CH: 'CH'
  readonly SY: 'SY'
  readonly TW: 'TW'
  readonly TJ: 'TJ'
  readonly TZ: 'TZ'
  readonly TH: 'TH'
  readonly TL: 'TL'
  readonly TG: 'TG'
  readonly TK: 'TK'
  readonly TO: 'TO'
  readonly TT: 'TT'
  readonly TN: 'TN'
  readonly TR: 'TR'
  readonly TM: 'TM'
  readonly TC: 'TC'
  readonly TV: 'TV'
  readonly UG: 'UG'
  readonly UA: 'UA'
  readonly AE: 'AE'
  readonly GB: 'GB'
  readonly US: 'US'
  readonly UY: 'UY'
  readonly UM: 'UM'
  readonly VI: 'VI'
  readonly UZ: 'UZ'
  readonly VU: 'VU'
  readonly VA: 'VA'
  readonly VE: 'VE'
  readonly VN: 'VN'
  readonly WF: 'WF'
  readonly EH: 'EH'
  readonly YE: 'YE'
  readonly ZM: 'ZM'
  readonly ZW: 'ZW'
}

export declare const enumGiftRegistryDynamicAttributeGroup: {
  readonly EVENT_INFORMATION: 'EVENT_INFORMATION'
  readonly PRIVACY_SETTINGS: 'PRIVACY_SETTINGS'
  readonly REGISTRANT: 'REGISTRANT'
  readonly GENERAL_INFORMATION: 'GENERAL_INFORMATION'
  readonly DETAILED_INFORMATION: 'DETAILED_INFORMATION'
  readonly SHIPPING_ADDRESS: 'SHIPPING_ADDRESS'
}

export declare const enumGiftRegistryPrivacySettings: {
  readonly PRIVATE: 'PRIVATE'
  readonly PUBLIC: 'PUBLIC'
}

export declare const enumGiftRegistryStatus: {
  readonly ACTIVE: 'ACTIVE'
  readonly INACTIVE: 'INACTIVE'
}

export declare const enumReturnItemStatus: {
  readonly PENDING: 'PENDING'
  readonly AUTHORIZED: 'AUTHORIZED'
  readonly RECEIVED: 'RECEIVED'
  readonly APPROVED: 'APPROVED'
  readonly REJECTED: 'REJECTED'
  readonly DENIED: 'DENIED'
}

export declare const enumReturnShippingTrackingStatusType: {
  readonly INFORMATION: 'INFORMATION'
  readonly ERROR: 'ERROR'
}

export declare const enumReturnStatus: {
  readonly PENDING: 'PENDING'
  readonly AUTHORIZED: 'AUTHORIZED'
  readonly PARTIALLY_AUTHORIZED: 'PARTIALLY_AUTHORIZED'
  readonly RECEIVED: 'RECEIVED'
  readonly PARTIALLY_RECEIVED: 'PARTIALLY_RECEIVED'
  readonly APPROVED: 'APPROVED'
  readonly PARTIALLY_APPROVED: 'PARTIALLY_APPROVED'
  readonly REJECTED: 'REJECTED'
  readonly PARTIALLY_REJECTED: 'PARTIALLY_REJECTED'
  readonly DENIED: 'DENIED'
  readonly PROCESSED_AND_CLOSED: 'PROCESSED_AND_CLOSED'
  readonly CLOSED: 'CLOSED'
}

export declare const enumRewardPointsSubscriptionStatusesEnum: {
  readonly SUBSCRIBED: 'SUBSCRIBED'
  readonly NOT_SUBSCRIBED: 'NOT_SUBSCRIBED'
}

export declare const enumPriceTypeEnum: {
  readonly FIXED: 'FIXED'
  readonly PERCENT: 'PERCENT'
  readonly DYNAMIC: 'DYNAMIC'
}

export declare const enumWishlistVisibilityEnum: {
  readonly PUBLIC: 'PUBLIC'
  readonly PRIVATE: 'PRIVATE'
}

export declare const enumPaymentTokenTypeEnum: {
  readonly card: 'card'
  readonly account: 'account'
}

export declare const enumDynamicBlockLocationEnum: {
  readonly CONTENT: 'CONTENT'
  readonly HEADER: 'HEADER'
  readonly FOOTER: 'FOOTER'
  readonly LEFT: 'LEFT'
  readonly RIGHT: 'RIGHT'
}

export declare const enumDynamicBlockTypeEnum: {
  readonly SPECIFIED: 'SPECIFIED'
  readonly CART_PRICE_RULE_RELATED: 'CART_PRICE_RULE_RELATED'
  readonly CATALOG_PRICE_RULE_RELATED: 'CATALOG_PRICE_RULE_RELATED'
}

export declare const enumPayflowLinkMode: {
  readonly TEST: 'TEST'
  readonly LIVE: 'LIVE'
}

export declare const enumReCaptchaFormEnum: {
  readonly PLACE_ORDER: 'PLACE_ORDER'
  readonly CONTACT: 'CONTACT'
  readonly CUSTOMER_LOGIN: 'CUSTOMER_LOGIN'
  readonly CUSTOMER_FORGOT_PASSWORD: 'CUSTOMER_FORGOT_PASSWORD'
  readonly CUSTOMER_CREATE: 'CUSTOMER_CREATE'
  readonly CUSTOMER_EDIT: 'CUSTOMER_EDIT'
  readonly NEWSLETTER: 'NEWSLETTER'
  readonly PRODUCT_REVIEW: 'PRODUCT_REVIEW'
  readonly SENDFRIEND: 'SENDFRIEND'
  readonly BRAINTREE: 'BRAINTREE'
}

export declare const enumCartUserInputErrorType: {
  readonly PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND'
  readonly NOT_SALABLE: 'NOT_SALABLE'
  readonly INSUFFICIENT_STOCK: 'INSUFFICIENT_STOCK'
  readonly UNDEFINED: 'UNDEFINED'
}

export declare const enumWishListUserInputErrorType: {
  readonly PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND'
  readonly UNDEFINED: 'UNDEFINED'
}

export declare const enumWishlistCartUserInputErrorType: {
  readonly PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND'
  readonly NOT_SALABLE: 'NOT_SALABLE'
  readonly INSUFFICIENT_STOCK: 'INSUFFICIENT_STOCK'
  readonly UNDEFINED: 'UNDEFINED'
}

export declare const enumGiftRegistryItemsUserErrorType: {
  readonly OUT_OF_STOCK: 'OUT_OF_STOCK'
  readonly NOT_FOUND: 'NOT_FOUND'
  readonly UNDEFINED: 'UNDEFINED'
}

export declare const enumCheckoutUserInputErrorCodes: {
  readonly REORDER_NOT_AVAILABLE: 'REORDER_NOT_AVAILABLE'
  readonly PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND'
  readonly NOT_SALABLE: 'NOT_SALABLE'
  readonly INSUFFICIENT_STOCK: 'INSUFFICIENT_STOCK'
  readonly UNDEFINED: 'UNDEFINED'
}

export declare const enumSubscriptionStatusesEnum: {
  readonly NOT_ACTIVE: 'NOT_ACTIVE'
  readonly SUBSCRIBED: 'SUBSCRIBED'
  readonly UNSUBSCRIBED: 'UNSUBSCRIBED'
  readonly UNCONFIRMED: 'UNCONFIRMED'
}

export declare const enumBatchMutationStatus: {
  readonly SUCCESS: 'SUCCESS'
  readonly FAILURE: 'FAILURE'
  readonly MIXED_RESULTS: 'MIXED_RESULTS'
}

export declare const enumCustomizableDateTypeEnum: {
  readonly DATE: 'DATE'
  readonly DATE_TIME: 'DATE_TIME'
  readonly TIME: 'TIME'
}

export declare const enumDownloadableFileTypeEnum: {
  readonly FILE: 'FILE'
  readonly URL: 'URL'
}

export declare const enumCustomAttributesListsEnum: {
  readonly PRODUCT_DETAILS_PAGE: 'PRODUCT_DETAILS_PAGE'
  readonly PRODUCTS_LISTING: 'PRODUCTS_LISTING'
  readonly PRODUCTS_COMPARE: 'PRODUCTS_COMPARE'
  readonly PRODUCT_SORT: 'PRODUCT_SORT'
  readonly PRODUCT_FILTER: 'PRODUCT_FILTER'
  readonly PRODUCT_SEARCH_RESULTS_FILTER: 'PRODUCT_SEARCH_RESULTS_FILTER'
  readonly ADVANCED_CATALOG_SEARCH: 'ADVANCED_CATALOG_SEARCH'
}

export declare const enumPriceViewEnum: {
  readonly PRICE_RANGE: 'PRICE_RANGE'
  readonly AS_LOW_AS: 'AS_LOW_AS'
}

export declare const enumShipBundleItemsEnum: {
  readonly TOGETHER: 'TOGETHER'
  readonly SEPARATELY: 'SEPARATELY'
}

export declare const enumGiftCardTypeEnum: {
  readonly VIRTUAL: 'VIRTUAL'
  readonly PHYSICAL: 'PHYSICAL'
  readonly COMBINED: 'COMBINED'
}
