const {
  linkTypeMap,
  createClient: createClientOriginal,
  generateGraphqlOperation,
  assertSameVersion,
} = require('@genql/runtime')
var typeMap = linkTypeMap(require('./types.cjs'))

var version = '2.9.0'
assertSameVersion(version)

module.exports.version = version

module.exports.createClient = function(options) {
  options = options || {}
  var optionsCopy = {
    url: 'https://master-7rqtwti-mfwmkrjfqvbjk.us-4.magentosite.cloud/graphql',
    queryRoot: typeMap.Query,
    mutationRoot: typeMap.Mutation,
    subscriptionRoot: typeMap.Subscription,
  }
  for (var name in options) {
    optionsCopy[name] = options[name]
  }
  return createClientOriginal(optionsCopy)
}

module.exports.enumAttributeEntityTypeEnum = {
  PRODUCT: 'PRODUCT',
}

module.exports.enumObjectDataTypeEnum = {
  STRING: 'STRING',
  FLOAT: 'FLOAT',
  INT: 'INT',
  BOOLEAN: 'BOOLEAN',
  COMPLEX: 'COMPLEX',
}

module.exports.enumUiInputTypeEnum = {
  BOOLEAN: 'BOOLEAN',
  DATE: 'DATE',
  DATETIME: 'DATETIME',
  GALLERY: 'GALLERY',
  IMAGE: 'IMAGE',
  MEDIA_IMAGE: 'MEDIA_IMAGE',
  MULTISELECT: 'MULTISELECT',
  PRICE: 'PRICE',
  SELECT: 'SELECT',
  TEXT: 'TEXT',
  TEXTAREA: 'TEXTAREA',
  WEIGHT: 'WEIGHT',
  FIXED_PRODUCT_TAX: 'FIXED_PRODUCT_TAX',
}

module.exports.enumFixedProductTaxDisplaySettings = {
  INCLUDE_FPT_WITHOUT_DETAILS: 'INCLUDE_FPT_WITHOUT_DETAILS',
  INCLUDE_FPT_WITH_DETAILS: 'INCLUDE_FPT_WITH_DETAILS',
  EXCLUDE_FPT_AND_INCLUDE_WITH_DETAILS: 'EXCLUDE_FPT_AND_INCLUDE_WITH_DETAILS',
  EXCLUDE_FPT_WITHOUT_DETAILS: 'EXCLUDE_FPT_WITHOUT_DETAILS',
  FPT_DISABLED: 'FPT_DISABLED',
}

module.exports.enumCurrencyEnum = {
  AFN: 'AFN',
  ALL: 'ALL',
  AZN: 'AZN',
  DZD: 'DZD',
  AOA: 'AOA',
  ARS: 'ARS',
  AMD: 'AMD',
  AWG: 'AWG',
  AUD: 'AUD',
  BSD: 'BSD',
  BHD: 'BHD',
  BDT: 'BDT',
  BBD: 'BBD',
  BYN: 'BYN',
  BZD: 'BZD',
  BMD: 'BMD',
  BTN: 'BTN',
  BOB: 'BOB',
  BAM: 'BAM',
  BWP: 'BWP',
  BRL: 'BRL',
  GBP: 'GBP',
  BND: 'BND',
  BGN: 'BGN',
  BUK: 'BUK',
  BIF: 'BIF',
  KHR: 'KHR',
  CAD: 'CAD',
  CVE: 'CVE',
  CZK: 'CZK',
  KYD: 'KYD',
  GQE: 'GQE',
  CLP: 'CLP',
  CNY: 'CNY',
  COP: 'COP',
  KMF: 'KMF',
  CDF: 'CDF',
  CRC: 'CRC',
  HRK: 'HRK',
  CUP: 'CUP',
  DKK: 'DKK',
  DJF: 'DJF',
  DOP: 'DOP',
  XCD: 'XCD',
  EGP: 'EGP',
  SVC: 'SVC',
  ERN: 'ERN',
  EEK: 'EEK',
  ETB: 'ETB',
  EUR: 'EUR',
  FKP: 'FKP',
  FJD: 'FJD',
  GMD: 'GMD',
  GEK: 'GEK',
  GEL: 'GEL',
  GHS: 'GHS',
  GIP: 'GIP',
  GTQ: 'GTQ',
  GNF: 'GNF',
  GYD: 'GYD',
  HTG: 'HTG',
  HNL: 'HNL',
  HKD: 'HKD',
  HUF: 'HUF',
  ISK: 'ISK',
  INR: 'INR',
  IDR: 'IDR',
  IRR: 'IRR',
  IQD: 'IQD',
  ILS: 'ILS',
  JMD: 'JMD',
  JPY: 'JPY',
  JOD: 'JOD',
  KZT: 'KZT',
  KES: 'KES',
  KWD: 'KWD',
  KGS: 'KGS',
  LAK: 'LAK',
  LVL: 'LVL',
  LBP: 'LBP',
  LSL: 'LSL',
  LRD: 'LRD',
  LYD: 'LYD',
  LTL: 'LTL',
  MOP: 'MOP',
  MKD: 'MKD',
  MGA: 'MGA',
  MWK: 'MWK',
  MYR: 'MYR',
  MVR: 'MVR',
  LSM: 'LSM',
  MRO: 'MRO',
  MUR: 'MUR',
  MXN: 'MXN',
  MDL: 'MDL',
  MNT: 'MNT',
  MAD: 'MAD',
  MZN: 'MZN',
  MMK: 'MMK',
  NAD: 'NAD',
  NPR: 'NPR',
  ANG: 'ANG',
  YTL: 'YTL',
  NZD: 'NZD',
  NIC: 'NIC',
  NGN: 'NGN',
  KPW: 'KPW',
  NOK: 'NOK',
  OMR: 'OMR',
  PKR: 'PKR',
  PAB: 'PAB',
  PGK: 'PGK',
  PYG: 'PYG',
  PEN: 'PEN',
  PHP: 'PHP',
  PLN: 'PLN',
  QAR: 'QAR',
  RHD: 'RHD',
  RON: 'RON',
  RUB: 'RUB',
  RWF: 'RWF',
  SHP: 'SHP',
  STD: 'STD',
  SAR: 'SAR',
  RSD: 'RSD',
  SCR: 'SCR',
  SLL: 'SLL',
  SGD: 'SGD',
  SKK: 'SKK',
  SBD: 'SBD',
  SOS: 'SOS',
  ZAR: 'ZAR',
  KRW: 'KRW',
  LKR: 'LKR',
  SDG: 'SDG',
  SRD: 'SRD',
  SZL: 'SZL',
  SEK: 'SEK',
  CHF: 'CHF',
  SYP: 'SYP',
  TWD: 'TWD',
  TJS: 'TJS',
  TZS: 'TZS',
  THB: 'THB',
  TOP: 'TOP',
  TTD: 'TTD',
  TND: 'TND',
  TMM: 'TMM',
  USD: 'USD',
  UGX: 'UGX',
  UAH: 'UAH',
  AED: 'AED',
  UYU: 'UYU',
  UZS: 'UZS',
  VUV: 'VUV',
  VEB: 'VEB',
  VEF: 'VEF',
  VND: 'VND',
  CHE: 'CHE',
  CHW: 'CHW',
  XOF: 'XOF',
  WST: 'WST',
  YER: 'YER',
  ZMK: 'ZMK',
  ZWD: 'ZWD',
  TRY: 'TRY',
  AZM: 'AZM',
  ROL: 'ROL',
  TRL: 'TRL',
  XPF: 'XPF',
}

module.exports.enumCartItemErrorType = {
  UNDEFINED: 'UNDEFINED',
  ITEM_QTY: 'ITEM_QTY',
  ITEM_INCREMENTS: 'ITEM_INCREMENTS',
}

module.exports.enumSortEnum = {
  ASC: 'ASC',
  DESC: 'DESC',
}

module.exports.enumPriceAdjustmentCodesEnum = {
  TAX: 'TAX',
  WEEE: 'WEEE',
  WEEE_TAX: 'WEEE_TAX',
}

module.exports.enumPriceAdjustmentDescriptionEnum = {
  INCLUDED: 'INCLUDED',
  EXCLUDED: 'EXCLUDED',
}

module.exports.enumProductStockStatus = {
  IN_STOCK: 'IN_STOCK',
  OUT_OF_STOCK: 'OUT_OF_STOCK',
}

module.exports.enumUrlRewriteEntityTypeEnum = {
  CMS_PAGE: 'CMS_PAGE',
  PRODUCT: 'PRODUCT',
  CATEGORY: 'CATEGORY',
}

module.exports.enumCheckoutAgreementMode = {
  AUTO: 'AUTO',
  MANUAL: 'MANUAL',
}

module.exports.enumUseInLayeredNavigationOptions = {
  NO: 'NO',
  FILTERABLE_WITH_RESULTS: 'FILTERABLE_WITH_RESULTS',
  FILTERABLE_NO_RESULT: 'FILTERABLE_NO_RESULT',
}

module.exports.enumCountryCodeEnum = {
  AF: 'AF',
  AX: 'AX',
  AL: 'AL',
  DZ: 'DZ',
  AS: 'AS',
  AD: 'AD',
  AO: 'AO',
  AI: 'AI',
  AQ: 'AQ',
  AG: 'AG',
  AR: 'AR',
  AM: 'AM',
  AW: 'AW',
  AU: 'AU',
  AT: 'AT',
  AZ: 'AZ',
  BS: 'BS',
  BH: 'BH',
  BD: 'BD',
  BB: 'BB',
  BY: 'BY',
  BE: 'BE',
  BZ: 'BZ',
  BJ: 'BJ',
  BM: 'BM',
  BT: 'BT',
  BO: 'BO',
  BA: 'BA',
  BW: 'BW',
  BV: 'BV',
  BR: 'BR',
  IO: 'IO',
  VG: 'VG',
  BN: 'BN',
  BG: 'BG',
  BF: 'BF',
  BI: 'BI',
  KH: 'KH',
  CM: 'CM',
  CA: 'CA',
  CV: 'CV',
  KY: 'KY',
  CF: 'CF',
  TD: 'TD',
  CL: 'CL',
  CN: 'CN',
  CX: 'CX',
  CC: 'CC',
  CO: 'CO',
  KM: 'KM',
  CG: 'CG',
  CD: 'CD',
  CK: 'CK',
  CR: 'CR',
  CI: 'CI',
  HR: 'HR',
  CU: 'CU',
  CY: 'CY',
  CZ: 'CZ',
  DK: 'DK',
  DJ: 'DJ',
  DM: 'DM',
  DO: 'DO',
  EC: 'EC',
  EG: 'EG',
  SV: 'SV',
  GQ: 'GQ',
  ER: 'ER',
  EE: 'EE',
  ET: 'ET',
  FK: 'FK',
  FO: 'FO',
  FJ: 'FJ',
  FI: 'FI',
  FR: 'FR',
  GF: 'GF',
  PF: 'PF',
  TF: 'TF',
  GA: 'GA',
  GM: 'GM',
  GE: 'GE',
  DE: 'DE',
  GH: 'GH',
  GI: 'GI',
  GR: 'GR',
  GL: 'GL',
  GD: 'GD',
  GP: 'GP',
  GU: 'GU',
  GT: 'GT',
  GG: 'GG',
  GN: 'GN',
  GW: 'GW',
  GY: 'GY',
  HT: 'HT',
  HM: 'HM',
  HN: 'HN',
  HK: 'HK',
  HU: 'HU',
  IS: 'IS',
  IN: 'IN',
  ID: 'ID',
  IR: 'IR',
  IQ: 'IQ',
  IE: 'IE',
  IM: 'IM',
  IL: 'IL',
  IT: 'IT',
  JM: 'JM',
  JP: 'JP',
  JE: 'JE',
  JO: 'JO',
  KZ: 'KZ',
  KE: 'KE',
  KI: 'KI',
  KW: 'KW',
  KG: 'KG',
  LA: 'LA',
  LV: 'LV',
  LB: 'LB',
  LS: 'LS',
  LR: 'LR',
  LY: 'LY',
  LI: 'LI',
  LT: 'LT',
  LU: 'LU',
  MO: 'MO',
  MK: 'MK',
  MG: 'MG',
  MW: 'MW',
  MY: 'MY',
  MV: 'MV',
  ML: 'ML',
  MT: 'MT',
  MH: 'MH',
  MQ: 'MQ',
  MR: 'MR',
  MU: 'MU',
  YT: 'YT',
  MX: 'MX',
  FM: 'FM',
  MD: 'MD',
  MC: 'MC',
  MN: 'MN',
  ME: 'ME',
  MS: 'MS',
  MA: 'MA',
  MZ: 'MZ',
  MM: 'MM',
  NA: 'NA',
  NR: 'NR',
  NP: 'NP',
  NL: 'NL',
  AN: 'AN',
  NC: 'NC',
  NZ: 'NZ',
  NI: 'NI',
  NE: 'NE',
  NG: 'NG',
  NU: 'NU',
  NF: 'NF',
  MP: 'MP',
  KP: 'KP',
  NO: 'NO',
  OM: 'OM',
  PK: 'PK',
  PW: 'PW',
  PS: 'PS',
  PA: 'PA',
  PG: 'PG',
  PY: 'PY',
  PE: 'PE',
  PH: 'PH',
  PN: 'PN',
  PL: 'PL',
  PT: 'PT',
  QA: 'QA',
  RE: 'RE',
  RO: 'RO',
  RU: 'RU',
  RW: 'RW',
  WS: 'WS',
  SM: 'SM',
  ST: 'ST',
  SA: 'SA',
  SN: 'SN',
  RS: 'RS',
  SC: 'SC',
  SL: 'SL',
  SG: 'SG',
  SK: 'SK',
  SI: 'SI',
  SB: 'SB',
  SO: 'SO',
  ZA: 'ZA',
  GS: 'GS',
  KR: 'KR',
  ES: 'ES',
  LK: 'LK',
  BL: 'BL',
  SH: 'SH',
  KN: 'KN',
  LC: 'LC',
  MF: 'MF',
  PM: 'PM',
  VC: 'VC',
  SD: 'SD',
  SR: 'SR',
  SJ: 'SJ',
  SZ: 'SZ',
  SE: 'SE',
  CH: 'CH',
  SY: 'SY',
  TW: 'TW',
  TJ: 'TJ',
  TZ: 'TZ',
  TH: 'TH',
  TL: 'TL',
  TG: 'TG',
  TK: 'TK',
  TO: 'TO',
  TT: 'TT',
  TN: 'TN',
  TR: 'TR',
  TM: 'TM',
  TC: 'TC',
  TV: 'TV',
  UG: 'UG',
  UA: 'UA',
  AE: 'AE',
  GB: 'GB',
  US: 'US',
  UY: 'UY',
  UM: 'UM',
  VI: 'VI',
  UZ: 'UZ',
  VU: 'VU',
  VA: 'VA',
  VE: 'VE',
  VN: 'VN',
  WF: 'WF',
  EH: 'EH',
  YE: 'YE',
  ZM: 'ZM',
  ZW: 'ZW',
}

module.exports.enumGiftRegistryDynamicAttributeGroup = {
  EVENT_INFORMATION: 'EVENT_INFORMATION',
  PRIVACY_SETTINGS: 'PRIVACY_SETTINGS',
  REGISTRANT: 'REGISTRANT',
  GENERAL_INFORMATION: 'GENERAL_INFORMATION',
  DETAILED_INFORMATION: 'DETAILED_INFORMATION',
  SHIPPING_ADDRESS: 'SHIPPING_ADDRESS',
}

module.exports.enumGiftRegistryPrivacySettings = {
  PRIVATE: 'PRIVATE',
  PUBLIC: 'PUBLIC',
}

module.exports.enumGiftRegistryStatus = {
  ACTIVE: 'ACTIVE',
  INACTIVE: 'INACTIVE',
}

module.exports.enumReturnItemStatus = {
  PENDING: 'PENDING',
  AUTHORIZED: 'AUTHORIZED',
  RECEIVED: 'RECEIVED',
  APPROVED: 'APPROVED',
  REJECTED: 'REJECTED',
  DENIED: 'DENIED',
}

module.exports.enumReturnShippingTrackingStatusType = {
  INFORMATION: 'INFORMATION',
  ERROR: 'ERROR',
}

module.exports.enumReturnStatus = {
  PENDING: 'PENDING',
  AUTHORIZED: 'AUTHORIZED',
  PARTIALLY_AUTHORIZED: 'PARTIALLY_AUTHORIZED',
  RECEIVED: 'RECEIVED',
  PARTIALLY_RECEIVED: 'PARTIALLY_RECEIVED',
  APPROVED: 'APPROVED',
  PARTIALLY_APPROVED: 'PARTIALLY_APPROVED',
  REJECTED: 'REJECTED',
  PARTIALLY_REJECTED: 'PARTIALLY_REJECTED',
  DENIED: 'DENIED',
  PROCESSED_AND_CLOSED: 'PROCESSED_AND_CLOSED',
  CLOSED: 'CLOSED',
}

module.exports.enumRewardPointsSubscriptionStatusesEnum = {
  SUBSCRIBED: 'SUBSCRIBED',
  NOT_SUBSCRIBED: 'NOT_SUBSCRIBED',
}

module.exports.enumPriceTypeEnum = {
  FIXED: 'FIXED',
  PERCENT: 'PERCENT',
  DYNAMIC: 'DYNAMIC',
}

module.exports.enumWishlistVisibilityEnum = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
}

module.exports.enumPaymentTokenTypeEnum = {
  card: 'card',
  account: 'account',
}

module.exports.enumDynamicBlockLocationEnum = {
  CONTENT: 'CONTENT',
  HEADER: 'HEADER',
  FOOTER: 'FOOTER',
  LEFT: 'LEFT',
  RIGHT: 'RIGHT',
}

module.exports.enumDynamicBlockTypeEnum = {
  SPECIFIED: 'SPECIFIED',
  CART_PRICE_RULE_RELATED: 'CART_PRICE_RULE_RELATED',
  CATALOG_PRICE_RULE_RELATED: 'CATALOG_PRICE_RULE_RELATED',
}

module.exports.enumPayflowLinkMode = {
  TEST: 'TEST',
  LIVE: 'LIVE',
}

module.exports.enumReCaptchaFormEnum = {
  PLACE_ORDER: 'PLACE_ORDER',
  CONTACT: 'CONTACT',
  CUSTOMER_LOGIN: 'CUSTOMER_LOGIN',
  CUSTOMER_FORGOT_PASSWORD: 'CUSTOMER_FORGOT_PASSWORD',
  CUSTOMER_CREATE: 'CUSTOMER_CREATE',
  CUSTOMER_EDIT: 'CUSTOMER_EDIT',
  NEWSLETTER: 'NEWSLETTER',
  PRODUCT_REVIEW: 'PRODUCT_REVIEW',
  SENDFRIEND: 'SENDFRIEND',
  BRAINTREE: 'BRAINTREE',
}

module.exports.enumCartUserInputErrorType = {
  PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND',
  NOT_SALABLE: 'NOT_SALABLE',
  INSUFFICIENT_STOCK: 'INSUFFICIENT_STOCK',
  UNDEFINED: 'UNDEFINED',
}

module.exports.enumWishListUserInputErrorType = {
  PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND',
  UNDEFINED: 'UNDEFINED',
}

module.exports.enumWishlistCartUserInputErrorType = {
  PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND',
  NOT_SALABLE: 'NOT_SALABLE',
  INSUFFICIENT_STOCK: 'INSUFFICIENT_STOCK',
  UNDEFINED: 'UNDEFINED',
}

module.exports.enumGiftRegistryItemsUserErrorType = {
  OUT_OF_STOCK: 'OUT_OF_STOCK',
  NOT_FOUND: 'NOT_FOUND',
  UNDEFINED: 'UNDEFINED',
}

module.exports.enumCheckoutUserInputErrorCodes = {
  REORDER_NOT_AVAILABLE: 'REORDER_NOT_AVAILABLE',
  PRODUCT_NOT_FOUND: 'PRODUCT_NOT_FOUND',
  NOT_SALABLE: 'NOT_SALABLE',
  INSUFFICIENT_STOCK: 'INSUFFICIENT_STOCK',
  UNDEFINED: 'UNDEFINED',
}

module.exports.enumSubscriptionStatusesEnum = {
  NOT_ACTIVE: 'NOT_ACTIVE',
  SUBSCRIBED: 'SUBSCRIBED',
  UNSUBSCRIBED: 'UNSUBSCRIBED',
  UNCONFIRMED: 'UNCONFIRMED',
}

module.exports.enumBatchMutationStatus = {
  SUCCESS: 'SUCCESS',
  FAILURE: 'FAILURE',
  MIXED_RESULTS: 'MIXED_RESULTS',
}

module.exports.enumCustomizableDateTypeEnum = {
  DATE: 'DATE',
  DATE_TIME: 'DATE_TIME',
  TIME: 'TIME',
}

module.exports.enumDownloadableFileTypeEnum = {
  FILE: 'FILE',
  URL: 'URL',
}

module.exports.enumCustomAttributesListsEnum = {
  PRODUCT_DETAILS_PAGE: 'PRODUCT_DETAILS_PAGE',
  PRODUCTS_LISTING: 'PRODUCTS_LISTING',
  PRODUCTS_COMPARE: 'PRODUCTS_COMPARE',
  PRODUCT_SORT: 'PRODUCT_SORT',
  PRODUCT_FILTER: 'PRODUCT_FILTER',
  PRODUCT_SEARCH_RESULTS_FILTER: 'PRODUCT_SEARCH_RESULTS_FILTER',
  ADVANCED_CATALOG_SEARCH: 'ADVANCED_CATALOG_SEARCH',
}

module.exports.enumPriceViewEnum = {
  PRICE_RANGE: 'PRICE_RANGE',
  AS_LOW_AS: 'AS_LOW_AS',
}

module.exports.enumShipBundleItemsEnum = {
  TOGETHER: 'TOGETHER',
  SEPARATELY: 'SEPARATELY',
}

module.exports.enumGiftCardTypeEnum = {
  VIRTUAL: 'VIRTUAL',
  PHYSICAL: 'PHYSICAL',
  COMBINED: 'COMBINED',
}

module.exports.generateQueryOp = function(fields) {
  return generateGraphqlOperation('query', typeMap.Query, fields)
}
module.exports.generateMutationOp = function(fields) {
  return generateGraphqlOperation('mutation', typeMap.Mutation, fields)
}
module.exports.generateSubscriptionOp = function(fields) {
  return generateGraphqlOperation('subscription', typeMap.Subscription, fields)
}
module.exports.everything = {
  __scalar: true,
}

var schemaExports = require('./guards.cjs')
for (var k in schemaExports) {
  module.exports[k] = schemaExports[k]
}
