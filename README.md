<div align="center">
<img src="readme/nodejs_animation.gif" width="250" height="250">
</div>

<h1 align="center"> Hasura - Typescript </h1>

## 🧑🏻‍💻🧑🏻‍💻Desenvolvido por

@[AdaoBJr](https://bitbucket.org/AdaoBJr/)
<br>

---

## 💡 Sobre o Projeto

O Projeto NodeJs-Typescript é um app desenvolvido para uso com NodeJs - Express - TypeScript com o intuito de aprofundar os conhecimentos em Hasura e Typescript.

## 🛠 Tecnologias Usadas

- NodeJS
- Hasura
- TypeScript
- Express
- Babel

## 🧙‍♂️ Como Iniciar o Projeto

Primeiro faça a clonagem do projeto em algum diretorio do seu computador:

```bash
> cd ~
> cd Documents/Mcx
> git clone git@bitbucket.org:AdaoBJr/6.hasura_ts.git Hasura_TS
> cd Hasura_TS
```

Depois disso instale as dependências:

```bash
> yarn ou npm install
```

Por fim, rode o seguinte script para iniciar o projeto:

```bash
> yarn dev
```

O projeto vai iniciar em http://localhost:4000/.
