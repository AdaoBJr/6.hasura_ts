import { GraphQLClient } from 'graphql-request';
import { IMagentoProvider } from '../IMagentoProvider';
import { IMagentoProviderDTO } from '../IMagentoProviderDTO';

class MagentoProvider implements IMagentoProvider {
  private clientMagento = new GraphQLClient(process.env.MAGENTO_BACKEND_URL);

  async request(data: IMagentoProviderDTO): Promise<any> {
    const { variables, query, token } = data;
    const headers = token && { authorization: `Bearer ${token}` };

    return Object.values(await this.clientMagento.request(query, variables, headers))[0];
  }
}

export { MagentoProvider };
