import { IMagentoProviderDTO } from './IMagentoProviderDTO';

interface IMagentoProvider {
  request(data: IMagentoProviderDTO): Promise<any>;
}

export { IMagentoProvider };
