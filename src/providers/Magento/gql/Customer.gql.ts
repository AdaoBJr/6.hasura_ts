import { gql } from 'graphql-request';

export const CREATE_CUSTOMER_MUTATION = gql`
  mutation createCustomerV2(
    $firstname: String!
    $lastname: String!
    $email: String!
    $password: String
    $date_of_birth: String
    $gender: Int
    $is_subscribed: Boolean
    $allow_remote_shopping_assistance: Boolean
  ) {
    createCustomerV2(
      input: {
        firstname: $firstname
        lastname: $lastname
        email: $email
        password: $password
        date_of_birth: $date_of_birth
        gender: $gender
        is_subscribed: $is_subscribed
        allow_remote_shopping_assistance: $allow_remote_shopping_assistance
      }
    ) {
      customer {
        firstname
        lastname
        email
        date_of_birth
        gender
        is_subscribed
        allow_remote_shopping_assistance
      }
    }
  }
`;

export const SIGNIN_CUSTOMER_MUTATION = gql`
  mutation generateCustomerToken($email: String!, $password: String!) {
    generateCustomerToken(email: $email, password: $password) {
      token
    }
  }
`;

export const CUSTOMER_QUERY = gql`
  query customer {
    customer {
      firstname
      lastname
      email
      is_subscribed
      created_at
    }
  }
`;
