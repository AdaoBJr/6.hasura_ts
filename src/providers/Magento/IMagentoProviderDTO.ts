interface IMagentoProviderDTO {
  variables: any;
  query: string;
  token: string;
}

export { IMagentoProviderDTO };
