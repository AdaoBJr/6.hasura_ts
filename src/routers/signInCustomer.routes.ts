import { Router } from 'express';

import { signInCustomerController } from 'src/modules/customer/useCases/SignInCustomer';

const signInCustomerRouter = Router();

signInCustomerRouter.post('/', (request, response) =>
  signInCustomerController.handle(request, response)
);

export { signInCustomerRouter };
