import { Router } from 'express';
import { customerInfoController } from 'src/modules/customer/useCases/CustomerInfo';

const customerInfoRouter = Router();

customerInfoRouter.post('/', (request, response) =>
  customerInfoController.handle(request, response)
);

export { customerInfoRouter };
