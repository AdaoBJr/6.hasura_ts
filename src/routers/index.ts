import { Router } from 'express';
import { createCustomerRouter } from './createCustomer.routes';
import { customerInfoRouter } from './customerInfo.routes';
import { signInCustomerRouter } from './signInCustomer.routes';

const router = Router();

router.use('/register', createCustomerRouter);
router.use('/login', signInCustomerRouter);
router.use('/customer', customerInfoRouter);

export { router };
