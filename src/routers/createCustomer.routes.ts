import { Router } from 'express';

import { createCustomerController } from 'src/modules/customer/useCases/CreateCustomer';

const createCustomerRouter = Router();

createCustomerRouter.post('/', (request, response) =>
  createCustomerController.handle(request, response)
);

export { createCustomerRouter };
