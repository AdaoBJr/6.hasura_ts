import { Request, Response } from 'express';
import { CustomerInfoUseCase } from './CustomerInfoUseCase';

class CustomerInfoController {
  constructor(private customerInfoUseCase: CustomerInfoUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    try {
      const body = request.body;
      const token = request.headers.authorization;

      const customerInfo = await this.customerInfoUseCase.execute(body, token);

      return response.status(200).json(customerInfo);
    } catch (error) {
      return response.status(400).json({ message: error.message || 'Unexpected Error.' });
    }
  }
}

export { CustomerInfoController };
