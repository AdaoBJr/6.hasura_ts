import { Customer } from 'src/entities/Customer';
import { IMagentoProvider } from 'src/providers/Magento/IMagentoProvider';

class CustomerInfoUseCase {
  constructor(private magentoProvider: IMagentoProvider) {}

  async execute(body: Customer, token: string) {
    const query = body.request_query;
    const data = { variables: null, query, token };

    return this.magentoProvider.request(data);
  }
}

export { CustomerInfoUseCase };
