import { MagentoProvider } from 'src/providers/Magento/implementations/MagentoProvider';
import { CustomerInfoController } from './CustomerInfoController';
import { CustomerInfoUseCase } from './CustomerInfoUseCase';

const magentoProvider = new MagentoProvider();

const customerInfoUseCase = new CustomerInfoUseCase(magentoProvider);

const customerInfoController = new CustomerInfoController(customerInfoUseCase);

export { customerInfoUseCase, customerInfoController };
