import { Request, Response } from 'express';
import { SIGNIN_CUSTOMER_MUTATION } from 'src/providers/Magento/gql/Customer.gql';
import { parseRequestBody } from 'src/utils/parseRequestBody';
import { SignInCustomerUseCase } from './SignInCustomerUseCase';

class SignInCustomerController {
  constructor(private signInCustomerUseCase: SignInCustomerUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    try {
      const data = parseRequestBody(request, SIGNIN_CUSTOMER_MUTATION);

      const user = await this.signInCustomerUseCase.execute(data);
      return response.status(200).json(user);
    } catch (error) {
      return response.status(400).json({ message: error.message || 'Unexpected Error.' });
    }
  }
}

export { SignInCustomerController };
