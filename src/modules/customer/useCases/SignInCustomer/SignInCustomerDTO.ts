import { SignIn } from 'src/entities/SignIn';

interface SignInCustomerDTO {
  variables: SignIn;
  query: string;
  token: string;
}

export { SignInCustomerDTO };
