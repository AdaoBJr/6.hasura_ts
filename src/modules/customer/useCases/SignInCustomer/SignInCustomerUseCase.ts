import { IMagentoProvider } from 'src/providers/Magento/IMagentoProvider';
import { SignInCustomerDTO } from './SignInCustomerDTO';

class SignInCustomerUseCase {
  constructor(private magentoProvider: IMagentoProvider) {}

  async execute(data: SignInCustomerDTO) {
    return await this.magentoProvider.request(data);
  }
}

export { SignInCustomerUseCase };
