import { MagentoProvider } from 'src/providers/Magento/implementations/MagentoProvider';
import { SignInCustomerController } from './SignInCustomerController';
import { SignInCustomerUseCase } from './SignInCustomerUseCase';

const magentoProvider = new MagentoProvider();

const signInCustomerUseCase = new SignInCustomerUseCase(magentoProvider);

const signInCustomerController = new SignInCustomerController(signInCustomerUseCase);

export { signInCustomerUseCase, signInCustomerController };
