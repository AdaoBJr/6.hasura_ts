import { MagentoProvider } from 'src/providers/Magento/implementations/MagentoProvider';
import { CreateCustomerController } from './CreateCustomerController';
import { CreateCustomerUseCase } from './CreateCustomerUseCase';

const magentoProvider = new MagentoProvider();

const createCustomerUseCase = new CreateCustomerUseCase(magentoProvider);

const createCustomerController = new CreateCustomerController(createCustomerUseCase);

export { createCustomerUseCase, createCustomerController };
