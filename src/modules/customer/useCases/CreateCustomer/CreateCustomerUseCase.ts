import { CREATE_CUSTOMER_MUTATION } from 'src/providers/Magento/gql/Customer.gql';
import { IMagentoProvider } from 'src/providers/Magento/IMagentoProvider';
import { CreateCustomerDTO } from './CreateCustomerDTO';

class CreateCustomerUseCase {
  constructor(private magentoProvider: IMagentoProvider) {}

  async execute(data: CreateCustomerDTO) {
    return await this.magentoProvider.request(data);
  }
}

export { CreateCustomerUseCase };
