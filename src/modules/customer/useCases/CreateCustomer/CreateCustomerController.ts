import { Request, Response } from 'express';
import { CREATE_CUSTOMER_MUTATION } from 'src/providers/Magento/gql/Customer.gql';
import { parseRequestBody } from 'src/utils/parseRequestBody';
import { CreateCustomerUseCase } from './CreateCustomerUseCase';

class CreateCustomerController {
  constructor(private createCustomerUseCase: CreateCustomerUseCase) {}

  async handle(request: Request, response: Response): Promise<Response> {
    try {
      const data = parseRequestBody(request, CREATE_CUSTOMER_MUTATION);

      const user = await this.createCustomerUseCase.execute(data);
      return response.status(201).json(user);
    } catch (error) {
      return response.status(400).json({ message: error.message || 'Unexpected Error.' });
    }
  }
}

export { CreateCustomerController };
