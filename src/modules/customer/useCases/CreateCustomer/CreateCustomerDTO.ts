import { Customer } from 'src/entities/Customer';

interface CreateCustomerDTO {
  variables: Customer;
  query: string;
  token: string;
}

export { CreateCustomerDTO };
