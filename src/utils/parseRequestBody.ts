import { Request } from 'express';

function parseRequestBody(request: Request, query: string) {
  const requestBody = request.body;

  const requestGraphQL = requestBody.input?.data || requestBody.input;
  const variables = requestGraphQL || requestBody;
  const token = request.headers.authorization;

  return { variables, query, token };
}

export { parseRequestBody };
