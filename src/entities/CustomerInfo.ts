interface CustomerInfo {
  firstname: string;
  lastname: string;
  email: string;
  is_subscribed: boolean;
  created_at: string;
}

export { CustomerInfo };
