interface SignIn {
  email: string;
  password: string;
}

export { SignIn };
