interface Customer {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  date_of_birth: string;
  gender: number;
  is_subscribed: boolean;
  allow_remote_shopping_assistance: boolean;
}

export { Customer };
